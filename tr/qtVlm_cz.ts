<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>Barrier</name>
    <message>
        <location filename="../src/Barrier.cpp" line="54"/>
        <source>Insert a point</source>
        <translation>Vložit bod</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="58"/>
        <source>Closed barrier</source>
        <translation>Uzavřená bariéra</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="64"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="68"/>
        <source>Delete barrier</source>
        <translation>Smazat bariéru</translation>
    </message>
</context>
<context>
    <name>BarrierPoint</name>
    <message>
        <location filename="../src/Barrier.cpp" line="341"/>
        <source>Remove point</source>
        <translation>Odebrat bod</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="345"/>
        <source>Insert a point</source>
        <translation>Vložit bod</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="349"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="353"/>
        <source>Closed barrier</source>
        <translation>Uzavřená bariéra</translation>
    </message>
    <message>
        <location filename="../src/Barrier.cpp" line="359"/>
        <source>Delete barrier</source>
        <translation>Smazat bariéru</translation>
    </message>
</context>
<context>
    <name>BarrierSet</name>
    <message>
        <location filename="../src/BarrierSet.cpp" line="95"/>
        <source>Edit barrier</source>
        <translation>Upravit bariéru</translation>
    </message>
    <message>
        <location filename="../src/BarrierSet.cpp" line="96"/>
        <source>The barrier you are editting has only one point,
 remove barrier?</source>
        <translation>Bariéra kterou upravuješ má pouze jeden bod,
chceš ji odstranit?</translation>
    </message>
</context>
<context>
    <name>BoardPilotVLMBoat</name>
    <message>
        <source>Heading</source>
        <translation type="obsolete">Kurs</translation>
    </message>
    <message>
        <source>background-color: rgb(239, 243, 247);</source>
        <translation type="obsolete">barva pozadí: rgb(239, 243, 247);</translation>
    </message>
    <message>
        <source> °</source>
        <translation type="obsolete"> °</translation>
    </message>
    <message>
        <source>Ortho</source>
        <translation type="obsolete">Ortho</translation>
    </message>
    <message>
        <source>VMG</source>
        <translation type="obsolete">VMG</translation>
    </message>
    <message>
        <source>VBVMG</source>
        <translation type="obsolete">VBVMG</translation>
    </message>
    <message>
        <source>Pilototo (x/x)</source>
        <translation type="obsolete">Autopilot (x/x)</translation>
    </message>
    <message>
        <source>Pilototo</source>
        <translation type="obsolete">Autopilot</translation>
    </message>
</context>
<context>
    <name>BoardPosition</name>
    <message>
        <source>000a00&apos;00&apos;&apos; W</source>
        <translation type="obsolete">000a00&apos;00&apos;&apos; W</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="obsolete">Pozice</translation>
    </message>
</context>
<context>
    <name>BoardRealBoatWP</name>
    <message>
        <source>Ortho</source>
        <translation type="obsolete">Ortho</translation>
    </message>
    <message>
        <source>999.99</source>
        <translation type="obsolete">999.99</translation>
    </message>
    <message>
        <source>a</source>
        <translation type="obsolete">za</translation>
    </message>
    <message>
        <source>Loxo</source>
        <translation type="obsolete">Loxo</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="obsolete">Úhel</translation>
    </message>
    <message>
        <source>DNM</source>
        <translation type="obsolete">DNM</translation>
    </message>
    <message>
        <source>nm</source>
        <translation type="obsolete">nm</translation>
    </message>
    <message>
        <source>VMG</source>
        <translation type="obsolete">VMG</translation>
    </message>
    <message>
        <source>99.99</source>
        <translation type="obsolete">99.99</translation>
    </message>
    <message>
        <source>PushButton</source>
        <translation type="obsolete">Tlačítko</translation>
    </message>
</context>
<context>
    <name>BoardRealPosition</name>
    <message>
        <source>Position</source>
        <translation type="obsolete">Pozice</translation>
    </message>
</context>
<context>
    <name>BoardRealUi</name>
    <message>
        <source>Nom bateau</source>
        <translation type="obsolete">Jméno lodi</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>Boat</source>
        <translation type="obsolete">Loď</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Informace</translation>
    </message>
</context>
<context>
    <name>BoardSpeed</name>
    <message>
        <source>Speed</source>
        <translation type="obsolete">Rychlost</translation>
    </message>
    <message>
        <source>99.99</source>
        <translation type="obsolete">99.99</translation>
    </message>
    <message>
        <source>Loch</source>
        <translation type="obsolete">Log</translation>
    </message>
    <message>
        <source>9999.99</source>
        <translation type="obsolete">9999.99</translation>
    </message>
    <message>
        <source>nm</source>
        <translation type="obsolete">nm</translation>
    </message>
    <message>
        <source>Avg</source>
        <translation type="obsolete">Prům</translation>
    </message>
</context>
<context>
    <name>BoardVlmNew</name>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="17"/>
        <source>VLM Command Board</source>
        <translation>VLM palubní deska</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="39"/>
        <source>Compas</source>
        <translation>Kompas</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="74"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="918"/>
        <source>BS</source>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="141"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="990"/>
        <source>TWS</source>
        <translation>TWS</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="183"/>
        <source>TWD</source>
        <translation>TWD</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="219"/>
        <source>Details</source>
        <translation>Detaily</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="239"/>
        <source>Bateau</source>
        <translation>Loď</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="267"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="290"/>
        <source>BS:</source>
        <translation>BS:</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="307"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="378"/>
        <source>14.32kts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="322"/>
        <source>HDG:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="339"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="410"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="481"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="513"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="552"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="687"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="726"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="758"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="879"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="911"/>
        <source>132.23d</source>
        <translation>132.23d</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="361"/>
        <source>Moy:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="393"/>
        <source>Loch:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="464"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="862"/>
        <source>Ortho:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="496"/>
        <source>DNM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="535"/>
        <source>Angle:</source>
        <translation>Úhel:</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="567"/>
        <source>VMG:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="584"/>
        <source>25.25kts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="610"/>
        <source>Wind</source>
        <translation>Vítr</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="638"/>
        <source>TWS:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="655"/>
        <source>25.35kts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="670"/>
        <source>TWD:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="709"/>
        <source>Pres:</source>
        <translation>Proti větru:</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="741"/>
        <source>Port.:</source>
        <translation>Po větru:</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="784"/>
        <source>Race</source>
        <translation>Závod</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="812"/>
        <source>-&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="827"/>
        <source>Fasnet gate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="894"/>
        <source>Dist:</source>
        <translation>Vzdál:</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="949"/>
        <source>Polaire</source>
        <translation>Polár</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="961"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1006"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1022"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1041"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1499"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="977"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="378"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="380"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="391"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="393"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="919"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="923"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="925"/>
        <source>kts</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1111"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1269"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1130"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="916"/>
        <source>TWA</source>
        <translation>TWA</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1187"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="924"/>
        <source>VMG</source>
        <translation>VMG</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1216"/>
        <source>HDG</source>
        <translation>HDG</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1245"/>
        <source>VBVMG</source>
        <translation>VBVMG</translation>
    </message>
    <message>
        <source>ORTHO</source>
        <translation type="obsolete">Orthodromic</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="436"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1333"/>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1304"/>
        <source>ORT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1346"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="659"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1377"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1411"/>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1445"/>
        <source>background-color: rgb(239, 243, 247);</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVlmNew.ui" line="1461"/>
        <source>VLM Sync</source>
        <translation>VLM Sync</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="417"/>
        <source>Meilleurs angles au pres/portant:</source>
        <translation>Nejlepší úhly proti/po větru:</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="377"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="379"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="381"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="384"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="385"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="392"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="397"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="417"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="418"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="525"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="757"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="917"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="921"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="374"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="376"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="388"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="390"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="399"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="401"/>
        <source>nm</source>
        <translation>nm</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="483"/>
        <source>pas de polaire chargee</source>
        <translation>Není nahrán polár</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="734"/>
        <source>Pas de WP</source>
        <translation>Žádný aktivní WP</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="736"/>
        <source>Pas de WP actif</source>
        <translation>Žádný aktivní WP</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="777"/>
        <source>WP defini dans VLM (pas de POI correspondant)</source>
        <translation>WP definován ve VLM (žádný korespondující POI v qtVlm)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="779"/>
        <source>WP VLM</source>
        <translation>WP-VLM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="783"/>
        <source>WP: </source>
        <translation>WP:</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="787"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="793"/>
        <source>WP defini dans VLM (</source>
        <translation>WP definován ve VLM (</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="787"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="793"/>
        <source> dans qtVlm)</source>
        <translation> v qtVlm)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="795"/>
        <source>Le cap a suivre n&apos;est pas le meme</source>
        <translation>Varování: kurs po WP je jiný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="801"/>
        <source>WP defini dans VLM mais le mode de navigation n&apos;est pas coherent</source>
        <translation>WP definován ve VLM, ale mód navigace není kompatibilní</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="838"/>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="856"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="920"/>
        <source>AWA</source>
        <translation>AWA</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="922"/>
        <source>AWS</source>
        <translation>AWS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="991"/>
        <source>Confirmation a chaque ordre vers VLM</source>
        <translation>Potvrdit každý příkaz do VLM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/BoardVlmNew.cpp" line="992"/>
        <source>Confirmez-vous cet ordre?</source>
        <translation>Potvrzuješ tento příkaz?</translation>
    </message>
</context>
<context>
    <name>BoardVlmUi</name>
    <message>
        <source>Score</source>
        <translation type="obsolete">Skóre</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>VLM Sync</source>
        <translation type="obsolete">VLM Sync</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="obsolete">F5</translation>
    </message>
    <message>
        <source>forcee</source>
        <translation type="obsolete">vynucený</translation>
    </message>
    <message>
        <source>ID:</source>
        <translation type="obsolete">ID:</translation>
    </message>
    <message>
        <source>Pseudo:</source>
        <translation type="obsolete">Přezdívka:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation type="obsolete">Email:</translation>
    </message>
</context>
<context>
    <name>BoardWP</name>
    <message>
        <source>Ortho</source>
        <translation type="obsolete">Ortho</translation>
    </message>
    <message>
        <source>999.99</source>
        <translation type="obsolete">999.99</translation>
    </message>
    <message>
        <source>a</source>
        <translation type="obsolete">za</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="obsolete">Úhel</translation>
    </message>
    <message>
        <source>DNM</source>
        <translation type="obsolete">DNM</translation>
    </message>
    <message>
        <source>nm</source>
        <translation type="obsolete">nm</translation>
    </message>
    <message>
        <source>VMG</source>
        <translation type="obsolete">VMG</translation>
    </message>
    <message>
        <source>99.99</source>
        <translation type="obsolete">99.99</translation>
    </message>
    <message>
        <source>PushButton</source>
        <translation type="obsolete">Tlačítko</translation>
    </message>
    <message>
        <source>WP: </source>
        <translation type="obsolete">WP:</translation>
    </message>
</context>
<context>
    <name>BoardWP_Real</name>
    <message>
        <source>WP: </source>
        <translation type="obsolete">WP:</translation>
    </message>
</context>
<context>
    <name>BoardWind</name>
    <message>
        <source>Dir</source>
        <translation type="obsolete">Směr</translation>
    </message>
    <message>
        <source>a</source>
        <translation type="obsolete">za</translation>
    </message>
    <message>
        <source>999.99</source>
        <translation type="obsolete">999.99</translation>
    </message>
    <message>
        <source>99.99</source>
        <translation type="obsolete">99.99</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="obsolete">Rychlost</translation>
    </message>
    <message>
        <source>Portant</source>
        <translation type="obsolete">Po větru</translation>
    </message>
    <message>
        <source>Pres</source>
        <translation type="obsolete">Proti větru</translation>
    </message>
</context>
<context>
    <name>BoardWindTool</name>
    <message>
        <source>Compas</source>
        <translation type="obsolete">Kompas</translation>
    </message>
    <message>
        <source>BS</source>
        <translation type="obsolete">BS</translation>
    </message>
    <message>
        <source>TWS</source>
        <translation type="obsolete">TWS</translation>
    </message>
    <message>
        <source>TWD</source>
        <translation type="obsolete">TWD</translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="obsolete">Detaily</translation>
    </message>
    <message>
        <source>Polaire</source>
        <translation type="obsolete">Polár</translation>
    </message>
    <message>
        <source>TWA</source>
        <translation type="obsolete">TWA</translation>
    </message>
    <message>
        <source>VMG</source>
        <translation type="obsolete">VMG</translation>
    </message>
    <message>
        <source>HDG</source>
        <translation type="obsolete">HDG</translation>
    </message>
    <message>
        <source>VBVMG</source>
        <translation type="obsolete">VBVMG</translation>
    </message>
    <message>
        <source>ORTHO</source>
        <translation type="obsolete">Orthodromic</translation>
    </message>
    <message>
        <source>WP</source>
        <translation type="obsolete">WP</translation>
    </message>
    <message>
        <source>Pilototo</source>
        <translation type="obsolete">Autopilot</translation>
    </message>
    <message>
        <source>background-color: rgb(239, 243, 247);</source>
        <translation type="obsolete">barva pozadí: rgb(239, 243, 247);</translation>
    </message>
    <message>
        <source>VLM Sync</source>
        <translation type="obsolete">VLM Sync</translation>
    </message>
</context>
<context>
    <name>DialogBoatAccount</name>
    <message>
        <source>Liste des bateaux pour</source>
        <translation type="obsolete">Boats list for</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="67"/>
        <source>Selectionner un skin tableau de bord VLM</source>
        <translation>Vybrat skin přístrojové desky VLM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="79"/>
        <source>Liste des bateaux pour </source>
        <translation>Seznam lodí pro </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="125"/>
        <source>&lt;Aucun&gt;</source>
        <translation>&lt;žádný&gt;</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="250"/>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="258"/>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="265"/>
        <source>pas en course en ce moment</source>
        <translation>momentálně nezávodí</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="265"/>
        <location filename="../src/Dialogs/DialogBoatAccount.cpp" line="267"/>
        <source>proprio</source>
        <translation>majitel</translation>
    </message>
</context>
<context>
    <name>DialogChooseBarrierSet</name>
    <message>
        <location filename="../src/Dialogs/DialogChooseBarrierSet.cpp" line="33"/>
        <source>Barrier set choice</source>
        <translation>Výběr sady bariér</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogChooseBarrierSet.cpp" line="33"/>
        <source>No barrier set define, create one before doing this action</source>
        <translation>Žádná sada bariér není definována, napřed nějakou vytvoř</translation>
    </message>
</context>
<context>
    <name>DialogChooseBarrierSet_ui</name>
    <message>
        <location filename="../src/Ui/DialogChooseBarrierSet.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogChooseBarrierSet.ui" line="22"/>
        <source>Barrier set:</source>
        <translation>Sada bariér:</translation>
    </message>
</context>
<context>
    <name>DialogChooseMultipleBarrierSet_ui</name>
    <message>
        <location filename="../src/Ui/DialogChooseMultipleBarrierSet.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogChooseMultipleBarrierSet.ui" line="20"/>
        <source>Available sets</source>
        <translation>Dostupné sady</translation>
    </message>
</context>
<context>
    <name>DialogChooseMultipleBoat_ui</name>
    <message>
        <location filename="../src/Ui/DialogChooseMultipleBoat.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogChooseMultipleBoat.ui" line="20"/>
        <source>Available boats</source>
        <translation>Dostupné lodě</translation>
    </message>
</context>
<context>
    <name>DialogDownloadTracks</name>
    <message>
        <source>Permet de telecharger manuellement une trace pour une course VLM.
Necessite d&apos;entrer un numero de bateau et un numero de courses valides.</source>
        <translation type="obsolete">Provides a manual download for a VLM race&apos;s track.
Valid boat ID and race ID required.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="26"/>
        <source>Numero de la course
 http://www.virtual-loup-de-mer.org/races.php?fulllist=1</source>
        <translation>Unikátní ID závodu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="27"/>
        <source>Numero du bateau</source>
        <translation>Číslo lodě</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="141"/>
        <source>Course ou bateau inconnu</source>
        <translation>Neznámý závod nebo loď</translation>
    </message>
    <message>
        <source>Ce nom est deja utilise ou invalide</source>
        <translation type="obsolete">This name is already used or invalid</translation>
    </message>
    <message>
        <source>Permet de telecharger manuellement une trace pour une course VLM.
La boîte à cocher trace partielle s&apos;active apres l&apos;entree d&apos;un numero de course valide, et permet de requérir une trace tronquée.</source>
        <translation type="obsolete">Povolit ruční stažení trasy závodu VLM.
La boîte à cocher trace partielle s&apos;active apres l&apos;entree d&apos;un numero de course valide, et permet de requérir une trace tronquée.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="184"/>
        <source>En cache.</source>
        <translation>V keši.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="316"/>
        <source>Trace entiere: pas de nom de route.</source>
        <translation>Celá stopa: chybí jméno trasy.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="359"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="434"/>
        <source>Trace cachee pour:</source>
        <translation>Stopa skryta pro:</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="364"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="441"/>
        <source>Pas de trace</source>
        <translation>Bez stopy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="371"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="446"/>
        <source>Pas de trace correspondant a la requete:</source>
        <translation>Žádná stopa pro:</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="377"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="453"/>
        <source>Requete incorrecte</source>
        <translation>Chybný požadavek</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="391"/>
        <source>Trace partielle: pas de nom de route.</source>
        <translation>Zkrácená stopa: Chybí jméno trasy.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="557"/>
        <source>Erreur de lecture json.</source>
        <translation>Chyba čtení Json.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="28"/>
        <source>Debut de la trace</source>
        <translation>Platný čas začátku stopy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="25"/>
        <source>Permet de telecharger manuellement une trace pour une course VLM.
La bo�te � cocher trace partielle s&apos;active apres l&apos;entree d&apos;un numero de course valide, et permet de requ�rir une trace tronqu�e.</source>
        <translation>Umožňuje manuální download stop závodu VLM. Pole se aktivuje po zadání platného čísla závodu a umožňuje získání zkrácené stopy.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="30"/>
        <source>Fin de la trace</source>
        <translation>Konec stopy</translation>
    </message>
    <message>
        <source>Course inconnue</source>
        <translation type="obsolete">Unknown race</translation>
    </message>
    <message>
        <source>Sauvegarde du fichier JSON</source>
        <translation type="obsolete">Saving json file</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="345"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="420"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="556"/>
        <source>Erreur</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="346"/>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="421"/>
        <source>Ecriture du fichier impossible.</source>
        <translation>Nemohu vytvořit soubor.</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="14"/>
        <source>Banque des Traces VLM</source>
        <translation>VLM banka tras</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="25"/>
        <source>ID Bateau</source>
        <translation>ID lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="57"/>
        <source>Armateur</source>
        <translation>Jméno hráče</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="70"/>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="113"/>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="248"/>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="274"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="106"/>
        <source>Nom Course</source>
        <translation>Jméno závodu</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="219"/>
        <source>Nom de route:</source>
        <translation>Jméno trasy:</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="235"/>
        <source>Location:</source>
        <translation>Místo:</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="261"/>
        <source>Status:</source>
        <translation>Stav:</translation>
    </message>
    <message>
        <source>Les traces au format json sont sauvegardées dans le dossier tracks du répertoire de qtVlm.</source>
        <translation type="obsolete">Tracks are saved in the tracks folder inside the qtVlm directory.</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="123"/>
        <source>Trace Partielle</source>
        <translation>Částečná stopa</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="137"/>
        <source>Heure Debut</source>
        <translation>Čas startu</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="166"/>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="202"/>
        <source>yyyy/MM/dd hh:mm UTC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="176"/>
        <source>Heure Fin</source>
        <translation>Čas konce</translation>
    </message>
    <message>
        <source>Pour eviter de surcharger le serveur VLM, les traces demandees sont sauvegardees localement dans le dossier tracks. Priere de ne les renommer qu&apos;en cas de necessite.</source>
        <translation type="obsolete">To avoid overloading VLM server, downloaded tracks are saved in the tracks folder. Please rename then only if really needed.</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogDownloadTracks.ui" line="77"/>
        <source>ID Course</source>
        <translation>ID závodu</translation>
    </message>
</context>
<context>
    <name>DialogEditBarrier</name>
    <message>
        <location filename="../src/Dialogs/DialogEditBarrier.cpp" line="55"/>
        <source>Barrier set edit</source>
        <translation>Úprava sady bariér</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogEditBarrier.cpp" line="55"/>
        <source>Barrier with same name already exists, choose a different name</source>
        <translation>Bariéra stejného jména již existuje, zvol jiné jméno</translation>
    </message>
</context>
<context>
    <name>DialogEditBarrier_ui</name>
    <message>
        <location filename="../src/Ui/DialogEditBarrier.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogEditBarrier.ui" line="22"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogEditBarrier.ui" line="36"/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
</context>
<context>
    <name>DialogFinePosit</name>
    <message>
        <location filename="../src/Dialogs/DialogFinePosit.cpp" line="35"/>
        <source>Parametres du positionnement automatique</source>
        <translation>Nastavení automatického umísťování</translation>
    </message>
</context>
<context>
    <name>DialogGraphicsParams</name>
    <message>
        <source>Paramètres graphiques</source>
        <translation type="obsolete">Graphics settings</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="178"/>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="188"/>
        <source>Parametres graphiques</source>
        <translation>Nastavení grafiky</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="201"/>
        <source>Valider</source>
        <translation>Potvrdit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="202"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="281"/>
        <source>Couleur de fond :</source>
        <translation>Barva pozadí :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="291"/>
        <source>Couleur des oceans :</source>
        <translation>Barva moří :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="323"/>
        <source>Opacite des zones de nuit :</source>
        <translation>Opacita pro noční zóny :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="346"/>
        <source>Traits de cotes :</source>
        <translation>Pobřeží :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="357"/>
        <source>Frontieres :</source>
        <translation>Hranice :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="368"/>
        <source>Rivieres :</source>
        <translation>Řeky :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="390"/>
        <source>Prochaine porte :</source>
        <translation>Příští aktivní brána :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="401"/>
        <source>Portes suivantes :</source>
        <translation>Další brány :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="413"/>
        <source>Routes :</source>
        <translation>Trasy :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="425"/>
        <source>Trace TWA :</source>
        <translation>TWA stopa :</translation>
    </message>
    <message>
        <source>Couleur des océans :</source>
        <translation type="obsolete">Oceans color :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="301"/>
        <source>Couleur des terres :</source>
        <translation>Barva pevniny :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="311"/>
        <source>Opacite des terres :</source>
        <translation>Průhlednost pevniny :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="335"/>
        <source>Estime :</source>
        <translation>Odhad :</translation>
    </message>
    <message>
        <source>Traits de côtes :</source>
        <translation type="obsolete">Coastlines :</translation>
    </message>
    <message>
        <source>Frontières :</source>
        <translation type="obsolete">Borders :</translation>
    </message>
    <message>
        <source>Rivières :</source>
        <translation type="obsolete">Rivers :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="379"/>
        <source>Isobares :</source>
        <translation>Isobary :</translation>
    </message>
</context>
<context>
    <name>DialogHorn</name>
    <message>
        <location filename="../src/Ui/dialoghorn.ui" line="14"/>
        <source>Parametres de la corne de brume</source>
        <translation>Nastavení mlžného rohu</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialoghorn.ui" line="20"/>
        <source>Activer l&apos;alarme a</source>
        <translation>Aktivovat alarm v</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialoghorn.ui" line="43"/>
        <source>d/M/yyyy hh:mm:ss</source>
        <translation>d/m/yyyy hh:mm:ss</translation>
    </message>
</context>
<context>
    <name>DialogLoadGrib</name>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="47"/>
        <source>Telechargement</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="160"/>
        <source>Sauvegarde du fichier GRIB</source>
        <translation>Uložit GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="179"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="194"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="211"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="428"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="185"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="191"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="207"/>
        <source>Erreur</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="186"/>
        <source>Ecriture du fichier impossible.</source>
        <translation>Nemohu vytvořit soubor.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="192"/>
        <source>Operation abandonnee.</source>
        <translation>Operace zrušena.</translation>
    </message>
    <message>
        <source>Erreur :</source>
        <translation type="obsolete">Error :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="368"/>
        <source>Taille estimee : environ %1 ko</source>
        <translation>Předpokládaná velikost : %1 kb</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="390"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <source> S</source>
        <translation type="obsolete"> S</translation>
    </message>
    <message>
        <source> N</source>
        <translation type="obsolete"> N</translation>
    </message>
    <message>
        <source> W</source>
        <translation type="obsolete"> W</translation>
    </message>
    <message>
        <source>�S</source>
        <translation type="obsolete"> S</translation>
    </message>
    <message>
        <source>�N</source>
        <translation type="obsolete"> N</translation>
    </message>
    <message>
        <source>�W</source>
        <translation type="obsolete"> W</translation>
    </message>
    <message>
        <source>�E</source>
        <translation type="obsolete"> E</translation>
    </message>
    <message>
        <source> E</source>
        <translation type="obsolete"> E</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="580"/>
        <source>Wind (10 m)</source>
        <translation>Vítr (10 m)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="582"/>
        <source>Mean sea level pressure</source>
        <translation>Tlak na hladině moře</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="584"/>
        <source>Total precipitation</source>
        <translation>Celkové srážky</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="586"/>
        <source>Cloud cover</source>
        <translation>Oblačnost</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="588"/>
        <source>Temperature (2 m)</source>
        <translation>Teplota (2 m)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="590"/>
        <source>Relative humidity (2 m)</source>
        <translation>Relativní vlhkost (2 m)</translation>
    </message>
    <message>
        <source>Isotherm 0 C</source>
        <translation type="obsolete">Isotherm 0 C</translation>
    </message>
    <message>
        <source>degS</source>
        <translation type="obsolete"> S</translation>
    </message>
    <message>
        <source>degN</source>
        <translation type="obsolete"> N</translation>
    </message>
    <message>
        <source>degW</source>
        <translation type="obsolete"> W</translation>
    </message>
    <message>
        <source>degE</source>
        <translation type="obsolete"> E</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="208"/>
        <source>Erreur : </source>
        <translation>Chyba :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="452"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="462"/>
        <source> degS</source>
        <translation>°S</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="457"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="467"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="523"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="531"/>
        <source> degN</source>
        <translation>°N</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="472"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="477"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="487"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="492"/>
        <source> degW</source>
        <translation>°W</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="482"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="497"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="539"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="547"/>
        <source> degE</source>
        <translation>°E</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="592"/>
        <source>Isotherm 0degC</source>
        <translation>Isotherma 0°C</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="595"/>
        <source>Potential temperature (sigma 995)</source>
        <translation>Potenciální teplota (sigma 995)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="597"/>
        <source>Temperature min (2 m)</source>
        <translation>Min teplota (2 m)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="599"/>
        <source>Temperature max (2 m)</source>
        <translation>Max teplota (2 m)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="601"/>
        <source>Snow (snowfall possible)</source>
        <translation>Sníh (možné sněžení)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="603"/>
        <source>Frozen rain (rainfall possible)</source>
        <translation>Mrznoucí déšť (možnost deště)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="605"/>
        <source>Snow (depth)</source>
        <translation>Sníh (pokrývka)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="607"/>
        <source>CAPE (surface)</source>
        <translation>CAPE (povrch)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="631"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="634"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="637"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="640"/>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="643"/>
        <source>hPa</source>
        <translation>hPa</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="647"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="656"/>
        <source>Download GRIB file</source>
        <translation>Stáhnout GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="658"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="660"/>
        <source>Server status</source>
        <translation>Stav serveru</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="676"/>
        <source>Latitude min :</source>
        <translation>Min zem. šířka :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="678"/>
        <source>Latitude max :</source>
        <translation>Max zem. šířka :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="681"/>
        <source>Longitude min :</source>
        <translation>Min zem. délka :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="683"/>
        <source>Longitude max :</source>
        <translation>Max zem. délka :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="697"/>
        <source>Resolution :</source>
        <translation>Rozlišení :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="699"/>
        <source> deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="705"/>
        <source> hours</source>
        <translation>hodin</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="711"/>
        <source> days</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="780"/>
        <source>File size max: </source>
        <translation>Max velikost souboru :</translation>
    </message>
    <message>
        <source>deg</source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="703"/>
        <source>Interval :</source>
        <translation>Interval :</translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="obsolete">hours</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="709"/>
        <source>Period :</source>
        <translation>Perioda :</translation>
    </message>
    <message>
        <source>days</source>
        <translation type="obsolete">days</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="752"/>
        <source>Atmosphere: geopotential altitude, wind, temperature, theta-e, relative humidity.</source>
        <translation>Atmosféra: geopotenciální výška, vítr, teplota, theta-e, relativní vlhkost.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogLoadGrib.cpp" line="754"/>
        <source>Warning : these data increase strongly the size of the GRIB file.</source>
        <translation>Varování : tato data velmi zvětšují velikost GRIB souboru.</translation>
    </message>
    <message>
        <source>File size max:</source>
        <translation type="obsolete">File size max:</translation>
    </message>
</context>
<context>
    <name>DialogParamAccount</name>
    <message>
        <source>Identifiant</source>
        <translation type="obsolete">Login</translation>
    </message>
    <message>
        <source>Nom du bateau</source>
        <translation type="obsolete">Boat name</translation>
    </message>
    <message>
        <source>Jmeno</source>
        <translation type="obsolete">Jméno</translation>
    </message>
    <message>
        <source>Lod jmeno</source>
        <translation type="obsolete">Jméno lodi</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="obsolete">Pseudo</translation>
    </message>
</context>
<context>
    <name>DialogParamVlm</name>
    <message>
        <source>Login</source>
        <translation type="obsolete">Pseudo</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="52"/>
        <source>Pseudo</source>
        <translation>Pseudo</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="53"/>
        <source>Nom</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="54"/>
        <source>Numero</source>
        <translation>Číslo</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="90"/>
        <source>If checked, let qtVlm choose the fastest way for your computer</source>
        <translation>Vyber složku map</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="91"/>
        <source>to display the Grib.</source>
        <translation>Měním složku map</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="92"/>
        <source>The result of the benchmark gives </source>
        <translation>Vybraná složka neobsahuje mapy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="92"/>
        <source> ms for multithread against</source>
        <translation>Vybraná složka obsahuje špatnou verzi map</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="93"/>
        <source> ms for monothread.</source>
        <translation> ms pro jednovlákno.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="95"/>
        <source>Therefore the automatic choice will be multithread</source>
        <translation>Proto automatiská volba bude vícevlákno</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="97"/>
        <source>Therefore the automatic choice will be monothread</source>
        <translation>Proto automatická volba bude jednovlákno</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="387"/>
        <source>Select maps folder</source>
        <translation>Vyber složku map</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="398"/>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="401"/>
        <source>Changing maps folder</source>
        <translation>Měním složku map</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="398"/>
        <source>Selected folder doesn&apos;t contain maps</source>
        <translation>Vybraná složka neobsahuje mapy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="401"/>
        <source>Selected folder contains maps with wrong version</source>
        <translation>Vybraná složka obsahuje špatnou verzi map</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="535"/>
        <source>Repertoire Grib</source>
        <translation>Složka Grib</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogParamVlm.cpp" line="544"/>
        <source>Selectionner un skin tableau de bord VLM</source>
        <translation>Vybrat skin přístrojové desky VLM</translation>
    </message>
</context>
<context>
    <name>DialogPilototo</name>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="75"/>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="232"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="76"/>
        <source>Chargement des instructions VLM en cours</source>
        <translation>Instrukce pro stažení VLM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="222"/>
        <source>Pilototo pour </source>
        <translation>Autopilot pro </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="233"/>
        <source>La recuperation des donnees pilototo de VLM n&apos;a pas fonctionne
Vous pouvez ajouter des instructions mais sans voir le resultat dans QtVlm</source>
        <translation>Přenos dat z autopilota VLM se nezdařil. Můžeš přidávat příkazy, ale neuvidíš výsledek v QtVlm</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="320"/>
        <source>Il reste des instructions non validees. Elles ne seront pas envoyees a VLM
Continuer la sauvegarde?</source>
        <translation>Máš neuložené instrukce. Nebudou odeslány do VLM, pokračovat v ukládání?</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="557"/>
        <source>(ddd) dd/MM/yyyy hh:mm:ss</source>
        <translation>(ddd) dd/MM/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="584"/>
        <source>Mise a jour Pilototo</source>
        <translation>Aktualizace autopilota</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="584"/>
        <source>Certains ordres ont des dates trop proches ou dans le passe</source>
        <translation>Některé příkazy mají datum příliš brzy nebo v minulosti</translation>
    </message>
    <message>
        <source>Pilototo pour</source>
        <translation type="obsolete">Autopilot for</translation>
    </message>
    <message>
        <source>La recuperation des donnees pilototo de VLM n&apos;a pas fonctionne Vous pouvez ajouter des instructions mais sans voir le resultat dans QtVlm</source>
        <translation type="obsolete">Data transfer from VLM autopilot did not work. You can add instructions but without seeing the result in QtVlm</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="319"/>
        <source>Instructions non Validees</source>
        <translation>Instrukce nezkontrolovány</translation>
    </message>
    <message>
        <source>Il reste des instructions non validees. Elles ne seront pas envoyees a VLM Continuer la sauvegarde?</source>
        <translation type="obsolete">There are uncommitted instructions. They will not be sent to VLM Continue saving?</translation>
    </message>
    <message>
        <source>La récupération des données pilototo de VLM n&apos;a pas fonctionné Vous pouvez ajouter des instructions mais sans voir le resultat dans QtVlm</source>
        <translation type="obsolete">Data recovery autopilot VLM did not work. You can add instructions but without seeing the result in QtVlm</translation>
    </message>
    <message>
        <source>Instructions non Validées</source>
        <translation type="obsolete">Instructions not validated</translation>
    </message>
    <message>
        <source>Il reste des instructions non validées. Elles ne seront pas envoyées �  VLM Continuer la sauvegarde?</source>
        <translation type="obsolete">There are uncommitted instructions. They will not be sent to VLM Continue saving?</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="663"/>
        <source>instructions</source>
        <translation>instrukce</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="663"/>
        <source>instruction</source>
        <translation>instrukce</translation>
    </message>
</context>
<context>
    <name>DialogPilototoInstruction</name>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="688"/>
        <source>Cap constant (1)</source>
        <translation>Konstantní kurs (1)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="689"/>
        <source>Angle du vent (2)</source>
        <translation>Konstantní úhel větru (2)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="690"/>
        <source>Pilote ortho (3)</source>
        <translation>Orthodromický pilot (3)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="691"/>
        <source>Meilleur VMG (4)</source>
        <translation>Nejlepší VMG (4)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="692"/>
        <source>VBVMG (5)</source>
        <translation>VBVMG (5)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="968"/>
        <source>Passee</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="971"/>
        <source>En-cours</source>
        <translation>Probíhá</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="974"/>
        <source>Nouveau</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototo.cpp" line="977"/>
        <source>Modifie</source>
        <translation>Změnit</translation>
    </message>
</context>
<context>
    <name>DialogPilototoParam</name>
    <message>
        <location filename="../src/Dialogs/DialogPilototoParam.cpp" line="41"/>
        <source>Cap constant (1)</source>
        <translation>Konstantní kurs (1)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototoParam.cpp" line="42"/>
        <source>Angle du vent (2)</source>
        <translation>Konstantní úhel větru (2)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototoParam.cpp" line="43"/>
        <source>Pilote ortho (3)</source>
        <translation>Orthodromický pilot (3)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototoParam.cpp" line="44"/>
        <source>Meilleur VMG (4)</source>
        <translation>Nejlepší VMG (4)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPilototoParam.cpp" line="45"/>
        <source>VBVMG (5)</source>
        <translation>VBVMG (5)</translation>
    </message>
</context>
<context>
    <name>DialogPlayerAccount</name>
    <message>
        <source>Rechargement du compte courant</source>
        <translation type="obsolete">Refresh actual account</translation>
    </message>
    <message>
        <source>Erreur lors du rechargement du compte, le compte courant a disparu, relancez qtVlm</source>
        <translation type="obsolete">Error during account loading, actual account disappeared, restart qtVlm</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="414"/>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="423"/>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="430"/>
        <source>Suppression de compte</source>
        <translation>Smazat účet</translation>
    </message>
    <message>
        <source>Voulez-vous supprimer le compte</source>
        <translation type="obsolete">Do you want to delete this account</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="415"/>
        <source>Ce compte possede un bateau utilise dans une route.&lt;br&gt;Impossible de supprimer ce compte.</source>
        <translation>Tento účet vlastní loď použitou v trase.&lt;br&gt;Tento účet nelze smazat.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="424"/>
        <source>Ce compte possede un bateau utilise dans un routage.&lt;br&gt;Impossible de supprimer ce compte.</source>
        <translation>Tento účet vlastní loď použitou v routingu.&lt;br&gt;Tento účet nelze smazat.</translation>
    </message>
    <message>
        <source>La suppression est definitive&lt;br&gt;Voulez-vous VRAIMENT supprimer le compte</source>
        <translation type="obsolete">Deletion is final&lt;br&gt;Do you REALLY want to delete the account</translation>
    </message>
    <message>
        <source>Mise � jour de compte</source>
        <translation type="obsolete">Updating account</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="479"/>
        <source>Mise a jour de compte</source>
        <translation>Aktualizuji účet</translation>
    </message>
    <message>
        <source>Mise a jour du compte en cours pour</source>
        <translation type="obsolete">Updating account for</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="149"/>
        <source>Mise a jour</source>
        <translation>Aktualizace</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="158"/>
        <source>Bateau reel</source>
        <translation>Skutečná loď</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="159"/>
        <source>Details du compte</source>
        <translation>Detaily účtu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="195"/>
        <source>Novy</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="199"/>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="202"/>
        <source>Jmeno</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="200"/>
        <source>Pocet lodi</source>
        <translation>Počet lodí</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="201"/>
        <source>Sprava uctu</source>
        <translation>Správa účtu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="206"/>
        <source>Skutecna lod</source>
        <translation>Skutečná loď</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="207"/>
        <source>Detaily uctu</source>
        <translatorcomment>Detaily účtu</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="220"/>
        <source>Gestion des comptes</source>
        <translation>Správa účtů</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="221"/>
        <source>Aucun compte cree</source>
        <translation>Účet nebyl vytvořen</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="431"/>
        <source>La suppression est definitive&lt;br&gt;Voulez-vous VRAIMENT supprimer le compte </source>
        <translation>Smazání nelze vrátit&lt;br&gt;Chceš OPRAVDU smazat účet </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="480"/>
        <source>Mise a jour du compte en cours pour </source>
        <translation>Aktualizuji účet pro </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="623"/>
        <source>NO NAME ?</source>
        <translation>NO NAME ?</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="632"/>
        <source>Pas de bateau</source>
        <translation>Žádná loď</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="636"/>
        <source>1 bateau</source>
        <translation>1 loď</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPlayerAccount.cpp" line="638"/>
        <source>bateaux</source>
        <translation>lodě</translation>
    </message>
</context>
<context>
    <name>DialogPoi</name>
    <message>
        <source>Marque :</source>
        <translation type="obsolete">Mark :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPoi.cpp" line="102"/>
        <location filename="../src/Dialogs/DialogPoi.cpp" line="293"/>
        <source>Marque : </source>
        <translation>Značka :</translation>
    </message>
    <message>
        <source>POI</source>
        <translation type="obsolete">POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPoi.cpp" line="97"/>
        <source>Nouvelle marque</source>
        <translation>Nová značka</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPoi.cpp" line="152"/>
        <source>Marque-&gt;WP
(</source>
        <translation>Znaška-&gt;WP (</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPoi.cpp" line="219"/>
        <source>La destruction d&apos;une marque est definitive.

Etes-vous sur ?</source>
        <translation>Smazání značky nelze vrátit. Jsi si jist?</translation>
    </message>
    <message>
        <source>Marque-&gt;WP(</source>
        <translation type="obsolete">Mark-&gt;WP(</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogPoi.cpp" line="218"/>
        <source>Detruire la marque : %1</source>
        <translation>Smazat značku : %1</translation>
    </message>
    <message>
        <source>D�truire la marque : %1</source>
        <translation type="obsolete">Delete mark : %1</translation>
    </message>
    <message>
        <source>La destruction d&apos;une marque est definitive.  Etes-vous sur ?</source>
        <translation type="obsolete">Deleting a mark is final.  Are you sure?</translation>
    </message>
</context>
<context>
    <name>DialogPoiConnect</name>
    <message>
        <location filename="../src/Ui/dialogpoiconnect.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogpoiconnect.ui" line="47"/>
        <source>Taille et couleur</source>
        <translation>Velikost a barva</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogpoiconnect.ui" line="22"/>
        <source>POI a connecter</source>
        <translation>Spojit s POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogpoiconnect.ui" line="55"/>
        <source>Supprimer la ligne</source>
        <translation>Zrušit čáru</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogpoiconnect.cpp" line="16"/>
        <source>Tracer une ligne entre deux POIs</source>
        <translation>Nakreslit čáru mezi 2 POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogpoiconnect.cpp" line="53"/>
        <source>Vous devez selectionner un poi dans la liste</source>
        <translation>Musíš vybrat  POI ze seznamu</translation>
    </message>
</context>
<context>
    <name>DialogProxy</name>
    <message>
        <source>Mode de connexion �  internet</source>
        <translation type="obsolete">Internet connection mode</translation>
    </message>
    <message>
        <source>Valider</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <source>Connexion directe �  internet</source>
        <translation type="obsolete">Direct internet connection</translation>
    </message>
    <message>
        <source>Utilise les parametres de IE</source>
        <translation type="obsolete">Use IE settings</translation>
    </message>
    <message>
        <source>Connexion �  travers un proxy</source>
        <translation type="obsolete">Connection using a proxy</translation>
    </message>
    <message>
        <source>Mode de connexion a internet</source>
        <translation type="obsolete">Internet connection mode</translation>
    </message>
    <message>
        <source>Connexion directe a internet</source>
        <translation type="obsolete">Direct internet connection</translation>
    </message>
    <message>
        <source>Connexion a travers un proxy</source>
        <translation type="obsolete">Connection using a proxy</translation>
    </message>
    <message>
        <source>Serveur de proxy :</source>
        <translation type="obsolete">Proxy server :</translation>
    </message>
    <message>
        <source>Numero de port :</source>
        <translation type="obsolete">Port number :</translation>
    </message>
    <message>
        <source>(* si necessaire)</source>
        <translation type="obsolete">(*  if necessary)</translation>
    </message>
    <message>
        <source>Numéro de port :</source>
        <translation type="obsolete">Port number :</translation>
    </message>
    <message>
        <source>Utilisateur * :</source>
        <translation type="obsolete">User * :</translation>
    </message>
    <message>
        <source>Mot de passe * :</source>
        <translation type="obsolete">Password * :</translation>
    </message>
    <message>
        <source>(* si nécessaire)</source>
        <translation type="obsolete">(* if necessary)</translation>
    </message>
</context>
<context>
    <name>DialogRace</name>
    <message>
        <source>Paramétrage des courses</source>
        <translation type="obsolete">Race settings</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="117"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="283"/>
        <source>Parametrage des courses</source>
        <translation>Nastavení závodu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="118"/>
        <source>Chargement des courses et des bateaux</source>
        <translation>Stáhni závody a lodě</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="284"/>
        <source>Nombre maximum de concurrent depasse</source>
        <translation>Překročen maximální počet závodníků</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="787"/>
        <source>dd MMM yyyy hh:mm:ss</source>
        <translation>dd MMM yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="868"/>
        <source>Filtrer les reels</source>
        <translation>Filtruj skutečné lodě</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="869"/>
        <source>Ids (separes par &apos;;&apos;:</source>
        <translation>Identifikátory (oddělené &apos;;&apos;):</translation>
    </message>
    <message>
        <source>Nombre maximum de concurrent dépassé</source>
        <translation type="obsolete">Maximum number of competitors exceeded</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="407"/>
        <source>Au mouillage</source>
        <translation>Na kotvě</translation>
    </message>
</context>
<context>
    <name>DialogRealBoatConfig</name>
    <message>
        <location filename="../src/Dialogs/DialogRealBoatConfig.cpp" line="62"/>
        <source>&lt;Aucun&gt;</source>
        <translation>&lt;žádný&gt;</translation>
    </message>
</context>
<context>
    <name>DialogRemovePoi</name>
    <message>
        <location filename="../src/Dialogs/DialogRemovePoi.cpp" line="59"/>
        <source>selected POI</source>
        <translation>vybraný POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRemovePoi.cpp" line="75"/>
        <source>Removing POI</source>
        <translation>Odstraňuji POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRemovePoi.cpp" line="76"/>
        <source>Are you sure to remove %1 POI?</source>
        <translation>Určitě chceš odstranit POI %1?</translation>
    </message>
</context>
<context>
    <name>DialogRemovePoi_ui</name>
    <message>
        <location filename="../src/Ui/DialogRemovePoi.ui" line="14"/>
        <source>Remove POI</source>
        <translation>Odstranit POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogRemovePoi.ui" line="26"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogRemovePoi.ui" line="33"/>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogRemovePoi.ui" line="84"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogRemovePoi.ui" line="104"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>DialogRoutage</name>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="72"/>
        <source>Calculer en parallele (</source>
        <translation>Použij multithreading (</translation>
    </message>
    <message>
        <source>processeurs disponibles)</source>
        <translation type="obsolete">cpu available)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="83"/>
        <source>Parametres Routage</source>
        <translation>Nastavení routingu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="309"/>
        <source>Ce nom est deja utilise, choisissez en un autre</source>
        <translation>Jméno už je použito, vyber jiné</translation>
    </message>
    <message>
        <source>Routage:</source>
        <translation type="obsolete">Routing :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="72"/>
        <source> processeurs disponibles)</source>
        <translation> procesorů k dispozici)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="360"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="378"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="384"/>
        <source>Creation d&apos;un routage</source>
        <translation>Vytváření routingu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="361"/>
        <source>Pas de bateau selectionne, pas de routage possible</source>
        <translation>Není zvolena loď, routing není možný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="379"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="385"/>
        <source>Vous ne pouvez pas utiliser le module route&lt;br&gt;avec moins de 4 vacations entre les isochrones.&lt;br&gt;Vous devez donc desactiver cette option ou rallonger la duree</source>
        <translation>Nemůžeš použít routovací modul,&lt;br&gt;pokud je mezi isochronami méně než 4 mezery.&lt;br&gt;Musíš deaktivovat tuto volbu nebo zvětšit časový krok</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="491"/>
        <source>Isochrones inverses</source>
        <translation>Obrácené isochrony</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="492"/>
        <source>Pour l&apos;instant, on ne peut pas calculer les isochrones inverses&lt;br&gt;si la duree des isochrones est variable</source>
        <translation>Momentálně nelze provést výpočet obrácených isochron,&lt;br&gt;pokud byla zvolena jejich proměnlivá délka</translation>
    </message>
    <message>
        <source>Routage: </source>
        <translation type="obsolete">Routing :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="463"/>
        <source>Ce nom de route est deja utilise, veuillez changer le nom du routage</source>
        <translation>Jméno trasy už je použito, vyber jiné</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="474"/>
        <source>Convertir en route</source>
        <translation>Konvertuj na trasu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="475"/>
        <source>Ce routage a ete calcule avec une hypothese modifiant les donnees du grib&lt;br&gt;La route ne prendra pas ce scenario en compte&lt;br&gt;Etes vous sur de vouloir le convertir en route?</source>
        <translation>Tento routing byl proveden s předpokladem zněny dat gribu&lt;br&gt;Trasa nebude brát tento scénář v úvahu&lt;br&gt;Určitě chceš konvertovat na trasu?</translation>
    </message>
</context>
<context>
    <name>DialogRoute</name>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="69"/>
        <source>Parametres Route</source>
        <translation>Nastavení trasy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="150"/>
        <source>Date de la derniere MAJ de la position</source>
        <translation>Poslední aktualizace pozice</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="308"/>
        <source>Chargement du tableau de marche</source>
        <translation>Nahrávání cestovní tabulky</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="309"/>
        <source>Veuillez patienter...</source>
        <translation>Prosím čekej...</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="411"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="418"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="438"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="537"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="538"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="414"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="416"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="420"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="424"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="440"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="453"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="460"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="422"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="451"/>
        <source>kts</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="458"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="535"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="536"/>
        <source> NM</source>
        <translation>NM</translation>
    </message>
    <message>
        <source> jours </source>
        <translation type="obsolete"> dnů </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="706"/>
        <source>Ce nom est deja utilise, choisissez en un autre</source>
        <translation>Jméno už je použito, vyber jiné</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="714"/>
        <source>Vous ne pouvez figer une route que&lt;br&gt;si elle part d&apos;un POI et d&apos;une date fixe</source>
        <translation>Můžeš zmrazit pouze trasu&lt;br&gt; která začíná z POI ve fixním datu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="864"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="913"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="950"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="864"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="913"/>
        <source>Pour utiliser cette action il faut que la route parte du bateau</source>
        <translation>Pro použití této funkce musí trasa začínat na pozici lodě</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="950"/>
        <source>Pour utiliser cette action il faut que:&lt;br&gt;- La route parte du bateau&lt;br&gt;- Le mode VBVMG-VLM soit actif</source>
        <translation>Pro použití této akce je třeba, aby&lt;br&gt;trasa začínala od lodi a VBVMG-VLM bylo aktivní</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1034"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1037"/>
        <source>Exporter un tableau de marche</source>
        <translation>Exportovat tabulku stopy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1057"/>
        <source>Voile/Moteur</source>
        <translation>Plachty/Motor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1073"/>
        <source>Moteur</source>
        <translation>Motor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1075"/>
        <source>Voile</source>
        <translation>Plachty</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Informace</translation>
    </message>
    <message>
        <source>Pour utiliser cette action il faut que:&lt;br&gt;- La route parte du bateau&lt;br&gt;- La route parte de la prochaine vacation&lt;br&gt;- Le mode VBVMG-VLM soit actif</source>
        <translation type="obsolete">To use this action it is needed that:&lt;br&gt;-Route starts from boat&lt;br&gt;-Route starts from next vacation&lt;br&gt;-VBVMG-VLM is activated</translation>
    </message>
    <message>
        <source>Pour utiliser cette action il faut que:&lt;br&gt;- La route parte du bateau&lt;br&gt;-La route parte de la prochaine vacation&lt;br&gt;- Le mode VBVMG-VLM soit actif</source>
        <translation type="obsolete">To use this action it is needed that:&lt;br&gt;-Route starts from boat&lt;br&gt;-Route starts from next vacation&lt;br&gt;VBVMG-VLM is activated</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="987"/>
        <source>WP-VLM</source>
        <translation>WP-VLM</translation>
    </message>
</context>
<context>
    <name>DialogRouteComparator</name>
    <message>
        <location filename="../src/Ui/RouteComparator.ui" line="14"/>
        <source>Routes Comparator</source>
        <translation>Srovnávač tras</translation>
    </message>
    <message>
        <location filename="../src/Ui/RouteComparator.ui" line="23"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="42"/>
        <source>Add a route to comparator</source>
        <translation>Přidat trasu do srovnávače</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="73"/>
        <source>Remove route</source>
        <translation>Odebrat trasu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="73"/>
        <source>from comparator</source>
        <translation>ze srovnávače</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="74"/>
        <source>Delete route</source>
        <translation>Smazat trasu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="148"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="225"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="242"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="259"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="276"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="293"/>
        <source>jours</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="148"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="225"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="242"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="259"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="276"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="293"/>
        <source>heures</source>
        <translation>hodin</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="149"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="226"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="243"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="260"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="277"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="294"/>
        <source>minutes</source>
        <translation>minuty</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="169"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="174"/>
        <source> NM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="179"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="184"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="189"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="194"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="199"/>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="204"/>
        <source> kts</source>
        <translation> kts</translation>
    </message>
</context>
<context>
    <name>DialogTwaLine</name>
    <message>
        <source>ETA:</source>
        <translation type="obsolete">ETA:</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="56"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="74"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="92"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="110"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="128"/>
        <source>Twa</source>
        <translation>Twa</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="62"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="80"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="98"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="116"/>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="134"/>
        <source>Cap</source>
        <translation>Hdg</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogTwaLine.cpp" line="324"/>
        <source>ETA: </source>
        <translation>ETA:</translation>
    </message>
</context>
<context>
    <name>DialogUnits</name>
    <message>
        <source>Unités</source>
        <translation type="obsolete">Units</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="64"/>
        <source>Valider</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="65"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="141"/>
        <source>Vitesse du vent :</source>
        <translation>Rychlost větru :</translation>
    </message>
    <message>
        <source>m/s</source>
        <translation type="obsolete">m/s</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="obsolete">km/h</translation>
    </message>
    <message>
        <source>nœuds</source>
        <translation type="obsolete">knots</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="151"/>
        <source>Distances :</source>
        <translation>Vzdálenosti :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="154"/>
        <source>mille marin</source>
        <translation>námořní míle</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="155"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>Coordonnées :</source>
        <translation type="obsolete">Coordinates :</translation>
    </message>
    <message>
        <source>dd mm&apos;ss&quot;</source>
        <translation type="obsolete">dd mm&apos;ss&quot;</translation>
    </message>
    <message>
        <source>dd mm,mm&apos;</source>
        <translation type="obsolete">dd mm,mm&apos;</translation>
    </message>
    <message>
        <source>dd,dd </source>
        <translation type="obsolete">dd,dd </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="40"/>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="51"/>
        <source>Unites</source>
        <translation>Jednotky</translation>
    </message>
    <message>
        <source>noeuds</source>
        <translation type="obsolete">uzle</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="160"/>
        <source>Coordonnees :</source>
        <translation>Souřadnice :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="163"/>
        <source>dddegmm&apos;ss&quot;</source>
        <translation>dd°mm&apos;ss&quot;</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="164"/>
        <source>dddegmm,mm&apos;</source>
        <translation>dd°mm,mm&apos;</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="165"/>
        <source>dd,dddeg</source>
        <translation>dd,dd°</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="171"/>
        <source>Longitudes :</source>
        <translation>Zem. délky :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="174"/>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="184"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="175"/>
        <source>Est positive</source>
        <translation>Východ kladný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="176"/>
        <source>Ouest positive</source>
        <translation>Západ kladný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="181"/>
        <source>Latitudes :</source>
        <translation>Zem. šířky :</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="185"/>
        <source>Nord positive</source>
        <translation>Sever kladný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogUnits.cpp" line="186"/>
        <source>Sud positive</source>
        <translation>Jih kladný</translation>
    </message>
</context>
<context>
    <name>DialogVLM_grib_ui</name>
    <message>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="14"/>
        <source>VLM grib download</source>
        <translation>Stažení VLM grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="20"/>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="27"/>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="34"/>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="41"/>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="48"/>
        <source>No grib file</source>
        <translation>Žádný soubor grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="70"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogVLM_grib.ui" line="77"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>DialogViewPolar</name>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="50"/>
        <source>Fermer</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="57"/>
        <source>Vitesse vent</source>
        <translation>Rychlost větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="71"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="103"/>
        <source>Best VMG pres</source>
        <translation>Nejlepší VMG proti větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="115"/>
        <location filename="../src/Ui/dialogviewpolar.ui" line="137"/>
        <source>00.0</source>
        <translation>00.0</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="125"/>
        <source>Best VMG portant</source>
        <translation>Nejlepší VMG po větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="96"/>
        <source>Tout afficher</source>
        <translation>Zobraz vše</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogviewpolar.ui" line="173"/>
        <source>Recharger la polaire</source>
        <translation>Znovu nahraj polár</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogviewpolar.cpp" line="70"/>
        <source>Analyse de la polaire: </source>
        <translation>Analýza poláru: </translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogviewpolar.cpp" line="127"/>
        <location filename="../src/Dialogs/dialogviewpolar.cpp" line="129"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
</context>
<context>
    <name>DialogVlmGrib</name>
    <message>
        <location filename="../src/Dialogs/DialogVlmGrib.cpp" line="52"/>
        <source>VLM Grib</source>
        <translation>VLM Grib</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmGrib.cpp" line="53"/>
        <source>Chargement de la liste de grib</source>
        <translation>Stáhni seznam grib souborů</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmGrib.cpp" line="146"/>
        <source>Sauvegarde du fichier GRIB</source>
        <translation>Ulož GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmGrib.cpp" line="164"/>
        <source>Erreur</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmGrib.cpp" line="165"/>
        <source>Ecriture du fichier impossible.</source>
        <translation>Nemohu vytvořit soubor.</translation>
    </message>
</context>
<context>
    <name>DialogVlmLog</name>
    <message>
        <location filename="../src/Ui/DialogVlmLog.ui" line="32"/>
        <source>Dialog</source>
        <translation>Grib SailDocs</translation>
    </message>
    <message>
        <location filename="../src/Ui/DialogVlmLog.ui" line="56"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="21"/>
        <source>Historique VLM</source>
        <translation>VLM Historie</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="144"/>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="147"/>
        <source>Sauvegarde Logs</source>
        <translation>Ulož Logy</translation>
    </message>
</context>
<context>
    <name>DialogWp</name>
    <message>
        <source>Confirmer le changement du WP</source>
        <translation type="obsolete">Potvrď změnu WP</translation>
    </message>
    <message>
        <source>WP change</source>
        <translation type="obsolete">Změň WP</translation>
    </message>
</context>
<context>
    <name>GshhsDwnload</name>
    <message>
        <source>Select maps folder</source>
        <translation type="obsolete">Vyber složku map</translation>
    </message>
    <message>
        <source>Sauvegarde des cartes</source>
        <translation type="obsolete">Záloha map</translation>
    </message>
    <message>
        <source>Le fichier zip </source>
        <translation type="obsolete">Soubor ZIP</translation>
    </message>
    <message>
        <source> ne peut etre dezippe</source>
        <translation type="obsolete">nelze rozbalit</translation>
    </message>
    <message>
        <source>Le fichier </source>
        <translation type="obsolete">Soubor</translation>
    </message>
    <message>
        <source> ne peut etre ouvert</source>
        <translation type="obsolete">nelze otevřít</translation>
    </message>
    <message>
        <location filename="../src/GshhsDwnload.cpp" line="82"/>
        <location filename="../src/GshhsDwnload.cpp" line="118"/>
        <source>Saving maps</source>
        <translation>Ukládám mapy</translation>
    </message>
    <message>
        <location filename="../src/GshhsDwnload.cpp" line="83"/>
        <location filename="../src/GshhsDwnload.cpp" line="119"/>
        <source>Zip file </source>
        <translation>Archiv ZIP</translation>
    </message>
    <message>
        <location filename="../src/GshhsDwnload.cpp" line="83"/>
        <source> can&apos;t be opened</source>
        <translation> nelze otevřít</translation>
    </message>
    <message>
        <location filename="../src/GshhsDwnload.cpp" line="110"/>
        <source>Decompressing maps</source>
        <translation>Rozbaluji mapy</translation>
    </message>
    <message>
        <location filename="../src/GshhsDwnload.cpp" line="119"/>
        <source> can&apos;t be unzip</source>
        <translation> nelze rozbalit</translation>
    </message>
</context>
<context>
    <name>InputColor</name>
    <message>
        <source>Valeurs par défauts</source>
        <translation type="obsolete">Default values</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="80"/>
        <source>Valeurs par defauts</source>
        <translation>Výchozí hodnoty</translation>
    </message>
</context>
<context>
    <name>InputLineParams</name>
    <message>
        <source>Valeurs par défauts</source>
        <translation type="obsolete">Default values</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogGraphicsParams.cpp" line="148"/>
        <source>Valeurs par defauts</source>
        <translation>Výchozí hodnoty</translation>
    </message>
</context>
<context>
    <name>LoadGribFile</name>
    <message>
        <source>Préparation du fichier sur le serveur</source>
        <translation type="obsolete">Preparing file on server</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="153"/>
        <source>Preparation du fichier sur le serveur</source>
        <translation>Příprava souboru na serveru</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="203"/>
        <location filename="../src/LoadGribFile.cpp" line="206"/>
        <source>Emplacement:</source>
        <translation>Umístění:</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="203"/>
        <location filename="../src/LoadGribFile.cpp" line="206"/>
        <source>qtVlm downloads</source>
        <translation>qtVlm ke stažení</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="209"/>
        <source>qtVlm version</source>
        <translation>qtVlm verze</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="201"/>
        <source>Vous n&apos;utilisez pas la derniere version de qtVlm: </source>
        <translation>Nepoužíváš poslední verzi qtVlm: </translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="253"/>
        <source>GetFileContent</source>
        <translation>GetFileContent</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="255"/>
        <source>Taille totale : </source>
        <translation>Celková velikost:</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="314"/>
        <source>Informations sur le serveur zyGrib</source>
        <translation>Stav zyGrib serveru</translation>
    </message>
    <message>
        <source>Taille totale :</source>
        <translation type="obsolete">Global size :</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="273"/>
        <source>Pas de fichier cree sur le serveur:</source>
        <translation>Na serveru nebyl vytvořen soubor:</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="302"/>
        <source>Termine</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <source>Pas de fichier créé sur le serveur:</source>
        <translation type="obsolete">No file created on server:</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="285"/>
        <source>CheckSum control</source>
        <translation>Kontrola CheckSum</translation>
    </message>
    <message>
        <source>Terminé</source>
        <translation type="obsolete">Done</translation>
    </message>
    <message>
        <location filename="../src/LoadGribFile.cpp" line="306"/>
        <source>Checksum incorrect.</source>
        <translation>Checksum nesedí.</translation>
    </message>
</context>
<context>
    <name>Magnifier</name>
    <message>
        <location filename="../src/Magnifier.cpp" line="26"/>
        <source>Change magnifier size</source>
        <translation>Změna velikosti lupou</translation>
    </message>
    <message>
        <location filename="../src/Magnifier.cpp" line="29"/>
        <source>Change magnifier zoom</source>
        <translation>Změna zoom lupou</translation>
    </message>
    <message>
        <location filename="../src/Magnifier.cpp" line="32"/>
        <source>Close magnifier</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../src/Magnifier.cpp" line="201"/>
        <source>New magnifier size</source>
        <translation>Nová velikost lupy</translation>
    </message>
    <message>
        <location filename="../src/Magnifier.cpp" line="201"/>
        <source>magnifier size</source>
        <translation>Velikost lupou</translation>
    </message>
    <message>
        <location filename="../src/Magnifier.cpp" line="213"/>
        <source>New magnifier zoom</source>
        <translation>Nová zoom lupy</translation>
    </message>
    <message>
        <location filename="../src/Magnifier.cpp" line="213"/>
        <source>magnifier zoom</source>
        <translation>Zoom lupy</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Outils</source>
        <translation type="obsolete">Nástroje</translation>
    </message>
    <message>
        <source>Estime</source>
        <translation type="obsolete">Reckoning</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="757"/>
        <location filename="../src/MainWindow.cpp" line="769"/>
        <source>Erreur</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <source>Cartes non trouvées.</source>
        <translation type="obsolete">Maps not found.</translation>
    </message>
    <message>
        <source>Vérifiez l&apos;installation du programme.</source>
        <translation type="obsolete">Check program installation.</translation>
    </message>
    <message>
        <source>qtVlm -</source>
        <translation type="obsolete">qtVlm -</translation>
    </message>
    <message>
        <source>Fichier :</source>
        <translation type="obsolete">File :</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="759"/>
        <location filename="../src/MainWindow.cpp" line="771"/>
        <source>Echec lors de l&apos;ouverture.</source>
        <translation>Chyba během otevírání.</translation>
    </message>
    <message>
        <source>Le fichier ne peut pas être ouvert,</source>
        <translation type="obsolete">File can&apos;t be opened,</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="761"/>
        <source>ou ce n&apos;est pas un fichier GRIB,</source>
        <translation>nebo to není GRIB soubor,</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="762"/>
        <source>ou le fichier est corrompu,</source>
        <translation>nebo je soubor poškozen,</translation>
    </message>
    <message>
        <source>ou il contient des données non reconnues,</source>
        <translation type="obsolete">or it contains unrecognized data,</translation>
    </message>
    <message>
        <source>Cartes non trouvees.</source>
        <translation type="obsolete">Maps not found.</translation>
    </message>
    <message>
        <source>Verifiez l&apos;installation du programme.</source>
        <translation type="obsolete">Zkontroluj instalaci programu.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="760"/>
        <source>Le fichier ne peut pas etre ouvert,</source>
        <translation>Soubor nelze otevřít,</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="763"/>
        <source>ou il contient des donnees non reconnues,</source>
        <translation>nebo obsahuje nerozeznatelná data,</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="764"/>
        <source>ou...</source>
        <translation>nebo...</translation>
    </message>
    <message>
        <source>qtVlm</source>
        <translation type="obsolete">qtVlm</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="933"/>
        <source>A propos</source>
        <translation>O programu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="934"/>
        <source>qtVlm : GUI pour Virtual loup de mer</source>
        <translation>qtVlm : GUI pro Virtual loup de mer</translation>
    </message>
    <message>
        <source>Version :</source>
        <translation type="obsolete">Version :</translation>
    </message>
    <message>
        <source> Estime </source>
        <translation type="obsolete">Odhad</translation>
    </message>
    <message>
        <source>Si cette option est cochee&lt;br&gt;l&apos;estime calcule la vitesse du bateau&lt;br&gt;a la prochaine vac.&lt;br&gt;Sinon elle utilise la vitesse du bateau&lt;br&gt;telle que donnee par VLM</source>
        <translation type="obsolete">Pokud je tato volba zaškrtnuta&lt;br&gt;rychlost lodi je určena odhadem&lt;br&gt;v příští mezeře.&lt;br&gt;Jinak je použita rychlost dodaná VLM</translation>
    </message>
    <message>
        <source>Cartes non trouvees.

</source>
        <translation type="obsolete">Mapy nenalezeny.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="800"/>
        <location filename="../src/MainWindow.cpp" line="811"/>
        <source> (du </source>
        <translation> (z </translation>
    </message>
    <message>
        <source>dd/MM/yyyy hh:mm:ss</source>
        <translation type="obsolete">dd/MM/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="801"/>
        <location filename="../src/MainWindow.cpp" line="812"/>
        <source> au </source>
        <translation> do </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="758"/>
        <location filename="../src/MainWindow.cpp" line="770"/>
        <source>Fichier : </source>
        <translation>Soubor :</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="937"/>
        <source>Version : </source>
        <translation>Verze : </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="939"/>
        <source>Licence : GNU GPL v3</source>
        <translation>Licence : GNU GPL v3</translation>
    </message>
    <message>
        <source>http://qtvlm.sf.net</source>
        <translation type="obsolete">http://qtvlm.sf.net</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="942"/>
        <source>Grib part is originaly from zygrib project</source>
        <translation>Část Grib je původně z projektu zygrib</translation>
    </message>
    <message>
        <source>http://www.zygrib.org</source>
        <translation type="obsolete">http://www.zygrib.org</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="984"/>
        <location filename="../src/MainWindow.cpp" line="1015"/>
        <source>Fichiers GRIB (*.grb *.grib *.grb.bz2 *.grib.bz2 *.grb.gz *.grib.gz)</source>
        <translation>Soubory GRIB (*.grb *.grib *.grb.bz2 *.grib.bz2 *.grb.gz *.grib.gz)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="985"/>
        <location filename="../src/MainWindow.cpp" line="1016"/>
        <source>;;Autres fichiers (*)</source>
        <translation>;;Jiné soubory (*)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="994"/>
        <location filename="../src/MainWindow.cpp" line="1025"/>
        <source>Choisir un fichier GRIB</source>
        <translation>Vyber GRIB soubor</translation>
    </message>
    <message>
        <source>qtVlm </source>
        <translation type="obsolete">qtVlm </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1169"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1204"/>
        <source>Imp. de lire le pilototo de VLM</source>
        <translation>Nemohu přečíst autopilota VLM</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1218"/>
        <location filename="../src/MainWindow.cpp" line="1220"/>
        <source>Selection d&apos;une marque</source>
        <translation>Vyber značku</translation>
    </message>
    <message>
        <source>Vent</source>
        <translation type="obsolete">Vítr</translation>
    </message>
    <message>
        <source>deg</source>
        <translation type="obsolete">°</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="772"/>
        <source>Ce fichier ne contient pas</source>
        <translation>Tento soubor neobsahuje</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="312"/>
        <source>Initializing status bar</source>
        <translation>Inicializuji stavový řádek</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="316"/>
        <source>Initializing tool bar</source>
        <translation>Inicializuji panel nástrojů</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="340"/>
        <source>Creating board &amp; dialogs</source>
        <translation>Vytvářím plochu a dialogy</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="773"/>
        <source>de donnees Courants</source>
        <translation>data proudů</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="801"/>
        <location filename="../src/MainWindow.cpp" line="802"/>
        <location filename="../src/MainWindow.cpp" line="812"/>
        <location filename="../src/MainWindow.cpp" line="813"/>
        <source>dd/MM/yyyy hh:mm</source>
        <translation>dd/MM/yyyy hh:mm</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="811"/>
        <source> courant: </source>
        <translation> proud:</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1259"/>
        <source>Create new barrier</source>
        <translation>Vytvořit novou bariéru</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1261"/>
        <source>Stop barrier create</source>
        <translation>Ukončit vytvářeni bariéry</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1265"/>
        <source>Barrier not in edit mode</source>
        <translation>Bariéra není v módu úprav</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1301"/>
        <source>Montrer les POIs intermediaires de la route</source>
        <translation>Zobrazit mezilehlé POIs na trase </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2411"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2431"/>
        <source>New set</source>
        <translation>Nová sada</translation>
    </message>
    <message>
        <source> Courant</source>
        <translation type="obsolete">Proud</translation>
    </message>
    <message>
        <source> Vent</source>
        <translation type="obsolete"> Vítr</translation>
    </message>
    <message>
        <source>Starting qtVLM</source>
        <translation type="obsolete">Startuji qtVLM</translation>
    </message>
    <message>
        <source>Unable to write in qtVlm folder</source>
        <translation type="obsolete">Nelze zapisovat do složky qtVlm</translation>
    </message>
    <message>
        <source>Please change the folder permission or</source>
        <translation type="obsolete">Změň práva složky nebo</translation>
    </message>
    <message>
        <source>reinstall qtVlm elsewhere</source>
        <translation type="obsolete">přeinstaluj qtVlm jinam</translation>
    </message>
    <message>
        <source>Please check your installation</source>
        <translation type="obsolete">Prosím, zkontoluj svou instalaci</translation>
    </message>
    <message>
        <source>File &apos;benchmark.grb&apos; cannot be find in img directory</source>
        <translation type="obsolete">Inicializace menu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="302"/>
        <source>Initializing menus</source>
        <translation>Inicializace menu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="307"/>
        <source>Initializing maps drawing</source>
        <translation>Inicializace obrázku map</translation>
    </message>
    <message>
        <source>Initializing toolbars</source>
        <translation type="obsolete">Inicializace nástrojové lišty</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="655"/>
        <source>Opening grib</source>
        <translation>Otevírání grib</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="323"/>
        <source>Creating context menus</source>
        <translation>Výroba kontextových menu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="332"/>
        <source>Loading polars list</source>
        <translation>Nahrávání seznamu polárů</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="336"/>
        <source>Reading boats data</source>
        <translation>Načítání dat lodě</translation>
    </message>
    <message>
        <source>Drawing some</source>
        <translation type="obsolete">Kreslení některých</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="366"/>
        <location filename="../src/MainWindow.cpp" line="380"/>
        <source>Preparing coffee</source>
        <translation>Vaření kafe</translation>
    </message>
    <message>
        <source>Drawing all</source>
        <translation type="obsolete">Kreslení všech</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="410"/>
        <source>Updating player</source>
        <translation>Aktualizace hráče</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="427"/>
        <source>Calling player dialog</source>
        <translation>Volání dialogu hráče</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="453"/>
        <location filename="../src/MainWindow.cpp" line="1935"/>
        <source>Updating boats</source>
        <translation>Aktualizace lodí</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="615"/>
        <source>Calibrating grib display</source>
        <translation>Kalibrace zobrazení grib</translation>
    </message>
    <message>
        <source> kts</source>
        <translation type="obsolete">kts</translation>
    </message>
    <message>
        <source>Selection: </source>
        <translation type="obsolete">Výběr:</translation>
    </message>
    <message>
        <source>  init.dir: %1deg</source>
        <translation type="obsolete">  vých.směr: %1°</translation>
    </message>
    <message>
        <source>dd-MM-yyyy, HH:mm:ss</source>
        <translation type="obsolete">dd-MM-yyyy, HH:mm:ss</translation>
    </message>
    <message>
        <source>Derniere synchro</source>
        <translation type="obsolete">Poslední synchronizace</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1306"/>
        <source>Simplifier la route</source>
        <translation>Zjednodušit trasu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1311"/>
        <source>Optimiser la route</source>
        <translation>Optimizovat trasu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1315"/>
        <source>Copier la route</source>
        <translation>Kopírovat trasu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1319"/>
        <source>Zoom sur la route </source>
        <translation>Zaměřit na trasu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1322"/>
        <source>Supprimer la route</source>
        <translation>Odstranit trasu</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1588"/>
        <source>Boats init</source>
        <translation>Inicializace lodí</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1297"/>
        <source>Editer la route</source>
        <translation>Upravit trasu</translation>
    </message>
    <message>
        <source> Arrivee WP</source>
        <translation type="obsolete">Dosažení WP</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1759"/>
        <source>Votre blocnote a change!</source>
        <translation>Tvůj poznámkový blok se změnil!</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1920"/>
        <source>Updating players</source>
        <translation>Aktualizuji hráče</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1924"/>
        <source>Selecting player</source>
        <translation>Vybírám hráče</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1927"/>
        <source>loading POIs</source>
        <translation>Nahrávám body zájmu</translation>
    </message>
    <message>
        <source> mins </source>
        <translation type="obsolete">min</translation>
    </message>
    <message>
        <source> vacs </source>
        <translation type="obsolete">volna</translation>
    </message>
    <message>
        <source> NM </source>
        <translation type="obsolete">NM</translation>
    </message>
    <message>
        <source> vacs</source>
        <translation type="obsolete">cranks</translation>
    </message>
    <message>
        <source>Selection:</source>
        <translation type="obsolete">Selection :</translation>
    </message>
    <message>
        <source>init.dir: %1deg</source>
        <translation type="obsolete">init.dir: %1 </translation>
    </message>
    <message>
        <source>Vacation de la derniere synchro</source>
        <translation type="obsolete">Last synchronization crank</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1271"/>
        <location filename="../src/MainWindow.cpp" line="1284"/>
        <source>Tirer un cap</source>
        <translation>Udat kurs</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1278"/>
        <source>Arret du cap</source>
        <translation>Zastavit kurs</translation>
    </message>
    <message>
        <source>j</source>
        <translation type="obsolete">d</translation>
    </message>
    <message>
        <source>h</source>
        <translation type="obsolete">h</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="obsolete">m</translation>
    </message>
    <message>
        <source>s</source>
        <translation type="obsolete">s</translation>
    </message>
    <message>
        <source>Arrivee WP</source>
        <translation type="obsolete">WP ETA</translation>
    </message>
    <message>
        <source>vacs</source>
        <translation type="obsolete">cranks</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <source>Sélection:</source>
        <translation type="obsolete">Selection :</translation>
    </message>
    <message>
        <source>(dist.orthodromique:</source>
        <translation type="obsolete">(orthodromická vzdálenost:</translation>
    </message>
    <message>
        <source>init.dir: %1 </source>
        <translation type="obsolete">init.dir: %1 </translation>
    </message>
    <message>
        <source>Vacation de la dernière synchro</source>
        <translation type="obsolete">Timestamp last VLM sync</translation>
    </message>
    <message>
        <source>Prochaine vac dans</source>
        <translation type="obsolete">Další obrat za</translation>
    </message>
    <message>
        <source>Arrivée WP</source>
        <translation type="obsolete">WP ETA</translation>
    </message>
    <message>
        <source>About </source>
        <translation type="obsolete">O programu</translation>
    </message>
    <message>
        <source>&lt;B&gt;</source>
        <translation type="obsolete">&lt;B&gt;</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">U&amp;končit</translation>
    </message>
    <message>
        <source>CTRL+D</source>
        <translation type="obsolete">CTRL+D</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation type="obsolete">Ukončit aplikaci</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="obsolete">O &amp;aplikaci</translation>
    </message>
    <message>
        <source>CTRL+A</source>
        <translation type="obsolete">CTRL+A</translation>
    </message>
    <message>
        <source>About application</source>
        <translation type="obsolete">O aplikaci</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Soubor</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Nápověda</translation>
    </message>
</context>
<context>
    <name>MenuBar</name>
    <message>
        <location filename="../src/MenuBar.cpp" line="52"/>
        <source>QtVlm</source>
        <translation>QtVlm</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="55"/>
        <source>Quitter</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="55"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="55"/>
        <location filename="../src/MenuBar.cpp" line="57"/>
        <source>Bye</source>
        <translation>Čau</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="57"/>
        <source>Quitter sans sauver</source>
        <translation>Ukončit bez uložení</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="57"/>
        <source>Ctrl+Shift+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="60"/>
        <source>Verrouiller</source>
        <translation>Zámek</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="60"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="60"/>
        <source>Verrouiller l&apos;envoi d&apos;ordre a VLM</source>
        <translation>Zamknout/Odemknout posílání příkazů do VLM</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="62"/>
        <source>Gestion des fichiers KAP</source>
        <translation>Správa KAP souborů</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="63"/>
        <source>Ouvrir un fichier KAP</source>
        <translation>Otevřít KAP soubor</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="65"/>
        <source>Fermer le fichier KAP</source>
        <translation>Zavřít KAP soubor</translation>
    </message>
    <message>
        <source>Montrer/cacher</source>
        <translation type="obsolete">Ukaž/Schovej</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="85"/>
        <source>Tout montrer</source>
        <translation>Ukaž vše</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="86"/>
        <source>Tout cacher sauf les bateaux actifs</source>
        <translation>Schovej vše kromě aktivních lodí</translation>
    </message>
    <message>
        <source>Cacher/Montrer les bateaux opposants</source>
        <translation type="obsolete">Ukaž/Schovej lodě protivníků</translation>
    </message>
    <message>
        <source>Cacher/Montrer les portes et WPs</source>
        <translation type="obsolete">Ukaž/Schovej brány a WP</translation>
    </message>
    <message>
        <source>Cacher/Montrer les POIs</source>
        <translation type="obsolete">Ukaž/Schovej POI</translation>
    </message>
    <message>
        <source>Cacher/Montrer les routes</source>
        <translation type="obsolete">Ukaž/Schovej trasy</translation>
    </message>
    <message>
        <source>Cacher/Montrer les routages</source>
        <translation type="obsolete">Show/Hide routings</translation>
    </message>
    <message>
        <source>Cacher/Montrer les etiquettes</source>
        <translation type="obsolete">Ukaž/Schovej popisky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="108"/>
        <source>Cacher/Montrer le compas</source>
        <translation>Ukaž/Schovej kompas</translation>
    </message>
    <message>
        <source>Cacher/Montrer le compas du bandeau</source>
        <translation type="obsolete">Ukaž/Schovej kompas VLM</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="110"/>
        <source>Cacher/Montrer la polaire</source>
        <translation>Ukaž/Schovej rychlostní polár</translation>
    </message>
    <message>
        <source>Cacher/Montrer les pavillons sur la carte</source>
        <translation type="obsolete">Ukaž Schovej vlajky lodí na mapě</translation>
    </message>
    <message>
        <source>Cacher/Montrer les zones de jour et nuit</source>
        <translation type="obsolete">Zobrazit/Skrýt noc zóna</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="89"/>
        <source>Centrer sur le bateau actif</source>
        <translation>Centruj na aktivní loď</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="72"/>
        <source>Configurer la corne de brume</source>
        <translation>Konfiguruj mlžný roh</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="90"/>
        <source>Conserver la position du bateau dans l&apos;ecran lors de zoom +/-</source>
        <translation>Zamkni pozici lodi na obrazovce během zoomování</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="73"/>
        <source>Rejouer l&apos;historique des traces</source>
        <translation>Přehraj historii stop</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="74"/>
        <source>Photo d&apos;ecran</source>
        <translation>Snímek obrazovky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="78"/>
        <source>View</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <source>Board</source>
        <translation type="obsolete">Deska</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="82"/>
        <source>ToolBar</source>
        <translation>Nástrojová lišta</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="88"/>
        <source>Show/Hide boat and POI</source>
        <translation>Ukaž/skryj loď a POI</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="93"/>
        <source>Montrer les pavillons sur la carte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="96"/>
        <source>Montrer les bateaux opposants</source>
        <translation>Zobrazit soupeřící lodě</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="98"/>
        <source>Montrer les portes et WPs</source>
        <translation>Zobrazit přístavy a WP</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="100"/>
        <source>Montrer les POIs</source>
        <translation>Zorazit POI</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="102"/>
        <source>Montrer les routes</source>
        <translation>Zobrazit trasy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="104"/>
        <source>Montrer les etiquettes</source>
        <translation>Zobrazit nálepky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="107"/>
        <source>Show/Hide compas</source>
        <translation>Zobrazit/skrýt kompas</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="117"/>
        <source>Montrer les zones de jour et nuit</source>
        <translation>Zobrazit zóny dne a noci</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="123"/>
        <source>Fichier GRIB</source>
        <translation>GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="124"/>
        <source>Ouvrir</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="125"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="126"/>
        <source>Ouvrir un fichier GRIB</source>
        <translation>Otevřít GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="127"/>
        <source>Recharcher</source>
        <translation>Vyhledat</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="129"/>
        <source>Recharger le fichier GRIB actuel</source>
        <translation>Znovu načíst současný GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="130"/>
        <location filename="../src/MenuBar.cpp" line="132"/>
        <source>Fermer</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="131"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>Téléchargement</source>
        <translation type="obsolete">Download</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="134"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <source>Téléchargement VLM</source>
        <translation type="obsolete">Download from VLM</translation>
    </message>
    <message>
        <source>Téléchargement SailsDoc</source>
        <translation type="obsolete">Download from SailDocs</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="150"/>
        <source>Informations sur le fichier</source>
        <translation>Informace o souboru</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="151"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="152"/>
        <source>Informations sur le fichier GRIB</source>
        <translation>Informace o GRIB souboru</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="155"/>
        <source>Type de carte</source>
        <translation>Typ mapy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="157"/>
        <source>Carte du vent</source>
        <translation>Mapa větru</translation>
    </message>
    <message>
        <source>Carte des précipitations</source>
        <translation type="obsolete">Precipitations map</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="160"/>
        <source>Couverture nuageuse</source>
        <translation>Oblačnost</translation>
    </message>
    <message>
        <source>Carte de l&apos;humidité relative</source>
        <translation type="obsolete">Map relative humidity</translation>
    </message>
    <message>
        <source>Carte de la température</source>
        <translation type="obsolete">Map temperature</translation>
    </message>
    <message>
        <source>Carte de la température potentielle</source>
        <translation type="obsolete">Map potential temperature</translation>
    </message>
    <message>
        <source>Ecart température-point de rosée</source>
        <translation type="obsolete">Deviation temperature-dew point</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="165"/>
        <source>Neige (chute possible)</source>
        <translation>Sníh (možné sněžení)</translation>
    </message>
    <message>
        <source>Neige (épaisseur)</source>
        <translation type="obsolete">Snow (depth)</translation>
    </message>
    <message>
        <source>Pluie verglaçante (chute possible)</source>
        <translation type="obsolete">Icy Rain (possible)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="133"/>
        <location filename="../src/MenuBar.cpp" line="135"/>
        <source>Telechargement</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="136"/>
        <location filename="../src/MenuBar.cpp" line="138"/>
        <source>Telechargement VLM</source>
        <translation>Stáhnout z VLM</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="139"/>
        <location filename="../src/MenuBar.cpp" line="141"/>
        <source>Telechargement SailsDoc</source>
        <translation>Stáhnout ze SailDocs</translation>
    </message>
    <message>
        <source>Carte des pre�cipitations</source>
        <translation type="obsolete">Precipitations map</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="159"/>
        <source>Carte des preecipitations</source>
        <translation>Mapa srážek</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="161"/>
        <source>Carte de l&apos;humidite relative</source>
        <translation>Mapa relativní vlhkosti</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="162"/>
        <source>Carte de la temperature</source>
        <translation>Mapa teploty</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="163"/>
        <source>Carte de la temperature potentielle</source>
        <translation>Map potenciální teploty</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="164"/>
        <source>Ecart temperature-point de rosee</source>
        <translation>Deviace teplota-rosný bod</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="166"/>
        <source>Neige (Epaisseur)</source>
        <translation>Sníh (pokrývka)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="167"/>
        <source>Pluie verglacante (chute possible)</source>
        <translation>Mrznoucí déšŤ (možný)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="168"/>
        <source>CAPE (surface)</source>
        <translation>CAPE (povrch)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="187"/>
        <source>Altitude</source>
        <translation>Nadm. výška</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="189"/>
        <source>Sea level</source>
        <translation>Hladina moře</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="190"/>
        <source>Surface</source>
        <translation>Povrch</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="191"/>
        <source>Sigma 995</source>
        <translation>Sigma 995</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="192"/>
        <source>1 m above ground</source>
        <translation>1 m nad zemí</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="193"/>
        <source>2 m above ground</source>
        <translation>2 m nad zemí</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="194"/>
        <source>3 m above ground</source>
        <translation>3 m nad zemí</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="195"/>
        <source>10 m above ground</source>
        <translation>10 m nad zemí</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="196"/>
        <source>850 hPa (? 1460 m)</source>
        <translation>850 hPa (? 1460 m)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="197"/>
        <source>700 hPa (? 3000 m)</source>
        <translation>700 hPa (? 3000 m)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="198"/>
        <source>500 hPa (? 5600 m)</source>
        <translation>500 hPa (? 5600 m)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="199"/>
        <source>300 hPa (? 9200 m)</source>
        <translation>300 hPa (? 9200 m</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="200"/>
        <source>200 hPa (? 11800 m)</source>
        <translation>200 hPa (? 11800 m)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="201"/>
        <source>Atmosphere</source>
        <translation>Atmosféra</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="217"/>
        <source>Geopotential altitude 850 hpa</source>
        <translation>Geopotenciální výška 850 hpa</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="218"/>
        <source>Geopotential altitude 700 hpa</source>
        <translation>Geopotenciální výška 700 hpa</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="219"/>
        <source>Geopotential altitude 500 hpa</source>
        <translation>Geopotenciální výška 500 hpa</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="220"/>
        <source>Geopotential altitude 300 hpa</source>
        <translation>Geopotenciální výška 300 hpa</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="221"/>
        <source>Geopotential altitude 200 hpa</source>
        <translation>Geopotenciální výška 200 hpa</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="228"/>
        <source>Spacing (m)</source>
        <translation>Krok (m)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="230"/>
        <location filename="../src/MenuBar.cpp" line="271"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="231"/>
        <location filename="../src/MenuBar.cpp" line="272"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="232"/>
        <location filename="../src/MenuBar.cpp" line="275"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="233"/>
        <location filename="../src/MenuBar.cpp" line="278"/>
        <location filename="../src/MenuBar.cpp" line="303"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="234"/>
        <location filename="../src/MenuBar.cpp" line="304"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="235"/>
        <location filename="../src/MenuBar.cpp" line="305"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="236"/>
        <location filename="../src/MenuBar.cpp" line="306"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="245"/>
        <source>Geopotentials labels</source>
        <translation>Popisky geopotenciálů</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="250"/>
        <source>Degrades de couleurs</source>
        <translation>Gradienty barev</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="253"/>
        <source>Fleches du vent</source>
        <translation>Větrné šipky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="254"/>
        <source>Afficher les fleches de direction du vent</source>
        <translation>Ukaž větrné šipky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="257"/>
        <source>Afficher les barbules sur les fleches de vent</source>
        <translation>Ukaž &quot;opeření&quot; na větrných šipkách</translation>
    </message>
    <message>
        <source>Afficher les Ã©tiquettes des isobares</source>
        <translation type="obsolete">Připojit popisky isobar</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="297"/>
        <location filename="../src/MenuBar.cpp" line="298"/>
        <source>Isothermes 0degC</source>
        <translation>Isothermy 0°C</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="299"/>
        <source>Afficher les isothermes 0degC</source>
        <translation>Ukaž isothermy 0°C </translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="303"/>
        <location filename="../src/MenuBar.cpp" line="304"/>
        <location filename="../src/MenuBar.cpp" line="305"/>
        <location filename="../src/MenuBar.cpp" line="306"/>
        <location filename="../src/MenuBar.cpp" line="307"/>
        <location filename="../src/MenuBar.cpp" line="308"/>
        <location filename="../src/MenuBar.cpp" line="309"/>
        <source>Espacement des isothermes 0degC</source>
        <translation>Krok isotherm 0°C</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="320"/>
        <source>Etiquettes des isothermes 0degC</source>
        <translation>Poipisky isotherm 0°C</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="321"/>
        <source>Afficher les étiquettes des isothermes 0degC</source>
        <translation>Ukázat popisky isotherm 0°C</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="325"/>
        <source>Fax meteo</source>
        <translation>Meteo fax</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="326"/>
        <source>Ouvrir un fax meteo</source>
        <translation>Otevřít meteo fax</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="328"/>
        <source>Fermer le fax meteo</source>
        <translation>Zavřít meteo fax</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="340"/>
        <source>Parametres du/des bateaux</source>
        <translation>Nastavení lodí</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="341"/>
        <source>Parametres des courses</source>
        <translation>Nastavení závodů</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="344"/>
        <source>Historique VLM</source>
        <translation>Historie VLM</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="345"/>
        <source>Telecharger trace</source>
        <translation>Stáhni stopu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="346"/>
        <source>Etudier la polaire</source>
        <translation>Polární analýza</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="354"/>
        <source>Creer une route</source>
        <translation>Vytvoř trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="362"/>
        <source>En mode VB-VMG</source>
        <translation>V módu VB-VMG</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="364"/>
        <source>En mode Ortho</source>
        <translation>V módu Ortho</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="369"/>
        <source>Routes comparator</source>
        <translation>Srovnávač tras</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="376"/>
        <source>Creer un routage</source>
        <translation>Vytvoř routing</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="388"/>
        <source>Sauvegarder POIs et routes</source>
        <translation>Ulož POI a trasy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="389"/>
        <source>Recharger POIs et routes</source>
        <translation>Znovu načti POI a trasy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="393"/>
        <source>Importer un fichier GeoData</source>
        <translation>Importuj GeoData soubor</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="396"/>
        <source>Supprimer des marques</source>
        <translation>Odstranit značky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="397"/>
        <source>Remove marks by type...</source>
        <translation>Odebrat značky podle typu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="400"/>
        <source>Barrier set</source>
        <translation>Sada bariér</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="403"/>
        <source>Add barrier set</source>
        <translation>Přidat sadu bariér</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="404"/>
        <source>Enable editting</source>
        <translation>POvolit úpravy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="406"/>
        <source>Add barrier</source>
        <translation>Přidat bariéru</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="408"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="410"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="420"/>
        <source>Unites</source>
        <translation>Jednotky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="422"/>
        <source>Parametres graphiques</source>
        <translation>Nastavení grafiky</translation>
    </message>
    <message>
        <source>Parametres VLM</source>
        <translation type="obsolete">VLM settings</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="426"/>
        <source>Planisphere</source>
        <translation>Planisféra</translation>
    </message>
    <message>
        <source>Qualite 1</source>
        <translation type="obsolete">Quality 1</translation>
    </message>
    <message>
        <source>Niveau de detail de la carte</source>
        <translation type="obsolete">Map detail level</translation>
    </message>
    <message>
        <source>Qualite 2</source>
        <translation type="obsolete">Quality 2</translation>
    </message>
    <message>
        <source>Qualite 3</source>
        <translation type="obsolete">Quality 3</translation>
    </message>
    <message>
        <source>Qualite 4</source>
        <translation type="obsolete">Quality 4</translation>
    </message>
    <message>
        <source>Qualite 5</source>
        <translation type="obsolete">Quality 5</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="431"/>
        <source>Frontieres</source>
        <translation>Hranice</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="431"/>
        <source>Afficher les frontieres</source>
        <translation>Zobraz hranice</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="433"/>
        <source>Rivieres</source>
        <translation>Řeky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="433"/>
        <source>Afficher les rivieres</source>
        <translation>Zobraz řeky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="457"/>
        <source>Francais</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="459"/>
        <source>Spanish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="460"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="487"/>
        <source>QtVlm forum</source>
        <translation>Fórum QtVlm</translation>
    </message>
    <message>
        <source>Augmenter l&apos;echelle de la carte</source>
        <translation type="obsolete">Přibliž mapu</translation>
    </message>
    <message>
        <source>Diminuer l&apos;echelle de la carte</source>
        <translation type="obsolete">Oddal mapu</translation>
    </message>
    <message>
        <source>Zoom (selection ou fichier Grib)</source>
        <translation type="obsolete">Zoom (výběr nebo Grib soubor)</translation>
    </message>
    <message>
        <source>Zoomer sur la zone selectionnee ou sur la surface du fichier Grib</source>
        <translation type="obsolete">Přibliž na vybrané území nebo rozměr Grib souboru</translation>
    </message>
    <message>
        <source>Afficher la carte entiere</source>
        <translation type="obsolete">Ukaž celou mapu</translation>
    </message>
    <message>
        <source>Prevision precedente [page prec]</source>
        <translation type="obsolete">Předchozí předpověď [minulá strana]</translation>
    </message>
    <message>
        <source>Dégradés de couleurs</source>
        <translation type="obsolete">Color gradients</translation>
    </message>
    <message>
        <source>Flèches du vent</source>
        <translation type="obsolete">Wind arrows</translation>
    </message>
    <message>
        <source>Afficher les flèches de direction du vent</source>
        <translation type="obsolete">Show wind arrows</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="256"/>
        <source>Barbules</source>
        <translation>Opeření</translation>
    </message>
    <message>
        <source>Afficher les barbules sur les flèches de vent</source>
        <translation type="obsolete">Show barbs on wind arrows</translation>
    </message>
    <message>
        <source>Température</source>
        <translation type="obsolete">Temperature</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="261"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="261"/>
        <source>Temperature</source>
        <translation>Teplota</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="266"/>
        <source>Isobares</source>
        <translation>Isobary</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="267"/>
        <source>Afficher les isobares</source>
        <translation>Zobraz isobary</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="269"/>
        <source>Espacement (hPa)</source>
        <translation>Krok (hPa)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="271"/>
        <location filename="../src/MenuBar.cpp" line="272"/>
        <location filename="../src/MenuBar.cpp" line="273"/>
        <location filename="../src/MenuBar.cpp" line="274"/>
        <location filename="../src/MenuBar.cpp" line="275"/>
        <location filename="../src/MenuBar.cpp" line="276"/>
        <location filename="../src/MenuBar.cpp" line="277"/>
        <location filename="../src/MenuBar.cpp" line="278"/>
        <source>Espacement des isobares</source>
        <translation>Krok isobar</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="273"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="274"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="276"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="277"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="288"/>
        <source>Etiquettes des isobares</source>
        <translation>Popisky isobar</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="289"/>
        <source>Afficher les étiquettes des isobares</source>
        <translation>Zobrazit popisky isobar</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="291"/>
        <source>Pression Mini(L) Maxi(H)</source>
        <translation>Tlak Mini(L) Maxi(H)</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="292"/>
        <source>Afficher les points de pression mini et maxi</source>
        <translation>Zobraz body minimálního a maximálního tlaku</translation>
    </message>
    <message>
        <source>Isothermes 0 C</source>
        <translation type="obsolete">Isotherms 0 C</translation>
    </message>
    <message>
        <source>Afficher les isothermes 0 C</source>
        <translation type="obsolete">Show isotherms 0C </translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="301"/>
        <source>Espacement (m)</source>
        <translation>Krok (m)</translation>
    </message>
    <message>
        <source>Espacement des isothermes 0 C</source>
        <translation type="obsolete">Isotherm 0 C spacing</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="307"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="308"/>
        <source>500</source>
        <translation>500</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="309"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <source>Etiquettes des isothermes 0 C</source>
        <translation type="obsolete">Isotherm 0 C labels</translation>
    </message>
    <message>
        <source>Afficher les étiquettes des isothermes 0 C</source>
        <translation type="obsolete">Show isotherm 0 C labels</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="335"/>
        <source>Bateau</source>
        <translation>Loď</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="336"/>
        <source>Gestion des comptes</source>
        <translation>Správa účtů</translation>
    </message>
    <message>
        <source>Paramètres du/des bateaux</source>
        <translation type="obsolete">Boat(s) settings</translation>
    </message>
    <message>
        <source>Paramètres des courses</source>
        <translation type="obsolete">Race settings</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="342"/>
        <source>VLM Sync</source>
        <translation>VLM Sync</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="343"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="351"/>
        <source>Routes</source>
        <translation>Trasy</translation>
    </message>
    <message>
        <source>Créer une route</source>
        <translation type="obsolete">Create route</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="355"/>
        <source>Supprimer une route</source>
        <translation>Smaž trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="356"/>
        <source>Editer une route</source>
        <translation>Uprav trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="357"/>
        <source>Exporter une route</source>
        <translation>Exportuj trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="360"/>
        <source>Importer une route</source>
        <translation>Importuj trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="374"/>
        <source>Routages</source>
        <translation>Routingy</translation>
    </message>
    <message>
        <source>Créer un routage</source>
        <translation type="obsolete">Create a routing</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="377"/>
        <source>Supprimer un routage</source>
        <translation>Smaž routing</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="378"/>
        <source>Editer un routage</source>
        <translation>Uprav routing</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="386"/>
        <source>Marques</source>
        <translation>Značky</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="387"/>
        <source>Ajout en masse</source>
        <translation>Přidat najednou</translation>
    </message>
    <message>
        <source>Sauvegarder</source>
        <translation type="obsolete">Save</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="391"/>
        <source>Importer</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="392"/>
        <source>Importer de zyGrib</source>
        <translation>Importuj ze zyGrib</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="395"/>
        <source>Ajouter une marque</source>
        <translation>Přidat značku</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="418"/>
        <source>Options</source>
        <translation>Možnosti</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="419"/>
        <source>Proxy Internet</source>
        <translation>Internet proxy</translation>
    </message>
    <message>
        <source>Unités</source>
        <translation type="obsolete">Units</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="420"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <source>Paramètres graphiques</source>
        <translation type="obsolete">Graphical settings</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="422"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <source>Paramètres VLM</source>
        <translation type="obsolete">VLM settings</translation>
    </message>
    <message>
        <source>Ctrl+V</source>
        <translation type="obsolete">Ctrl+V</translation>
    </message>
    <message>
        <source>Planisphère</source>
        <translation type="obsolete">Planisphere</translation>
    </message>
    <message>
        <source>Qualité 1</source>
        <translation type="obsolete">Quality 1</translation>
    </message>
    <message>
        <source>Niveau de détail de la carte</source>
        <translation type="obsolete">Map detail level</translation>
    </message>
    <message>
        <source>Qualité 2</source>
        <translation type="obsolete">Quality 2</translation>
    </message>
    <message>
        <source>Qualité 3</source>
        <translation type="obsolete">Quality 3</translation>
    </message>
    <message>
        <source>Qualité 4</source>
        <translation type="obsolete">Quality 4</translation>
    </message>
    <message>
        <source>Qualité 5</source>
        <translation type="obsolete">Quality 5</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="427"/>
        <source>Distance orthodromique</source>
        <translation>Orthodromická vzdálenost</translation>
    </message>
    <message>
        <source>Frontières</source>
        <translation type="obsolete">Borders</translation>
    </message>
    <message>
        <source>Afficher les frontières</source>
        <translation type="obsolete">Show borders</translation>
    </message>
    <message>
        <source>Rivières</source>
        <translation type="obsolete">Rivers</translation>
    </message>
    <message>
        <source>Afficher les rivières</source>
        <translation type="obsolete">Show rivers</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="435"/>
        <source>Noms des pays</source>
        <translation>Názvy států</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="435"/>
        <source>Afficher les noms des pays</source>
        <translation>Zobraz názvy států</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="439"/>
        <source>Nom des villes</source>
        <translation>Názvy měst</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="441"/>
        <source>Aucun</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="442"/>
        <source>Niveau 1</source>
        <translation>Úroveň 1</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="443"/>
        <source>Niveau 2</source>
        <translation>Úroveň 2</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="444"/>
        <source>Niveau 3</source>
        <translation>Úroveň 3</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="445"/>
        <source>Niveau 4</source>
        <translation>Úroveň 4</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="455"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <source>Français</source>
        <translation type="obsolete">Français</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="458"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="483"/>
        <location filename="../src/MenuBar.cpp" line="484"/>
        <source>Aide</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="484"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="485"/>
        <source>A propos de qtVlm</source>
        <translation>O qtVlm</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="486"/>
        <source>A propos de QT</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <source>Augmenter l&apos;échelle de la carte</source>
        <translation type="obsolete">Increase map scale</translation>
    </message>
    <message>
        <source>Diminuer l&apos;échelle de la carte</source>
        <translation type="obsolete">Reduce map scale</translation>
    </message>
    <message>
        <source>Zoom (sélection ou fichier Grib)</source>
        <translation type="obsolete">Zoom (selection or Grib file)</translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation type="obsolete">Ctrl+Z</translation>
    </message>
    <message>
        <source>Zoomer sur la zone sélectionnée ou sur la surface du fichier Grib</source>
        <translation type="obsolete">Zoom on selected zone or Grib surface</translation>
    </message>
    <message>
        <source>Afficher la carte entière</source>
        <translation type="obsolete">Show entire map</translation>
    </message>
    <message>
        <source>Prévision précédente [page préc]</source>
        <translation type="obsolete">Previous forecast [prev page]</translation>
    </message>
    <message>
        <source>PgUp</source>
        <translation type="obsolete">PgUp</translation>
    </message>
    <message>
        <source>Prévision suivante [page suiv]</source>
        <translation type="obsolete">Next forecast [next page]</translation>
    </message>
    <message>
        <source>PgDown</source>
        <translation type="obsolete">PgDown</translation>
    </message>
    <message>
        <source>Prevision suivante [page suiv]</source>
        <translation type="obsolete">Příští předpověď [další strana]</translation>
    </message>
    <message>
        <source>Now</source>
        <translation type="obsolete">Teď</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Vybrat</translation>
    </message>
    <message>
        <source>15 m</source>
        <translation type="obsolete">15 m</translation>
    </message>
    <message>
        <source>30 m</source>
        <translation type="obsolete">30 m</translation>
    </message>
    <message>
        <source>1 h</source>
        <translation type="obsolete">1 h</translation>
    </message>
    <message>
        <source>2 h</source>
        <translation type="obsolete">2 h</translation>
    </message>
    <message>
        <source>3 h</source>
        <translation type="obsolete">3 h</translation>
    </message>
    <message>
        <source>6 h</source>
        <translation type="obsolete">6 h</translation>
    </message>
    <message>
        <source>12 h</source>
        <translation type="obsolete">12 h</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="503"/>
        <source>Positionner une nouvelle Marque</source>
        <translation>Umístit novou značku</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="504"/>
        <source>Coller une marque</source>
        <translation>Vlepit značku</translation>
    </message>
    <message>
        <source>Effacer toutes les marques</source>
        <translation type="obsolete">Smazat všechny značky</translation>
    </message>
    <message>
        <source>Effacer les marques...</source>
        <translation type="obsolete">Smazat značky...</translation>
    </message>
    <message>
        <source>Rendre toutes les marques non-simplifiables</source>
        <translation type="obsolete">Udělat všechny značky nezjednodušitelné</translation>
    </message>
    <message>
        <source>Rendre toutes les marques simplifiables</source>
        <translation type="obsolete">Udělat všechny značky zjednodušitelné</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="510"/>
        <source>Tracer une estime TWA</source>
        <translation>Kreslit očekávaný TWA</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="511"/>
        <source>Tirer un cap</source>
        <translation>Kreslit kurs</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="513"/>
        <source>Centrer le compas sur le bateau actif</source>
        <translation>Vycentrovat kompas na aktivní loď</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="516"/>
        <source>Centrer le compas sur le WP VLM</source>
        <translation>Vycentrovat kompas na VLM WP</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="517"/>
        <source>Centrer le compass sur l&apos;interpolation de la route</source>
        <translation>Vycentrovat kompas na interpolaci trasy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="520"/>
        <source>Centrer la carte ici</source>
        <translation>Vycentrovat mapu zde</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="521"/>
        <source>Positionner l&apos;echelle ici</source>
        <translation>Umístit měřítko sem</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="524"/>
        <source>Deplacer le bateau ici</source>
        <translation>Přesunout sem loď</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="528"/>
        <source>Montrer les POIs intermediaires de la route</source>
        <translation>Zobrazit mezilehlé POIs na trase </translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="529"/>
        <source>Simplifier la route</source>
        <translation>Zjednodušit trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="530"/>
        <source>Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="531"/>
        <source>Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="533"/>
        <source>Optimiser la route</source>
        <translation>Optimizovat trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="534"/>
        <source>Copier la route au format kml</source>
        <translation>Zkopírovat trasu v KML formátu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="366"/>
        <location filename="../src/MenuBar.cpp" line="537"/>
        <source>Coller une route</source>
        <translation>Vlepit trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="143"/>
        <source>Ouvrir un GRIB Courants</source>
        <translation>Otevřít GRIB s proudy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="145"/>
        <source>Ouvrir un fichier GRIB Courants</source>
        <translation>Otevřít soubor GRIB s proudy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="146"/>
        <location filename="../src/MenuBar.cpp" line="148"/>
        <source>Fermer le GRIB Courants</source>
        <translation>Zavřít GRIB s proudy</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="158"/>
        <source>Carte du courant</source>
        <translation>Mapa proudů</translation>
    </message>
    <message>
        <source>Afficher les Ã©tiquettes des isothermes 0degC</source>
        <translation type="obsolete">Připojit popisky isotherm 0degC</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="423"/>
        <source>Parametres</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="535"/>
        <source>Zoom sur la route </source>
        <translation>Zaměřit na trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="536"/>
        <source>Supprimer la route</source>
        <translation>Smazat trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="527"/>
        <source>Editer la route</source>
        <translation>Uprav trasu</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="506"/>
        <source>Barrier not in edit mode</source>
        <translation>Bariéra není v módu úprav</translation>
    </message>
    <message>
        <location filename="../src/MenuBar.cpp" line="594"/>
        <source>Aucune</source>
        <translation>Žádný</translation>
    </message>
</context>
<context>
    <name>POI</name>
    <message>
        <location filename="../src/POI.cpp" line="194"/>
        <source>Editer</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="198"/>
        <source>Supprimer</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="274"/>
        <location filename="../src/POI.cpp" line="540"/>
        <source>Supprimer la route </source>
        <translation>Smazat trasu </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="202"/>
        <source>Copier</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="228"/>
        <source>Marque-&gt;WP</source>
        <translation>Značka-&gt;WP</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="206"/>
        <source>Set Date</source>
        <translation>Nastav datum</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="210"/>
        <source>Activer la corne de brume 10mn avant le passage</source>
        <translation>Aktivuj mlžný roh 10 minut před dosažením</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="214"/>
        <location filename="../src/POI.cpp" line="483"/>
        <location filename="../src/POI.cpp" line="494"/>
        <source>Tirer un cap</source>
        <translation>Kresli kurs</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="219"/>
        <source>Tracer une estime TWA</source>
        <translation>Kresli odhad TWA</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="223"/>
        <source>Center on POI</source>
        <translation>Centrovat na POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="234"/>
        <source>Routes</source>
        <translation>Trasy</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="239"/>
        <location filename="../src/POI.cpp" line="543"/>
        <source>Editer la route </source>
        <translation>Uprav trasu </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="244"/>
        <source>Montrer les pois intermediaires de la route </source>
        <translation>Zobrazit mezilehlé POIs na trase </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="250"/>
        <location filename="../src/POI.cpp" line="550"/>
        <source>Copier la route </source>
        <translation>Kopíruj trasu</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="256"/>
        <location filename="../src/POI.cpp" line="553"/>
        <source>Simplifier la route </source>
        <translation>Zjednodušit trasu</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="257"/>
        <source>Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="258"/>
        <source>Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="265"/>
        <location filename="../src/POI.cpp" line="556"/>
        <source>Optimiser la route </source>
        <translation>Optimizovat trasu</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="269"/>
        <location filename="../src/POI.cpp" line="559"/>
        <source>Zoom sur la route </source>
        <translation>Zaměřit na trasu</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="279"/>
        <source>Optimiser le placement sur la route</source>
        <translation>Optimalizuj pozici na trase</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="283"/>
        <source>Non simplifiable</source>
        <translation>Nezjednodušitelný</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="289"/>
        <source>Mode de navigation vers ce POI</source>
        <translation>Mód navigace k tomuto POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="292"/>
        <source>VB-VMG</source>
        <translation>VBVMG</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="298"/>
        <source>B-VMG</source>
        <translation>BestVMG</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="304"/>
        <source>ORTHO</source>
        <translation>Orthodromic</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="317"/>
        <source>Pre-selectionner pour le pilototo</source>
        <translation>Přednastavení pro autopilota</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="324"/>
        <source>Routage vers ce POI</source>
        <translation>Routing přes tento POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="329"/>
        <source>Tracer/Editer une ligne avec un autre POI</source>
        <translation>Nakresli/Uprav čáru k jinému POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="546"/>
        <source>Montrer les POIs intermediaires de la route </source>
        <translation>Zobrazit mezilehlé POIs na trase </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="686"/>
        <source>Ortho a partir de </source>
        <translation>Ortho od </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="962"/>
        <source>Etes-vous sur ?</source>
        <translation>Určitě?</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="994"/>
        <source>La destruction d&apos;une marque est definitive.

Etes-vous sur de vouloir supprimer</source>
        <translation>Odstranění značky je definitivní.

Jsi si jist, že ji chceš smazat</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1607"/>
        <source>Fichiers ini (*.ini)</source>
        <translation>ini soubory (*.ini)</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1608"/>
        <location filename="../src/POI.cpp" line="1696"/>
        <source>;;Autres fichiers (*)</source>
        <translation>;;Jiné soubory (*)</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1610"/>
        <source>Choisir un fichier ini</source>
        <translation>Vyber ini soubor</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1685"/>
        <source>Zygrib POI import</source>
        <translation>Import Zygrib POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1685"/>
        <source>POI imported from zyGrib</source>
        <translation>POI importován ze zyGrib</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1695"/>
        <source>Fichiers textes (*.txt)</source>
        <translation>Textové soubory</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1698"/>
        <source>Choisir un fichier GeoData</source>
        <translation>Vyber GeoData soubor</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1745"/>
        <source>Classement </source>
        <translation>Pořadí</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1749"/>
        <source>GeoData POI import</source>
        <translation>Import POI GeoData</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1749"/>
        <source>POI imported from GeoData</source>
        <translation>POI importován z GeoData</translation>
    </message>
    <message>
        <source>Relier ce poi a un autre poi</source>
        <translation type="obsolete">Draw a line between this POI and anoter POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="488"/>
        <source>Arret du cap</source>
        <translation>Zastav kurs</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="568"/>
        <source>Aucune route</source>
        <translation>Žádná trasa</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="609"/>
        <source>Marque-&gt;WP : </source>
        <translation>Značka-&gt;WP :</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="663"/>
        <source>1ere cible du pilototo</source>
        <translation>První cíl pro autopilota</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="666"/>
        <source>2eme cible du pilototo</source>
        <translation>Druhý cíl pro autopilota</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="669"/>
        <source>3eme cible du pilototo</source>
        <translation>Třetí cíl pro autopilota</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="672"/>
        <source>4eme cible du pilototo</source>
        <translation>Čtvrtý cíl pro autopilota</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="675"/>
        <source>5eme cible du pilototo</source>
        <translation>Pátý cíl pro autopilota</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="680"/>
        <location filename="../src/POI.cpp" line="688"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <source>Distance Ortho a partir de </source>
        <translation type="obsolete">Orthodromic distance from </translation>
    </message>
    <message>
        <source>La destruction d&apos;une marque est definitive.

Etes-vous sur ?</source>
        <translation type="obsolete">Smazání značky nelze zvrátit.

Určitě to chceš?</translation>
    </message>
    <message>
        <source>Destruction d&apos;un marque</source>
        <translation type="obsolete">Smazání značky</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1002"/>
        <source>Le calcul de la route n&apos;est pas fini, impossible de supprimer ce POI</source>
        <translation>Trasa se počítá, nelze teď smazat tento POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1002"/>
        <source>Destruction d&apos;une marque</source>
        <translation>Smazání značky</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1143"/>
        <source>Optimisation du placement d&apos;un POI</source>
        <translation>Optimalizace pozice POI</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1144"/>
        <source>Vous ne pouvez pas optimiser en mode VBVMG-VLM</source>
        <translation>Nemůžeš optimalizovat v módu VBVMG-VLM</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1188"/>
        <source>ETA du prochain POI: </source>
        <translation>Předchozí ETA dalšího POI: </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1191"/>
        <source>Dist. restante du prochain POI: </source>
        <translation>Zbývající vzdálenost k dalšímu POI: </translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1228"/>
        <source>Meilleure ETA: </source>
        <translation>Nejlepší ETA:</translation>
    </message>
    <message>
        <source>Meilleure distance restante: </source>
        <translation type="obsolete">Shortest remaining distance:</translation>
    </message>
    <message>
        <source>Marque-&gt;WP :</source>
        <translation type="obsolete">Mark-&gt;WP :</translation>
    </message>
    <message>
        <source>Distance Ortho a partir de</source>
        <translation type="obsolete">Orthodromic distance from</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="708"/>
        <source>Non joignable avec ce Grib</source>
        <translation>Nelze dosáhnout s tímto Gribem</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="957"/>
        <source>Attention: votre bateau est en mode cap fixe</source>
        <translation>Varování: tvá loď je v módu fixního kursu</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="959"/>
        <source>Attention: votre bateau est en mode Regulateur d&apos;Allure (angle au vent fixe)</source>
        <translation>Varování: tvá loď je v módu fixního úhlu větru</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="961"/>
        <source>Definition du WP-VLM</source>
        <translation>Nastavení WP-VLM</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="993"/>
        <source>Detruire la marque</source>
        <translation>Smazat značku</translation>
    </message>
    <message>
        <source>D�truire la marque</source>
        <translation type="obsolete">Delete mark</translation>
    </message>
    <message>
        <source>La destruction d&apos;une marque est definitive.  Etes-vous sur ?</source>
        <translation type="obsolete">Mark deleting is final.  Are you sure ?</translation>
    </message>
    <message>
        <source>Meilleure ETA:</source>
        <translation type="obsolete">Best ETA:</translation>
    </message>
    <message>
        <source>Meilleure distance restante:</source>
        <translation type="obsolete">Shortest remaining distance:</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1374"/>
        <source>Abandon du positionnement automatique</source>
        <translation>Automatické poziciování bylo zastaveno</translation>
    </message>
    <message>
        <location filename="../src/POI.cpp" line="1374"/>
        <source>Souhaitez vous conserver la meilleure position deja trouvee?</source>
        <translation>Chceš zachovat nejlepší zatím nalezenou pozici?</translation>
    </message>
</context>
<context>
    <name>POI_delete_ui</name>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="14"/>
        <source>Selection de type de marque</source>
        <translation>Výběr typu značky</translation>
    </message>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="43"/>
        <source>&lt;a href=&quot;titi&quot;&gt;Tous&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;titi&quot;&gt;Vše&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="61"/>
        <source>&lt;a href=&quot;toto&quot;&gt;Aucun&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;toto&quot;&gt;Nic&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="73"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="83"/>
        <source>Marque</source>
        <translation>Značka</translation>
    </message>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="90"/>
        <source>Balise</source>
        <translation>Maják</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../src/Ui/poi_delete.ui" line="26"/>
        <source>Type de marque a supprimer</source>
        <translation>Typ značky pro smazání</translation>
    </message>
</context>
<context>
    <name>POI_editor_ui</name>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="22"/>
        <source>Nom et type de la marque</source>
        <translation>Jméno a typ značky</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="36"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="41"/>
        <source>Marque</source>
        <translation>Značka</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="46"/>
        <source>Balise</source>
        <translation>Maják</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="57"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="137"/>
        <location filename="../src/Ui/POI_editor.ui" line="235"/>
        <source>&apos;&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="298"/>
        <source>Prochain cap</source>
        <translation>Příští kurs</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="63"/>
        <source>Latitude</source>
        <translation>Zem. šířka</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="78"/>
        <source>Nord</source>
        <translation>Sever</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="83"/>
        <source>Sud</source>
        <translation>Jih</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <source>&apos;</source>
        <translation type="obsolete">&apos;</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="70"/>
        <source>Longitude</source>
        <translation>Zem. délka</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="92"/>
        <source>Est</source>
        <translation>Východ</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="97"/>
        <source>Ouest</source>
        <translation>Západ</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="105"/>
        <source>Latitude (dd,dd)</source>
        <translation>Zem. šířka (dd,dd)</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="178"/>
        <source>Longitude (dd,dd)</source>
        <translation>Zem. délka (dd,dd)</translation>
    </message>
    <message>
        <source>Horadatage de passage</source>
        <translation type="obsolete">Passage timestamp</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="378"/>
        <source>Horodatage de passage</source>
        <translation>Moment průjezdu</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="384"/>
        <source>Activer</source>
        <translation>Aktivovat</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="391"/>
        <source>M/d/yyyy hh:mm:ss</source>
        <translation>MM/dd/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="407"/>
        <source>Valider</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="414"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="421"/>
        <source>Supprimer</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="441"/>
        <source>Copier</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="448"/>
        <source>Coller</source>
        <translation>Vložit</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="468"/>
        <source>Marque-&gt;WP</source>
        <translation>Značka-&gt;WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="319"/>
        <source>Route</source>
        <translation>Trasa</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="330"/>
        <source>Ordre dans la route</source>
        <translation>Pořadí v trase</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="368"/>
        <source>Non simplifiable</source>
        <translation>Nezjednodušitelný</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="118"/>
        <location filename="../src/Ui/POI_editor.ui" line="210"/>
        <location filename="../src/Ui/POI_editor.ui" line="257"/>
        <location filename="../src/Ui/POI_editor.ui" line="276"/>
        <source> °</source>
        <translation> °</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_editor.ui" line="159"/>
        <location filename="../src/Ui/POI_editor.ui" line="188"/>
        <source> &apos;</source>
        <translation> &apos;</translation>
    </message>
</context>
<context>
    <name>POI_input_ui</name>
    <message>
        <location filename="../src/Ui/POI_input.ui" line="14"/>
        <source>Importation de marques - LAT,LON@WPH</source>
        <translation>Importovat značky - LAT,LON@WPH</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_input.ui" line="20"/>
        <source>Coller ou inserer des POI au format LAT,LON@WPH</source>
        <translation>Vlož nebo kopíruj POI ve formátu LAT,LON@WPH</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_input.ui" line="32"/>
        <source>0/0 valid WP</source>
        <translation>0/0 validní WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_input.ui" line="53"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_input.ui" line="58"/>
        <source>Route</source>
        <translation>Trasa</translation>
    </message>
    <message>
        <location filename="../src/Ui/POI_input.ui" line="63"/>
        <source>Balise</source>
        <translation>Maják</translation>
    </message>
</context>
<context>
    <name>Polar</name>
    <message>
        <source>Pre-calcul des valeurs de VMG (ceci est fait seulement la premiere fois qu&apos;une polaire est utilisee)</source>
        <translation type="obsolete">Pre-calculus VMG values (this is done only the first time a polar is used)</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="318"/>
        <source>Pre-calcul des valeurs de VMG pour </source>
        <translation>Předpočítávám VMG pro </translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <source>Starting qtVLM</source>
        <translation type="obsolete">Startuji qtVLM</translation>
    </message>
</context>
<context>
    <name>QJson::ParserRunnable</name>
    <message>
        <source>An error occured while parsing json: %1</source>
        <translation type="obsolete">Chyba při přežvykování json: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="59"/>
        <source>Sel</source>
        <translation>Sel</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="60"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="88"/>
        <source>Rang</source>
        <translation>Rang</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="64"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="92"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="650"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="651"/>
        <source>Pseudo</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="67"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="95"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="654"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="655"/>
        <source>Nom</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="70"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="98"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="658"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="659"/>
        <source>Numero</source>
        <translation>Číslo</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="73"/>
        <location filename="../src/Dialogs/DialogRace.cpp" line="101"/>
        <source>Pavillon</source>
        <translation>Vlajka</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="74"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="75"/>
        <source>Loch 1h</source>
        <translation>Log 1h</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="76"/>
        <source>Loch 3h</source>
        <translation>Log 3h</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="77"/>
        <source>Loch 24h</source>
        <translation>Log 24h</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="78"/>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="256"/>
        <source>DNM</source>
        <translation>DNM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="79"/>
        <source>Dist 1er</source>
        <translation>Ztráta na 1.</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="102"/>
        <source>Loch</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="103"/>
        <source>Date depart</source>
        <translation>Datum startu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="104"/>
        <source>Duree</source>
        <translation>Trvání</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRace.cpp" line="105"/>
        <source>Ecart</source>
        <translation>Rozdíl</translation>
    </message>
    <message>
        <location filename="../src/Player.cpp" line="212"/>
        <source>Parametre compte VLM</source>
        <translation>Nastavení účtu VLM</translation>
    </message>
    <message>
        <location filename="../src/Player.cpp" line="213"/>
        <source>Erreur de parametrage du compte VLM &apos;</source>
        <translation>Chyba v parametrech lodi</translation>
    </message>
    <message>
        <location filename="../src/Player.cpp" line="214"/>
        <source>&apos;.
 Verifiez le login et mot de passe</source>
        <translation>&apos;. Zkontroluj jméno a heslo</translation>
    </message>
    <message>
        <source>&apos;.Verifiez le login et mot de passe</source>
        <translation type="obsolete">&apos;. Check login and password</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="56"/>
        <location filename="../src/Polar.cpp" line="70"/>
        <location filename="../src/Polar.cpp" line="100"/>
        <location filename="../src/Polar.cpp" line="116"/>
        <location filename="../src/Polar.cpp" line="130"/>
        <location filename="../src/Polar.cpp" line="141"/>
        <source>Lecture de polaire</source>
        <translation>Načítání poláru</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="57"/>
        <location filename="../src/Polar.cpp" line="71"/>
        <source>Fichier %1 invalide</source>
        <translation>Chybný soubor %1</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="101"/>
        <location filename="../src/mycentralwidget.cpp" line="2140"/>
        <location filename="../src/mycentralwidget.cpp" line="2814"/>
        <source>Impossible d&apos;ouvrir le fichier %1</source>
        <translation>Nelze otevřít soubor %1</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="117"/>
        <source>Impossible d&apos;ouvrir le fichier %1 (ni en .csv ni en .pol)</source>
        <translation>Nelze otevřít soubor %1 (není .csv nebo není .pol)</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="131"/>
        <location filename="../src/mycentralwidget.cpp" line="2277"/>
        <source>Fichier %1 vide</source>
        <translation>Soubor %1 je prázdný</translation>
    </message>
    <message>
        <location filename="../src/Polar.cpp" line="142"/>
        <source>Fichier %1 invalide (doit commencer par TWA\TWS et non &apos;%2&apos;)</source>
        <translation>Soubor %1 je chybný (měl by začínat s TWA\TWS a ne s &apos;%2&apos;)</translation>
    </message>
    <message>
        <source> C</source>
        <translation type="obsolete"> C</translation>
    </message>
    <message>
        <source> F</source>
        <translation type="obsolete"> F</translation>
    </message>
    <message>
        <source> K</source>
        <translation type="obsolete"> K</translation>
    </message>
    <message>
        <source>degC</source>
        <translation type="obsolete"> C</translation>
    </message>
    <message>
        <source>degF</source>
        <translation type="obsolete"> F</translation>
    </message>
    <message>
        <source>degK</source>
        <translation type="obsolete"> K</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="obsolete">km/h</translation>
    </message>
    <message>
        <source>m/s</source>
        <translation type="obsolete">m/s</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="obsolete">km</translation>
    </message>
    <message>
        <source>dddegmm&apos;ss&quot;</source>
        <translation type="obsolete">dd mm&apos;ss&quot;</translation>
    </message>
    <message>
        <source>dddegmm,mm&apos;</source>
        <translation type="obsolete">dd mm,mm&apos;</translation>
    </message>
    <message>
        <source>dd mm&apos;ss&quot;</source>
        <translation type="obsolete">dd mm&apos;ss&quot;</translation>
    </message>
    <message>
        <source>dd mm,mm&apos;</source>
        <translation type="obsolete">dd mm,mm&apos;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="390"/>
        <location filename="../src/boatVLM.cpp" line="749"/>
        <source>Bateau au ponton</source>
        <translation>Loď v přístavu</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="391"/>
        <location filename="../src/boatVLM.cpp" line="750"/>
        <source>Le bateau </source>
        <translation>Loď </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="391"/>
        <source>a ete desactive car vous n&apos;etes plus boatsitter</source>
        <translation>byla deaktivována pravděpodobně proto, že se o ní nestaráš</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="720"/>
        <source>Telechargement de la polaire</source>
        <translation>Stáhnout polár</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="721"/>
        <source>La polaire </source>
        <translation>Polár </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="750"/>
        <source>a ete desactive car hors course</source>
        <translation>byla deaktivována, protože se neúčastní žádného závodu</translation>
    </message>
    <message>
        <source>Paramètre bateau</source>
        <translation type="obsolete">Boat setting</translation>
    </message>
    <message>
        <source>Erreur de communication avec VLM ds</source>
        <translation type="obsolete">Communication error with VLM ds</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="135"/>
        <source>Reporter l&apos;erreur au dev qtVlm</source>
        <translation>Pošli chybu autorům qtVlm</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="obsolete">Version:</translation>
    </message>
    <message>
        <source>Zone:</source>
        <translation type="obsolete">Zone:</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="134"/>
        <source>Erreur de communication avec VLM ds </source>
        <translation>Chyba komunikace s VLM ds</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="136"/>
        <source>Version: </source>
        <translation>Verze:</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="137"/>
        <source>Zone: </source>
        <translation>Zóna: </translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="138"/>
        <source>Code erreur:</source>
        <translation>Kód chyby:</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="139"/>
        <source>Msg erreur:</source>
        <translation>Chybová zpráva:</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="140"/>
        <source>Complement: </source>
        <translation>Dodatek:</translation>
    </message>
    <message>
        <source>Complement:</source>
        <translation type="obsolete">Supplement:</translation>
    </message>
    <message>
        <location filename="../src/inetClient.cpp" line="141"/>
        <source>Ordre:</source>
        <translation>Pořadí:</translation>
    </message>
    <message>
        <location filename="../src/inetConnexion.cpp" line="202"/>
        <location filename="../src/inetConnexion.cpp" line="205"/>
        <source>Erreur internet</source>
        <translation>Chyba sítě</translation>
    </message>
    <message>
        <location filename="../src/inetConnexion.cpp" line="203"/>
        <source>Serveur VLM inaccessible</source>
        <translation>VLM server neodpovídá</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2016"/>
        <source>Un nom de fichier valide portant l&apos;extension .png est requis</source>
        <translation>Je potřeba platný název souboru s příponou .png</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2037"/>
        <source>Voir Vlm Logs</source>
        <translation>Zkontroluj logy Vlm</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2038"/>
        <source>Pas de bateau VLM actif.</source>
        <translation>Žádná aktivní loď ve VLM.</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2050"/>
        <source>Telecharger traces VLM</source>
        <translation>Stáhnout trasy z VLM</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2051"/>
        <source>Pas de compte VLM actif.</source>
        <translation>Žádný aktivní uživatel VLM.</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2082"/>
        <source>Corne de brume activee</source>
        <translation>Mlžný roh aktivován</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2082"/>
        <source>Arreter la corne de brume</source>
        <translation>Zastav mlžný roh</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2116"/>
        <source>Aucun Bateau</source>
        <translation>Není vybrána loď</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2117"/>
        <source>L&apos;import de route necessite un bateau actif.</source>
        <translation>Importování trasy vyžaduje aktivní loď.</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2139"/>
        <location filename="../src/mycentralwidget.cpp" line="2276"/>
        <location filename="../src/mycentralwidget.cpp" line="2304"/>
        <location filename="../src/mycentralwidget.cpp" line="2499"/>
        <location filename="../src/mycentralwidget.cpp" line="2813"/>
        <location filename="../src/mycentralwidget.cpp" line="2821"/>
        <location filename="../src/mycentralwidget.cpp" line="2841"/>
        <location filename="../src/mycentralwidget.cpp" line="3444"/>
        <source>Lecture de route</source>
        <translation>Načítání trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2290"/>
        <source>Importation de routage MaxSea</source>
        <translation>Importuj trasu z MaxSea</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2290"/>
        <source>Heures a ajouter/enlever pour obtenir UTC (par ex -2 pour la France)</source>
        <translation>Hodin přidat/ubrat k dosažení UTC (např. -2 pro Francii)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2305"/>
        <source>Fichier %1 invalide (doit commencer par POSITION et non &apos;%2&apos;), ou alors etre au format sbsRouteur</source>
        <translation>Soubor %1 neplatný (měl by začínat pozicí (POSITION) a ne&apos;%2&apos;), nebo by měl býtve formátu sbsRouteur</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2619"/>
        <source>Export de route</source>
        <translation>Export trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3682"/>
        <location filename="../src/mycentralwidget.cpp" line="3684"/>
        <source>Simplification/Optimisation de route</source>
        <translation>Zjednodušení/optimalizace trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3682"/>
        <source>Cette operation est impossible pour une route figee ou une route sans ETA</source>
        <translation>Tato operace je nemožná pro zmrazenou trasu nebo trasu bez ETA</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3684"/>
        <source>Cette operation est impossible si le mode de calcul VBVMG est celui de VLM</source>
        <translation>Tato operace není povolena pokud mód VBVMG je nastaven na VLM</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3836"/>
        <source>Resultat de l&apos;optimisation</source>
        <translation>Pravidla pro optimalizaci</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="155"/>
        <source>Un nom de fichier valide portant l&apos;extension .txt est requis</source>
        <translation>Je vyžadován platný soubor s příponou .txt</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="160"/>
        <source>Sauvegarde Logs VLM</source>
        <translation>Uložit logy VLM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1046"/>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="161"/>
        <location filename="../src/boatVLM.cpp" line="1110"/>
        <location filename="../src/boatVLM.cpp" line="1116"/>
        <location filename="../src/mycentralwidget.cpp" line="2022"/>
        <location filename="../src/mycentralwidget.cpp" line="2620"/>
        <location filename="../src/mycentralwidget.cpp" line="3445"/>
        <source>Impossible de creer le fichier %1</source>
        <translation>Nelze vytvořit soubor %1</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogVlmLog.cpp" line="154"/>
        <location filename="../src/mycentralwidget.cpp" line="2015"/>
        <location filename="../src/mycentralwidget.cpp" line="2021"/>
        <source>Sauvegarde ecran</source>
        <translation>Snímek obrazovky</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="1109"/>
        <source>Exports VLM Syncs</source>
        <translation>Exporty VLM Syncs </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="1115"/>
        <source>Exports VLM Syncs Summary</source>
        <translation>Přehled exportů VLM Sync</translation>
    </message>
    <message>
        <source>Simplification de route</source>
        <translation type="obsolete">Simplification of route</translation>
    </message>
    <message>
        <source>La simplification est impossible pour une route figee ou une route sans ETA</source>
        <translation type="obsolete">Simplification is not possible for a frozen route or a route without ETA</translation>
    </message>
    <message>
        <source>La simplification est impossible si le mode de calcul VBVMG est celui de VLM</source>
        <translation type="obsolete">Simplification is not allowed if VB-VMG mode is set to VLM</translation>
    </message>
    <message>
        <source>Simplication de route</source>
        <translation type="obsolete">Simplify route</translation>
    </message>
    <message>
        <source>Perte maximum de temps sur l&apos;ETA finale (en minutes)</source>
        <translation type="obsolete">Maximum loss of time on final ETA (in minutes)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3712"/>
        <source>Resultat de la simplification</source>
        <translation>Výsledek zjednodušení</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogDownloadTracks.cpp" line="234"/>
        <location filename="../src/boatVLM.cpp" line="766"/>
        <location filename="../src/opponentBoat.cpp" line="866"/>
        <source>Parametre bateau</source>
        <translation>Nastavení lodi</translation>
    </message>
    <message>
        <location filename="../src/xmlBoatData.cpp" line="449"/>
        <source>Lecture de parametre bateau</source>
        <translation>Načítání nastavení lodi</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="399"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="405"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="414"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="421"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="429"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="438"/>
        <source>Routage</source>
        <translation>Routing</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="399"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="414"/>
        <source>Le POI de destination est invalide</source>
        <translation>Cílový POI je neplatný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="405"/>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="421"/>
        <source>Le POI de depart est invalide</source>
        <translation>Výchozí POI je neplatný</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="429"/>
        <source>Le POI de depart et d&apos;arrivee sont les memes, vous etes deja arrive...</source>
        <translation>Výchozí a cílový POI je shodný, už jsi připlul...</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoutage.cpp" line="438"/>
        <source>Le routage ne peut pas partir du bateau si la fonction&lt;br&gt;multi-routage est utilisee</source>
        <translation>Routing nemůže vycházet od lodi, pokud je použita funkce&lt;br&gt;multi-routingu</translation>
    </message>
    <message>
        <source>TWA %ddeg, BS %.1fnds
AWA %ddeg, AWS %.1fnds</source>
        <translation type="obsolete">TWA %d°, BS %.1fkts
AWA %d°, AWS %.1fkts</translation>
    </message>
    <message>
        <source>TWA %ddeg, BS %.1fnds
AWA %ddeg, AWS %.1fnds
VMG %.1fnds</source>
        <translation type="obsolete">TWA %d°, BS %.1f kts
AWA %d°, AWS %.1f kts
VMG %.1 kts</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="178"/>
        <source>Date et heure</source>
        <translation>Datum a čas</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="179"/>
        <source>Aller vers</source>
        <translation>Jet do</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="180"/>
        <source>Cap a suivre apres</source>
        <translation>Kurs po</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="181"/>
        <source>Mode</source>
        <translation>Mód</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="242"/>
        <source>Date heure</source>
        <translation>Datum a čas</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="244"/>
        <source>TWS</source>
        <translation>TWS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="246"/>
        <source>TWA</source>
        <translation>TWA</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="247"/>
        <source>BS</source>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="248"/>
        <source>HDG</source>
        <translation>HDG</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="249"/>
        <source>SOG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="250"/>
        <source>COG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="253"/>
        <source>CS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="254"/>
        <source>CD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="257"/>
        <source>CNM</source>
        <translation>CNM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="1045"/>
        <source>Export de tableau de marche</source>
        <translation>Export záznamu trasy</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="245"/>
        <source>TWD</source>
        <translation>TWD</translation>
    </message>
    <message>
        <source>Vitesse</source>
        <translation type="obsolete">Speed</translation>
    </message>
    <message>
        <source>Cap</source>
        <translation type="obsolete">Heading</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="251"/>
        <source>AWS</source>
        <translation>AWS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="252"/>
        <source>AWA</source>
        <translation>AWA</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="255"/>
        <source>POI cible</source>
        <translation>Cílový POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="258"/>
        <source>Lon POI cible</source>
        <translation>Lon cílového POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRoute.cpp" line="259"/>
        <source>Lat POI cible</source>
        <translation>Lat cílového POI</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogviewpolar.cpp" line="62"/>
        <source>TWA %ddeg, BS %.2fnds
AWA %.2fdeg, AWS %.2fnds
VMG %.2fnds</source>
        <translation>TWA %d°, BS %.2f kts
AWA %.2f°, AWS %.2f kts
VMG %.2f kts</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/routeInfo.cpp" line="16"/>
        <source>Information au point d&apos;interpolation pour </source>
        <translation>Data v interpolačním bodě pro  </translation>
    </message>
    <message>
        <location filename="../src/Util.cpp" line="130"/>
        <source>nds</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/Util.cpp" line="184"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Util.cpp" line="748"/>
        <source>jours</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/Progress.cpp" line="30"/>
        <source>Starting qtVLM</source>
        <translation>Startuji qtVLM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="19"/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="20"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="21"/>
        <source>Start Date</source>
        <translation>Datum startu</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="22"/>
        <source>ETA</source>
        <translation>ETA</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="23"/>
        <source>Duration</source>
        <translation>Trvání</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="24"/>
        <source>Ortho Distance</source>
        <translation>Ortho vzdálenost</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="25"/>
        <source>Sailed Distance</source>
        <translation>Uplutá vzdálenost</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="26"/>
        <source>Avg BS</source>
        <translation>Prům. BS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="27"/>
        <source>Max BS</source>
        <translation>Max BS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="28"/>
        <source>Min BS</source>
        <translation>Min BS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="29"/>
        <source>Avg TWS</source>
        <translation>Prům. TWS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="30"/>
        <source>Max TWS</source>
        <translation>Max TWS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="31"/>
        <source>Min TWS</source>
        <translation>Min TWS</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="32"/>
        <source>Nb Tacks/Gybes</source>
        <translation>Počet Re/Halz</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="33"/>
        <source>Beating time</source>
        <translation>Čas proti větru</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="34"/>
        <source>Downwind time</source>
        <translation>Čas po větru</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="35"/>
        <source>Reaching time</source>
        <translation>Čas na bočák</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="36"/>
        <source>Motor time</source>
        <translation>Čas na motor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/DialogRouteComparator.cpp" line="37"/>
        <source>Night navigation</source>
        <translation>Noční navigace</translation>
    </message>
    <message>
        <location filename="../src/XmlFile.cpp" line="69"/>
        <location filename="../src/XmlFile.cpp" line="105"/>
        <source>Reading XML file %1</source>
        <translation>Načítáni XML souboru %1</translation>
    </message>
    <message>
        <location filename="../src/XmlFile.cpp" line="71"/>
        <location filename="../src/XmlFile.cpp" line="107"/>
        <source>Error line %1, column %2:
%3</source>
        <translation>Chyba řádek %1, sloupec %2:
%3</translation>
    </message>
</context>
<context>
    <name>ROUTAGE</name>
    <message>
        <source>Date et heure d&apos;arrivee:</source>
        <translation type="obsolete">Date and time of arrival:</translation>
    </message>
    <message>
        <source>&lt;br&gt;Arrivee en:</source>
        <translation type="obsolete">&lt;br&gt;Arrival within:</translation>
    </message>
    <message>
        <source>jours</source>
        <translation type="obsolete">days</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;br&gt;Temps de calcul:</source>
        <translation type="obsolete">&lt;br&gt;&lt;br&gt;Calculation time:</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="899"/>
        <location filename="../src/routage.cpp" line="908"/>
        <location filename="../src/routage.cpp" line="923"/>
        <source>Routage</source>
        <translation>Routing</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="899"/>
        <source>Pas de polaire chargee</source>
        <translation>Není nahrán polár</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="908"/>
        <source>Pas de grib charge</source>
        <translation>Není nahrán grib</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="923"/>
        <source>Date de depart choisie incoherente avec le grib</source>
        <translation>Výchozí datum není kompatibilní s daty v gribu</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2407"/>
        <source>Date et heure d&apos;arrivee: </source>
        <translation>Datum a čas příjezdu: </translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2408"/>
        <source>&lt;br&gt;Arrivee en: </source>
        <translation>&lt;br&gt;Příjezd v: </translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2408"/>
        <source> jours </source>
        <translation> dnů </translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2409"/>
        <source>&lt;br&gt;&lt;br&gt;Temps de calcul: </source>
        <translation>&lt;br&gt;&lt;br&gt;Čas výpočtu: </translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2414"/>
        <source>Routage arrete par l&apos;utilisateur</source>
        <translation>Routing zastaven uživatelem</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2416"/>
        <source>Impossible de rejoindre l&apos;arrivee, desole</source>
        <translation>Soráč, nelze dosáhnout WP&lt;br&gt;Zkus to s jiným gribem</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2449"/>
        <source>Convertir en route</source>
        <translation>Konvertuj na trasu</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2450"/>
        <source>Ce routage a ete calcule avec une hypothese modifiant les donnees du grib&lt;br&gt;La route ne prendra pas ce scenario en compte&lt;br&gt;Etes vous sur de vouloir le convertir en route?</source>
        <translation>Tento routing byl proveden s předpokladem zněny dat gribu&lt;br&gt;Trasa nebude brát tento scénář v úvahu&lt;br&gt;Určitě chceš konvertovat na trasu?</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2897"/>
        <source>Supprimer les autres routages</source>
        <translation>Potlačit ostatní routingy</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2913"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2914"/>
        <source>La route partira du point de depart et a l&apos;heure de depart du routage</source>
        <translation>Trasa bude začínat z výchozího bodu routingu a data</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="3772"/>
        <source>Editer le routage</source>
        <translation>Upravit routing</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="3774"/>
        <source>Supprimer le routage</source>
        <translation>Odsttranit routing</translation>
    </message>
    <message>
        <source>Convertion d&apos;un routage en route</source>
        <translation type="obsolete">Konvertovat routing na trasu</translation>
    </message>
    <message>
        <source>Voulez-vous que la route parte du bateau a la prochaine vacation?</source>
        <translation type="obsolete">Chceš, aby nová trasa začínala od lodi v příští mezeře?</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="4492"/>
        <source>Calcul des routes alternatives</source>
        <translation>Výpočet alternativních tras</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="4493"/>
        <source>Veuillez patienter...</source>
        <translation>Čekej...</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="4588"/>
        <source>Arrivee (estimation): </source>
        <translation>Příjezd (odhad):</translation>
    </message>
    <message>
        <source>Detruire le routage : %1?</source>
        <translation type="obsolete">Delete routing: %1?</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="3769"/>
        <source>Creer un pivot en changeant les options</source>
        <translation>Vytvoř pivot, který mění nastavení routingu</translation>
    </message>
    <message>
        <source>D�truire le routage : %1?</source>
        <translation type="obsolete">Delete routing : %1?</translation>
    </message>
    <message>
        <source>La destruction d&apos;un routage est definitive.</source>
        <translation type="obsolete">Deleting a routing is not reversible.</translation>
    </message>
    <message>
        <source>Routage: </source>
        <translation type="obsolete">Routing:</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2880"/>
        <location filename="../src/routage.cpp" line="2890"/>
        <source>Conversion d&apos;un routage en route</source>
        <translation>Konvertuji routing na trasu</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2881"/>
        <source>Un des routages en amont a ete supprime,&lt;br&gt;la conversion est impossible</source>
        <translation>Jeden z nadřazených routingů byl smazán,&lt;br&gt;konverze není možná</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2891"/>
        <source>Voulez-vous que le point de depart de la route suive le bateau maintenant?</source>
        <translation>Chceš aby bod odjezdu byl teď u lodi?</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="2892"/>
        <source>Simplifier/Optimiser automatiquement</source>
        <translation>Autoaticky zjednodušit/optimalizovat</translation>
    </message>
    <message>
        <location filename="../src/routage.cpp" line="3767"/>
        <source>Creer un pivot</source>
        <translation>Vytvořit pivot</translation>
    </message>
</context>
<context>
    <name>ROUTAGE_Editor_ui</name>
    <message>
        <source>Valider</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="115"/>
        <source>Parametres Generaux</source>
        <translation>Obecné nastavení</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="140"/>
        <source>Points de depart et d&apos;arrivee</source>
        <translation>Cílový a výchozí bod</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="146"/>
        <source>Router depuis le bateau</source>
        <translation>Routing od lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="166"/>
        <source>Date et heure de depart du routage</source>
        <translation>Počáteční datum a čas routingu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="175"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="200"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="781"/>
        <source>M/d/yyyy hh:mm:ss</source>
        <translation>MM/dd/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="231"/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1075"/>
        <source>Multi-routage</source>
        <translation>Multi-routing</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1087"/>
        <source>Generer </source>
        <translation>Generovat</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1100"/>
        <source> routes</source>
        <translation>trasy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1107"/>
        <source>a interval de </source>
        <translation>v intervalu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1120"/>
        <source> jours</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1149"/>
        <source> minutes</source>
        <translation>minuty</translation>
    </message>
    <message>
        <source>Duree Isochrones</source>
        <translation type="obsolete">Time between iso</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="370"/>
        <source>Pas Anglulaire</source>
        <translation>Úhlový krok</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="328"/>
        <source>Angle Balayage</source>
        <translation>Úhel skenování</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="363"/>
        <source>Isochrones &lt;24h</source>
        <translation>Isochrony &lt;24h</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="335"/>
        <source>Cette valeur est egalement utilisee en approche de l&apos;arrivee</source>
        <translation>Tato hodnota je také použita při přiblížení k cíli</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="491"/>
        <source>Ne pas router en dehors de l&apos;ecran</source>
        <translation>Neroutuj mimo obrazovku</translation>
    </message>
    <message>
        <source>Reglage du niveau de zoom</source>
        <translation type="obsolete">Adjust zoom strength</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="877"/>
        <source>Vent minimum/maximum</source>
        <translation>Minimální/Maximální rychlost větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="965"/>
        <source>Pres</source>
        <translation>Proti větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="955"/>
        <source>Portant</source>
        <translation>Po větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="985"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="975"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <source>min</source>
        <extracomment>min</extracomment>
        <translation type="obsolete">min</translation>
    </message>
    <message>
        <source>deg</source>
        <extracomment>deg----------deg</extracomment>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <source>Forcer le vent</source>
        <translation type="obsolete">Vynutit vítr</translation>
    </message>
    <message>
        <source>TWS</source>
        <translation type="obsolete">TWS</translation>
    </message>
    <message>
        <source>nds</source>
        <extracomment>nds</extracomment>
        <translation type="obsolete">kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="77"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="341"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="380"/>
        <source> min</source>
        <extracomment>min
----------
min</extracomment>
        <translation> min</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="245"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="270"/>
        <source> deg</source>
        <extracomment>deg</extracomment>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="313"/>
        <source>Convertir en Route
avec ce prefixe</source>
        <translation>Konvertovat na trasu
s tímto prefixem</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="298"/>
        <source>Empannages et
Virements de bord</source>
        <translation>Obraty proti a po větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="612"/>
        <source>Angle d&apos;epuration par sillage</source>
        <translation type="unfinished">Prune wake angle</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1035"/>
        <source>Réglage du niveau de zoom (plus à droite = plus serré).
La différence est surtout sensible lors d&apos;un routage en diagonale.</source>
        <translation>Nastavení úrovně přiblížení (pohni doprava pro zoom).
Rozdíl je nejvíce patrný při routingu v diagonálním směru.</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="889"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="911"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="939"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="1007"/>
        <source> nds</source>
        <extracomment>nds
----------
nds
----------
nds
----------
nds</extracomment>
        <translation>kts</translation>
    </message>
    <message>
        <source>TWD</source>
        <translation type="obsolete">TWD</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1186"/>
        <source>Options par defaut</source>
        <translation>Výchozí nastavení</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="402"/>
        <source>Montrer les isochrones</source>
        <translation>Zobraz isochrony</translation>
    </message>
    <message>
        <source>Convertir en Route</source>
        <translation type="obsolete">Convert to route</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="484"/>
        <source>Utiliser le module route entre les isochrones</source>
        <translation>Použij trasový modul nezi isochronami</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="474"/>
        <source>Eviter les cotes</source>
        <translation>Vyhni se pobřeží</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="467"/>
        <source>Calculer en parallele (1 processeur disponible)</source>
        <translation>Použij multithreading (1 procesor k dispozici)</translation>
    </message>
    <message>
        <source>A; </source>
        <translation type="obsolete">A; </translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="445"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <source>Zone sensible</source>
        <translation type="obsolete">Sensitive zone</translation>
    </message>
    <message>
        <source>Reglage du niveau de la zone sensible </source>
        <translatorcomment>Réglage du niveau de la zone sensible</translatorcomment>
        <translation type="obsolete">Tune level for sensitive zone</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="1026"/>
        <source>Auto Zoom</source>
        <translation>Auto Zoom</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="409"/>
        <source>Pourcentage de la polaire a appliquer pendant une vacation</source>
        <translation>Percentáž poláru použitá pro jednu mezeru po obratu</translation>
    </message>
    <message>
        <source>Empannages/Virements</source>
        <translation type="obsolete">Tacks/Gybes</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="191"/>
        <source>Date et heure d&apos;arrivee du routage</source>
        <translation>Datum a čas příjezdu routingu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="121"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="223"/>
        <source>Taille et couleur du resultat du routage</source>
        <translation>Velikost abarva výsledku routingu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="442"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="511"/>
        <source>Colorer TWS entre isos</source>
        <translation>Obarvit TWS mezi iso.</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="524"/>
        <source>Parametres avances</source>
        <translation>Rozšířené nastavení</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="640"/>
        <source>Routage Orthodromique</source>
        <translation>Orthodromický routing</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="626"/>
        <source>Afficher la meilleure route pendant le calcul</source>
        <translation>Ukázat nejlepší trasu během výpočtu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="753"/>
        <source>Modification des previsions (what if...)</source>
        <translation>Modifikace předpovědi (co když...)</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="771"/>
        <source>A partir de cette date</source>
        <translation>Od tohoto data</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="415"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="812"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="321"/>
        <source>Isochrones &gt;24h</source>
        <translation>Isochrony &gt;24h</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="501"/>
        <source>Eviter les barrieres</source>
        <translation>Vyhnout se bariérám</translation>
    </message>
    <message>
        <source>Le parametrage de cette option se trouve dans les options avancees</source>
        <translation type="obsolete">Parameters for this option are in advanced options panel</translation>
    </message>
    <message>
        <source>Calculer des routes
alternatives</source>
        <translation type="obsolete">Calculate alternate routes</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="791"/>
        <source>La force du vent est a</source>
        <translation>Síla větru je</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="828"/>
        <source>du grib</source>
        <translation>z Gribu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="837"/>
        <source>Les previsions du grib sont decalees de</source>
        <translation>Offset gribové předpovědi</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="856"/>
        <location filename="../src/Ui/Routage_Editor.ui" line="1133"/>
        <source> heures</source>
        <translation> hodin</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="663"/>
        <source>Routes alternatives</source>
        <translation>Alternativní trasy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="669"/>
        <source>Nombre de routes alternatives</source>
        <translation>Počet alternativních tras</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="711"/>
        <source>Seuil de divergence (% mini non commun)</source>
        <translation>Hranice rozbíhavosti (% mini ne společné)</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="721"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="59"/>
        <source>Isochrones
inverses</source>
        <translation>Inverzní
isochrony</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="67"/>
        <source>Enveloppe</source>
        <translation>Obálka</translation>
    </message>
    <message>
        <source>heures</source>
        <translation type="obsolete">hours</translation>
    </message>
    <message>
        <source>Utiliser ce scenario</source>
        <translation type="obsolete">Použít tento scénář</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="530"/>
        <source>Options supplementaires</source>
        <translation>Další nastavení</translation>
    </message>
    <message>
        <source>Prune Wake Angle</source>
        <translation type="obsolete">Prune Wake Angle</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="633"/>
        <source>Coeff Exploration</source>
        <translation>Koeficient průzkumu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Routage_Editor.ui" line="619"/>
        <source>Utiliser une convergence logarithmique vers l&apos;arrivee</source>
        <translation>Použít logaritmické přiblížení k cíli</translation>
    </message>
</context>
<context>
    <name>ROUTE</name>
    <message>
        <location filename="../src/route.cpp" line="466"/>
        <location filename="../src/route.cpp" line="812"/>
        <source>&lt;br&gt;Route: </source>
        <translation>&lt;br&gt;Trasa: </translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="816"/>
        <location filename="../src/route.cpp" line="896"/>
        <source>&lt;br&gt;ETA: Non joignable avec ce fichier GRIB</source>
        <translation>&lt;br&gt;ETA: nelze dosáhnout s tímto souborem GRIB</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="821"/>
        <location filename="../src/route.cpp" line="900"/>
        <source>&lt;br&gt;ETA: deja atteint</source>
        <translation>&lt;br&gt;ETA: již dosaženo</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="827"/>
        <source>Note: la date indiquee correspond a la desactivation du WP</source>
        <translation>Poznámka: indikovaný ETA odpovídá deaktivaci WP</translation>
    </message>
    <message>
        <source>ETA depuis la derniere vacation</source>
        <translation type="obsolete">ETA from last crank</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="851"/>
        <location filename="../src/route.cpp" line="926"/>
        <source>ETA a partir de maintenant</source>
        <translation>ETA: od teď</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="854"/>
        <location filename="../src/route.cpp" line="929"/>
        <source>ETA depuis la date Grib</source>
        <translation>ETA od data GRIB</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="857"/>
        <location filename="../src/route.cpp" line="932"/>
        <source>ETA depuis la date fixe</source>
        <translation>ETA od fixního data</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="860"/>
        <location filename="../src/route.cpp" line="937"/>
        <source>jours</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="860"/>
        <location filename="../src/route.cpp" line="937"/>
        <source>heures</source>
        <translation>hodin</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="861"/>
        <location filename="../src/route.cpp" line="938"/>
        <source>minutes</source>
        <translation>minut</translation>
    </message>
    <message>
        <location filename="../src/route.cpp" line="893"/>
        <source>Route: </source>
        <translation>Trasa: </translation>
    </message>
    <message>
        <source>Optimisation en cours</source>
        <translation type="obsolete">Optimizace probíhá</translation>
    </message>
    <message>
        <source>ETA avant optimisation: </source>
        <translation type="obsolete">ETA před optimalizací: </translation>
    </message>
    <message>
        <source>Resultat de l&apos;optimisation</source>
        <translation type="obsolete">Pravidla pro optimalizaci</translation>
    </message>
    <message>
        <source>Suppression d&apos;une route</source>
        <translation type="obsolete">Delete a route</translation>
    </message>
    <message>
        <source>Vous ne pouvez pas supprimer une route figee</source>
        <translation type="obsolete">You can&apos;t delete a frozen route</translation>
    </message>
    <message>
        <source>Detruire la route : %1</source>
        <translation type="obsolete">Delete route : %1</translation>
    </message>
    <message>
        <source>La destruction d&apos;une route est definitive.

Voulez-vous egalement supprimer tous les POIs lui appartenant?</source>
        <translation type="obsolete">Route deletion is not reversible. Also delete all POIs of this route?</translation>
    </message>
    <message>
        <source>D�truire la route : %1</source>
        <translation type="obsolete">Delete route : %1</translation>
    </message>
    <message>
        <source>La destruction d&apos;une route est definitive.  Voulez-vous egalement supprimer tous les POIs lui appartenant?</source>
        <translation type="obsolete">Route deletion is final. Also delete all POIs of this route?</translation>
    </message>
</context>
<context>
    <name>ROUTE_Editor_ui</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="161"/>
        <source>Point de depart de la route</source>
        <translation>Výchozí bod trasy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="167"/>
        <source>Du bateau</source>
        <translation>Od lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="177"/>
        <source>Depuis la 1ere marque</source>
        <translation>Od 1. značky</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="85"/>
        <source>Valider</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="95"/>
        <source>Simplifie la route en supprimant les POIs qui ne changent pas l&apos;eta finale.</source>
        <translation>Zjednodušit trasu odstraněním POI, ale bez změny konečného ETA.</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="98"/>
        <source>Simplifier</source>
        <translation>Zjednodušit</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="142"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="70"/>
        <source>Taille et couleur</source>
        <translation>Velikost a barva</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="42"/>
        <source>Route</source>
        <translation>Trasa</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="283"/>
        <source>Les cotes invibles a l&apos;ecran ne sont pas prises en compte</source>
        <translation>Pobřeží neviditelná na obrazovce nejsou brána v úvahu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="286"/>
        <source>Detecter les collisions avec les cotes visibles a l&apos;ecran</source>
        <translation>Detekovat kolize s pobřežím viditelným na obrazovce</translation>
    </message>
    <message>
        <source>Utiliser la formule VLM pour le VB-VMG</source>
        <translation type="obsolete">Use VLM formula for VB-VMG</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="200"/>
        <source>Date et heure de depart de la route</source>
        <translation>Datum a čas začátku trasy</translation>
    </message>
    <message>
        <source>Depuis la derniere Vac</source>
        <translation type="obsolete">From last crank</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="219"/>
        <source>Depuis la date du grib</source>
        <translation>Od data GRIBu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="226"/>
        <source>Date fixe</source>
        <translation>Fixní datum</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="267"/>
        <source>Options</source>
        <translation>Možnosti</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="52"/>
        <source>Nom</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="276"/>
        <source>Figer la route</source>
        <translation>Zmrazit trasu</translation>
    </message>
    <message>
        <source>Cette option peut entrainer un ralentissement lors des mouvements de POIs</source>
        <translation type="obsolete">This option can cause a slowdown when moving the POIs</translation>
    </message>
    <message>
        <source>Recalculer la route simultanement aux mouvements de POIs</source>
        <translation type="obsolete">Recalculate route simultaneously POIs movements</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="296"/>
        <source>Cacher les POIs intermediaires</source>
        <translation>Schovat průběžné POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="423"/>
        <source>Nbre de vac par pas de calcul</source>
        <translation>Počet zalomení na krok výpočtu</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="356"/>
        <source>Cacher la route et tous ses POIs</source>
        <translation>Schovej trasu a všechny její POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="340"/>
        <source>Ce bouton a 3 etats, selon que vous utilisez la formule qtVlm, une formule VLM optimisee, ou la formule VLM originale. La formule VLM optimisee donne en principe exactement les memes resultats que la formule non optimisee.</source>
        <translation>Tenhle čudlík má 3 stavy, podle toho jestli používáš vzorec qtVlm, optimalizovanou verzi vbVmg-Vlm nebo originální verzi vzorce vbVmg z VLM. Optimalizovaná verze a originál by měly vracet stejné výsledky.</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="346"/>
        <source>Formule VLM pour le VB-VMG</source>
        <translation>VLM vzorec pro VB-VMG</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="384"/>
        <source>Empannages/Virements</source>
        <translation>Obraty/Halzy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="391"/>
        <source>Pourcentage de la polaire a appliquer pendant une vacation</source>
        <translation>Percentáž poláru použitá pro jednu mezeru po obratu nebo halze</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="397"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="363"/>
        <source>Retirer les POIs avant le WP-VLM</source>
        <translation>Odstranit POI před WP-VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="377"/>
        <source>Renseigner le cap a suivre (@) des POIs</source>
        <translation>Přidej informaci o kurzu (@) do POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="239"/>
        <source>M/d/yyyy hh:mm:ss</source>
        <translation>MM/dd/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="209"/>
        <source>A partir de la prochaine vacation</source>
        <translation>Od příští mezery</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="454"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="122"/>
        <source>Appliquer</source>
        <translation>Použij</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="108"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Optimise le placement sur la route de tous les POIs simplifiables en utilisant l&apos;algorithme Simplex de Nelder-Mead.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Optimmalizuj všechny zjednodušitelné POI použitím algoritmu Simplex Nelder-Mead.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="115"/>
        <source>Optimiser</source>
        <translation>Optimalizovat
bez zjednodušení</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="553"/>
        <source>Envoyer les ordres</source>
        <translation>Pošli příkazy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="477"/>
        <source>Charger automatiquement</source>
        <translation>Nahraj automaticky</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="487"/>
        <source>Charger les POIs preselectionnes</source>
        <translation>Nahraj předvybrané POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="579"/>
        <source>Tableau de marche</source>
        <translation>Detaily trasy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="643"/>
        <source> min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="630"/>
        <source>Interval</source>
        <translation>Interval</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="132"/>
        <source>Copier</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="416"/>
        <source>Trier les POIs par nom</source>
        <translation>POI podle abecedy</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="370"/>
        <source>Afficher infos du point d&apos;interpolation</source>
        <translation>Zobraz informace na bodech interpolace</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="440"/>
        <source>Trier les POIs par numero d&apos;ordre</source>
        <translation>POI podle pořadí</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="503"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="519"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;This route is not in BVMG-VLM mode so pilots orders timestamps and even route&apos;s way and trajectory might not be the same. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;Tato trasa není v módu VBVMG-VLM. Časy povelů pilota a dokonce i 
projetá trasa a trajektorie se nemusí shodovat.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="587"/>
        <source>Export (format CSV)</source>
        <translation>Export (CSV formát)</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="610"/>
        <source>Ecart HDG</source>
        <translation>Deviace HDG</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="620"/>
        <source> deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="677"/>
        <source>Statistiques</source>
        <translation>Statistiky</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="683"/>
        <source>Distance orthodromique</source>
        <translation>Orthodromická vzdálenost</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="700"/>
        <source>Distance parcourue</source>
        <translation>Projetá vzdálenost</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="717"/>
        <source>Vitesse moyenne</source>
        <translation>Průměrná rychlost</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="734"/>
        <source>TWS moyen</source>
        <translation>Průměr TWS</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="771"/>
        <source>Temps de navigation</source>
        <translation>Doba plavby</translation>
    </message>
    <message>
        <location filename="../src/Ui/Route_Editor.ui" line="778"/>
        <source>Duree moteur</source>
        <translation>Doba na motor</translation>
    </message>
</context>
<context>
    <name>ReceiverThread</name>
    <message>
        <location filename="../src/boatReal.cpp" line="458"/>
        <source>Activation du GPS</source>
        <translation>Aktivace GPS</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="459"/>
        <source>Impossible d&apos;ouvrir le port </source>
        <translation>Chyba při otevírání portu </translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="459"/>
        <source>Erreur: </source>
        <translation>Chyba: </translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="475"/>
        <source>Copier</source>
        <translation>Kopírovat do schránky</translation>
    </message>
</context>
<context>
    <name>SailDocs</name>
    <message>
        <location filename="../src/Ui/sailDocs.ui" line="14"/>
        <source>Dialog</source>
        <translation>SailDocs</translation>
    </message>
    <message>
        <location filename="../src/Ui/sailDocs.ui" line="22"/>
        <source>Destinataire de l&apos;email</source>
        <translation>Příjemce e-mailu</translation>
    </message>
    <message>
        <location filename="../src/Ui/sailDocs.ui" line="35"/>
        <source>query@saildocs.com</source>
        <translation>query@saildocs.com</translation>
    </message>
    <message>
        <location filename="../src/Ui/sailDocs.ui" line="45"/>
        <source>Sujet du mail indifferent</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <source>Sujet du mail indifférent</source>
        <translation type="obsolete">Email subject</translation>
    </message>
    <message>
        <location filename="../src/Ui/sailDocs.ui" line="56"/>
        <source>Corps:</source>
        <translation>Zpráva:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Ouverture des param�tres</source>
        <translation type="obsolete">Otevíráni parametrů</translation>
    </message>
    <message>
        <source>Ouverture des paramètres</source>
        <translation type="obsolete">Otevíráni parametrů</translation>
    </message>
    <message>
        <source>Il existe des parametres locaux, les importer dans qtvlm.ini?</source>
        <translation type="obsolete">Existje lokální nastavení, chceš ho importovat do qtVlm.ini ?</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../src/StatusBar.cpp" line="168"/>
        <location filename="../src/StatusBar.cpp" line="181"/>
        <location filename="../src/StatusBar.cpp" line="190"/>
        <location filename="../src/StatusBar.cpp" line="202"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="179"/>
        <source> Vent</source>
        <translation> Vítr</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="183"/>
        <location filename="../src/StatusBar.cpp" line="192"/>
        <location filename="../src/StatusBar.cpp" line="204"/>
        <source> kts</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="188"/>
        <location filename="../src/StatusBar.cpp" line="200"/>
        <source> Courant</source>
        <translation>Proud</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="213"/>
        <source>Selection: </source>
        <translation>Výběr:</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="221"/>
        <source>(dist.orthodromique:</source>
        <translation>(orthodromická vzdálenost:</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="224"/>
        <source>  init.dir: %1deg</source>
        <translation>  vých.směr: %1°</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="241"/>
        <source>Derniere synchro</source>
        <translation>Poslední synchronizace</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="241"/>
        <source>dd-MM-yyyy, HH:mm:ss</source>
        <translation>dd-MM-yyyy, HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/StatusBar.cpp" line="242"/>
        <source>Prochaine vac dans</source>
        <translation>Další obrat za</translation>
    </message>
</context>
<context>
    <name>Terrain</name>
    <message>
        <location filename="../src/Terrain.cpp" line="392"/>
        <source>Niveau de detail des cotes: </source>
        <translation>Úroveň podrobnosti pobřeží: </translation>
    </message>
    <message>
        <location filename="../src/Terrain.cpp" line="474"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/Terrain.cpp" line="476"/>
        <source>cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Terrain.cpp" line="478"/>
        <source>NM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Terrain.cpp" line="480"/>
        <source>(Max zoom reached)</source>
        <translation>(Dosaženo max přiblížení)</translation>
    </message>
    <message>
        <location filename="../src/Terrain.cpp" line="906"/>
        <source>Calculs en cours...</source>
        <translation>Počítám...</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>Misc</source>
        <translation type="obsolete">Různé</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="48"/>
        <source>Grib</source>
        <translation>Grib</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="51"/>
        <location filename="../src/ToolBar.cpp" line="146"/>
        <source>Selection</source>
        <translation>Výběr</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="54"/>
        <location filename="../src/ToolBar.cpp" line="160"/>
        <source>Estime</source>
        <translation>Odhad</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="57"/>
        <source>Boat</source>
        <translation>Loď</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="60"/>
        <source>ETA</source>
        <translation>ETA</translation>
    </message>
    <message>
        <source>Quitter</source>
        <translation type="obsolete">Ukončit</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation type="obsolete">Ctrl+Q</translation>
    </message>
    <message>
        <source>Bye</source>
        <translation type="obsolete">Čau</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="63"/>
        <source>Barrier Set</source>
        <translation>Sada bariér</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="87"/>
        <source>Hold to select download method</source>
        <translation>Drž pro výběr metody stahování</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="89"/>
        <source>Telechargement zyGrib</source>
        <translation>Stáhnout zyGrib</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="90"/>
        <source>Telechargement VLM</source>
        <translation>Stáhnout VLM</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="91"/>
        <source>Telechargement SailsDoc</source>
        <translation>Stáhnout SailDocs</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="92"/>
        <source>Open a grib</source>
        <translation>Otevřít grib</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="101"/>
        <source>Prevision precedente [page prec]</source>
        <translation>Předchozí předpověď [minulá strana]</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="102"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="103"/>
        <source>Prevision suivante [page suiv]</source>
        <translation>Příští předpověď [další strana]</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="104"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="105"/>
        <source>Animation du grib</source>
        <translation>Animace gribu</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="107"/>
        <source>Now</source>
        <translation>Teď</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="108"/>
        <source>Select date</source>
        <translation>Vyber datum</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="113"/>
        <source>15 m</source>
        <translation>15 m</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="114"/>
        <source>30 m</source>
        <translation>30 m</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="115"/>
        <source>1 h</source>
        <translation>1 h</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="116"/>
        <source>2 h</source>
        <translation>2 h</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="117"/>
        <source>3 h</source>
        <translation>3 h</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="118"/>
        <source>6 h</source>
        <translation>6 h</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="119"/>
        <source>12 h</source>
        <translation>12 h</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="136"/>
        <location filename="../src/ToolBar.cpp" line="137"/>
        <source>Augmenter l&apos;echelle de la carte</source>
        <translation>Přibliž mapu</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="138"/>
        <location filename="../src/ToolBar.cpp" line="139"/>
        <source>Diminuer l&apos;echelle de la carte</source>
        <translation>Oddal mapu</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="140"/>
        <source>Zoom (selection ou fichier Grib)</source>
        <translation>Zoom (výběr nebo Grib soubor)</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="141"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="141"/>
        <source>Zoomer sur la zone selectionnee ou sur la surface du fichier Grib</source>
        <translation>Přibliž na vybrané území nebo rozměr Grib souboru</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="143"/>
        <location filename="../src/ToolBar.cpp" line="144"/>
        <source>Afficher la carte entiere</source>
        <translation>Ukaž celou mapu</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="148"/>
        <source>Loupe</source>
        <translation>Lupou</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="165"/>
        <source>mins</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="166"/>
        <source>vacs</source>
        <translation>mezery</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="167"/>
        <source>NM</source>
        <translation>NM</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="170"/>
        <source>Si cette option est cochee&lt;br&gt;l&apos;estime calcule la vitesse du bateau&lt;br&gt;a la prochaine vac.&lt;br&gt;Sinon elle utilise la vitesse du bateau&lt;br&gt;telle que donnee par VLM</source>
        <translation>Pokud je tato volba zaškrtnuta&lt;br&gt;rychlost lodi je určena odhadem&lt;br&gt;v příští mezeře.&lt;br&gt;Jinak je použita rychlost dodaná VLM</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="179"/>
        <source>Verrouiller</source>
        <translation>Zámek</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="179"/>
        <source>Verrouiller l&apos;envoi d&apos;ordre a VLM</source>
        <translation>Zamknout/Odemknout posílání příkazů do VLM</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="186"/>
        <location filename="../src/ToolBar.cpp" line="311"/>
        <source>No WP</source>
        <translation>Není WP</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="191"/>
        <source>Add Barrier</source>
        <translation>Přidat bariéru</translation>
    </message>
    <message>
        <source>Tableau de bord</source>
        <translation type="obsolete">Palubní deska</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="327"/>
        <source>j</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="328"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="329"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="330"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="331"/>
        <source> Arrivee WP</source>
        <translation>Dosažení WP</translation>
    </message>
    <message>
        <location filename="../src/ToolBar.cpp" line="331"/>
        <source>dd-MM-yyyy, HH:mm:ss</source>
        <translation>dd-MM-yyyy, HH:mm:ss</translation>
    </message>
</context>
<context>
    <name>VLM_param_ui</name>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="20"/>
        <source>Parametrage VLM</source>
        <translation>Nastavení VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="43"/>
        <source>Affichage</source>
        <translation>Zobrazení</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="55"/>
        <source>Concurrents</source>
        <translation>Soupeři</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="61"/>
        <source>Label</source>
        <translation>Popisek</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="71"/>
        <source>Afficher les pavillons (F)</source>
        <translation>Kreslit vlajky na mapě (F)</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="684"/>
        <source>Routes</source>
        <translation>Trasy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="760"/>
        <source>Cacher les routes des bateaux non-selectionnes</source>
        <translation>Schovat trasy nevybraných lodí</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="81"/>
        <source>Police de caractere par defaut</source>
        <translation>Výchozí písmo</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="87"/>
        <source>Police</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <source>Facteur de taille</source>
        <translation type="obsolete">Size factor</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="97"/>
        <source>Taille</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <source>Divers</source>
        <translation type="obsolete">Různé</translation>
    </message>
    <message>
        <source>Boutons VLM classiques</source>
        <translation type="obsolete">Klasické čudlíky VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="243"/>
        <source>Couleurs</source>
        <translation>Barvy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="265"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="211"/>
        <location filename="../src/Ui/paramVLM.ui" line="258"/>
        <location filename="../src/Ui/paramVLM.ui" line="272"/>
        <location filename="../src/Ui/paramVLM.ui" line="318"/>
        <location filename="../src/Ui/paramVLM.ui" line="341"/>
        <location filename="../src/Ui/paramVLM.ui" line="385"/>
        <location filename="../src/Ui/paramVLM.ui" line="431"/>
        <source>Changer</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="311"/>
        <source>Waypoint</source>
        <translation>Waypoint</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="371"/>
        <source>Balise</source>
        <translation>Maják</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="364"/>
        <source>Marque est proc WP</source>
        <translation>Značka a další WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="378"/>
        <source>Bateau qtVlm</source>
        <translation>Loď qtVlm</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="408"/>
        <source>Bateau qtVlm sel.</source>
        <translation>Vybraná loď qtVlm.</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="448"/>
        <source>Bateau</source>
        <translation>Loď</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="454"/>
        <source>Calcul de l&apos;estime</source>
        <translation>Výpočet odhadu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="593"/>
        <source>En temps</source>
        <translation>Čas</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="558"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="466"/>
        <source>En nombre de vacations</source>
        <translation>Počet zlomů</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="517"/>
        <source>vac</source>
        <translation>zlom</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="479"/>
        <source>En distance</source>
        <translation>Vzdálenost</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="123"/>
        <source>Apparence</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="129"/>
        <location filename="../src/Ui/paramVLM.ui" line="139"/>
        <source>Redemarrage de qtVlm necessaire</source>
        <translation>Je třeba restartovat qtVlm</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="132"/>
        <source>Style Fusion</source>
        <translation>Styl Fusion</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="142"/>
        <source>Tableau de bord VLM classique</source>
        <translation>Klasická deska VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="151"/>
        <source>Skin par defaut</source>
        <translation>Výchozí skin</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="190"/>
        <source>Repertoire des cartes</source>
        <translation>Nabídka karet</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="202"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="498"/>
        <source>milles</source>
        <translation>mil</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="631"/>
        <source>Centrage sur bateau apres synchro VLM</source>
        <translation>Vycentrovat na loď po synchronizaci VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="644"/>
        <source>Centrage apres changement de bateau</source>
        <translation>Vycentrovat na loď po změně lodě</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="657"/>
        <source>Confirmation a chaque ordre vers VLM</source>
        <translation>Potvrdit každý příkaz do VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="690"/>
        <source>Parametres des virements/empanages</source>
        <translatorcomment>Tacks/Gybes</translatorcomment>
        <translation>Parametry obratů</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="703"/>
        <location filename="../src/Ui/paramVLM.ui" line="728"/>
        <source>Pourcentage de la polaire a appliquer pendant une vacation</source>
        <translation>Percentáž poláru použitá pro jednu mezeru po obratu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="712"/>
        <location filename="../src/Ui/paramVLM.ui" line="734"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1055"/>
        <source>Supprimer automatiquement les gribs de plus de 3 jours</source>
        <translation>Automatické odstranění gribů starších než 3 dny</translation>
    </message>
    <message>
        <source>Algorithme de simplification maximal</source>
        <translation type="obsolete">Algoritmus maximálního zjednodušení</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1078"/>
        <source>Demander egalement la pression atmospherique</source>
        <translation>Zeptej se taky na tlak vzduchu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1090"/>
        <source>Forcer le vent</source>
        <translation>Vynutit vítr</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1104"/>
        <source>TWS</source>
        <translation>TWS</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1114"/>
        <location filename="../src/Ui/paramVLM.ui" line="1177"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1128"/>
        <source>TWD</source>
        <translation>TWD</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1138"/>
        <location filename="../src/Ui/paramVLM.ui" line="1201"/>
        <source> deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1153"/>
        <source>Forcer le courant</source>
        <translation>Vynutit  proudů</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1167"/>
        <source>CS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1191"/>
        <source>CD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1403"/>
        <source>Mode d&apos;affichage du grib</source>
        <translation>Režim zobrazení gribu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1409"/>
        <source>MultiThread</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1419"/>
        <source>MonoThread</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1426"/>
        <source>Automatique</source>
        <translation>Automaticky</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="696"/>
        <source>pour les bateaux reels</source>
        <translation>pro skutečné lodě</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="750"/>
        <source>pour les bateaux VLM</source>
        <translation>pro VLM lodě</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="767"/>
        <source>Options par defaut</source>
        <translation>Výchozí nastavení</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="780"/>
        <source>Renseigner le cap a suivre (@) des POIs</source>
        <translation>Přidej informaci o kurzu (@) do POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="773"/>
        <source>Retirer les POIs avant le WP-VLM</source>
        <translation>Odstranit POI před WP-VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="787"/>
        <source>Trier les POIs par nom</source>
        <translation>POI podle abecedy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="794"/>
        <source>Trier les POIs par numero d&apos;ordre</source>
        <translation>POI podle pořadí</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="857"/>
        <source>Vitesse du replay</source>
        <translation>Rychlost přehrávání</translation>
    </message>
    <message>
        <source> vac</source>
        <translation type="obsolete">crank</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1003"/>
        <source>Demander l&apos;emplacement lors du telechargement</source>
        <translation>Dotázat se na destinaci během stahování</translation>
    </message>
    <message>
        <source>Date automatique a chaque synchro VLM</source>
        <translation type="obsolete">Automatické datum při každé synchronizaci VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1253"/>
        <source>Delais d&apos;envoi (sec)</source>
        <translation>Zpoždění odeslání (sek)</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1308"/>
        <source>Avance</source>
        <translation>Rozšířené</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1326"/>
        <source>Sauvegarde a la fermeture</source>
        <translation>Uložit při ukončení</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1370"/>
        <source>Interpolation des donnees GRIB</source>
        <translation>Interpolace Grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1376"/>
        <source>Hybride(VLM)</source>
        <translation>Hybridní(VLM)</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1386"/>
        <source>TWSA</source>
        <translation>TWSA</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1393"/>
        <source>Selective</source>
        <translation>Selektivní</translation>
    </message>
    <message>
        <source>Centrage sur bateau après synchro VLM</source>
        <translation type="obsolete">Center on boat after VLM sync</translation>
    </message>
    <message>
        <source>Centrage après changement de bateau</source>
        <translation type="obsolete">Center on boat after changing boat</translation>
    </message>
    <message>
        <source>Confirmation à chaque ordre vers VLM</source>
        <translation type="obsolete">Confirm each order to VLM</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="824"/>
        <source>Trace</source>
        <translation>Stopa</translation>
    </message>
    <message>
        <source>Pas de la trace</source>
        <translation type="obsolete">Trace step</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="830"/>
        <source>Longueur de la trace</source>
        <translation>Délka stopy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="896"/>
        <source>Afficher la trace des concurrents</source>
        <translation>Zobrazit stopy protivníků</translation>
    </message>
    <message>
        <source>vacations</source>
        <translation type="obsolete">crank</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="850"/>
        <source>heures</source>
        <translation>hodin</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="910"/>
        <source>Compas</source>
        <translation>Kompas</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="916"/>
        <source>Affichage du compas</source>
        <translation>Zobraz kompas</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="922"/>
        <source>Afficher le compas (C)</source>
        <translation>Zobraz kompas (C)</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="935"/>
        <source>Polaire</source>
        <translation>Polár</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="941"/>
        <source>Afficher la polaire (L)</source>
        <translation>Zobraz polár (L)</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="951"/>
        <source>Echelle fixe (50nds)</source>
        <translation>Fixní měřítko (50knots)</translation>
    </message>
    <message>
        <source>Nombre de vacations</source>
        <translation type="obsolete">Number of cranks</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="961"/>
        <source>Meme duree que l&apos;estime</source>
        <translation>Stejné trvání jako odhad</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="991"/>
        <source>Grib</source>
        <translation>Grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="997"/>
        <source>Fichier grib</source>
        <translation>Soubor Grib</translation>
    </message>
    <message>
        <source>Demander l&apos;emplacement lors du téléchargement</source>
        <translation type="obsolete">Ask for destination during download</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1012"/>
        <source>Repertoire</source>
        <translation>Složka</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="178"/>
        <location filename="../src/Ui/paramVLM.ui" line="1039"/>
        <source>Parcourir</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1048"/>
        <source>zoom sur grib au chargement</source>
        <translation>po stažení přiblížit na grib</translation>
    </message>
    <message>
        <source>Grib horodate</source>
        <translation type="obsolete">Časová značka Gribu</translation>
    </message>
    <message>
        <source>Date automatique à chaque synchro VLM</source>
        <translation type="obsolete">Automatic date at each VLM sync</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1065"/>
        <source>SailsDoc</source>
        <translation>SailDocs</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1071"/>
        <source>Utilisation d&apos;un client mail externe</source>
        <translation>Použít externí poštovní klient</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1225"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1231"/>
        <source>Emulation de GPS</source>
        <translation>Emulace GPS</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1239"/>
        <source>Port serie</source>
        <translation>Sériový port</translation>
    </message>
    <message>
        <source>Délais d&apos;envoi (sec)</source>
        <translation type="obsolete">Delay to send (sec)</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1276"/>
        <source>Activer</source>
        <translation>Aktivovat</translation>
    </message>
    <message>
        <source>Avancé</source>
        <translation type="obsolete">Advanced</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1358"/>
        <source>Url</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1314"/>
        <source>Position et taille de qtVlm</source>
        <translation>Pozice a velikost qtVlm</translation>
    </message>
    <message>
        <source>Sauvegarde à la fermeture</source>
        <translation type="obsolete">Save when closing</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1336"/>
        <source>User agent</source>
        <translation>User agent</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramVLM.ui" line="1342"/>
        <source>Force user agent</source>
        <translation>Vnutit user agent</translation>
    </message>
</context>
<context>
    <name>WP_dialog_ui</name>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="14"/>
        <source>Mon WP</source>
        <translation>Můj WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="74"/>
        <source>Lat</source>
        <translation>Lat</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="91"/>
        <source>Lon</source>
        <translation>Lon</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="105"/>
        <source>@WPH</source>
        <translation>@WPH</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="161"/>
        <source>000a00a00W</source>
        <translation>000a00a00W</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="39"/>
        <source>00a00a00N</source>
        <translation>00a00a00N</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="26"/>
        <source>Paste</source>
        <translation>Vložit</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="148"/>
        <source>Clear</source>
        <translation>Vyčistit</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="52"/>
        <source>Copy</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <source>Sauver</source>
        <translation type="obsolete">Save</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../src/Ui/WP_dialog.ui" line="128"/>
        <source>Selectionner une marque</source>
        <translation>Vybrat značku</translation>
    </message>
</context>
<context>
    <name>board</name>
    <message>
        <source>Mon bateau</source>
        <translation type="obsolete">My boat</translation>
    </message>
    <message>
        <location filename="../src/Board.cpp" line="45"/>
        <source>Tableau de bord VLM</source>
        <translation>VLM palubní deska</translation>
    </message>
    <message>
        <location filename="../src/Board.cpp" line="54"/>
        <location filename="../src/Board.cpp" line="92"/>
        <source>Tableau de bord</source>
        <translation>Palubní deska</translation>
    </message>
</context>
<context>
    <name>board VLM</name>
    <message>
        <source>Prochaine balise (0 WP)</source>
        <translation type="obsolete">Next beacon (0 WP)</translation>
    </message>
    <message>
        <source>Cacher compas</source>
        <translation type="obsolete">Hide compass</translation>
    </message>
    <message>
        <source>Erreur</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Pas de bateau selectionn�</source>
        <translation type="obsolete">No boat selected</translation>
    </message>
    <message>
        <source>Instruction pour</source>
        <translation type="obsolete">Instruction for</translation>
    </message>
    <message>
        <source>Confirmer le changement du WP</source>
        <translation type="obsolete">Confirm WP change</translation>
    </message>
    <message>
        <source>WP change</source>
        <translation type="obsolete">WP change</translation>
    </message>
    <message>
        <source>Confirmer le mode pilotage &apos;Cap&apos;</source>
        <translation type="obsolete">Confirm &apos;Constant heading&apos; pilot mode</translation>
    </message>
    <message>
        <source>Mode de pilotage change en &apos;Cap&apos;</source>
        <translation type="obsolete">Pilot mode changed to &apos;Constant heading&apos;</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <source>Confirmer le mode pilotage &apos;Angle&apos;</source>
        <translation type="obsolete">Confirm &apos;Constant angle&apos; pilot mode</translation>
    </message>
    <message>
        <source>Mode de pilotage change en &apos;Angle&apos;</source>
        <translation type="obsolete">Pilot mode changed to &apos;Constant angle&apos;</translation>
    </message>
    <message>
        <source>Confirmer le mode &apos;Pilot Ortho&apos;</source>
        <translation type="obsolete">Confirm &apos;Ortho&apos; pilot mode</translation>
    </message>
    <message>
        <source>Mode de pilotage change en &apos;Pilot Ortho&apos;</source>
        <translation type="obsolete">Pilot mode changed to &apos;Ortho&apos;</translation>
    </message>
    <message>
        <source>Confirmer le mode &apos;VMG&apos;</source>
        <translation type="obsolete">Confirm &apos;BestVMG&apos; pilot mode</translation>
    </message>
    <message>
        <source>Mode de pilotage change en &apos;VMG&apos;</source>
        <translation type="obsolete">Pilot mode changed to &apos;BestVMG&apos;</translation>
    </message>
    <message>
        <source>Confirmer le mode &apos;VBVMG&apos;</source>
        <translation type="obsolete">Confirm &apos;VBVMG&apos; pilot mode</translation>
    </message>
    <message>
        <source>Mode de pilotage change en &apos;VBVMG&apos;</source>
        <translation type="obsolete">Pilot mode changed to &apos;VBVMG&apos;</translation>
    </message>
    <message>
        <source>forcé</source>
        <translation type="obsolete">forced</translation>
    </message>
    <message>
        <source>erreur chargement</source>
        <translation type="obsolete">error loading</translation>
    </message>
    <message>
        <source>Information sur</source>
        <translation type="obsolete">Info about</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation type="obsolete">Alias:</translation>
    </message>
    <message>
        <source>ID:</source>
        <translation type="obsolete">ID:</translation>
    </message>
    <message>
        <source>Nom:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation type="obsolete">Email:</translation>
    </message>
    <message>
        <source>Course:</source>
        <translation type="obsolete">Race:</translation>
    </message>
    <message>
        <source>Score:</source>
        <translation type="obsolete">Score:</translation>
    </message>
    <message>
        <source>Polaire:</source>
        <translation type="obsolete">Polar:</translation>
    </message>
    <message>
        <source>GPS synchronisation</source>
        <translation type="obsolete">GPS synchronization</translation>
    </message>
    <message>
        <source>Impossible d&apos;envoyer les donnees sur le port serie</source>
        <translation type="obsolete">Impossible to send data through serial port</translation>
    </message>
    <message>
        <source>Cacher le compas</source>
        <translation type="obsolete">Hide compass</translation>
    </message>
    <message>
        <source>Afficher le compas</source>
        <translation type="obsolete">Show compass</translation>
    </message>
</context>
<context>
    <name>boardReal</name>
    <message>
        <location filename="../src/BoardReal.cpp" line="48"/>
        <source>Cacher compas</source>
        <translation>Schovat kompas</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="201"/>
        <source>nds</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="142"/>
        <location filename="../src/BoardReal.cpp" line="202"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="142"/>
        <source>Inclure la declinaison (</source>
        <translation>Zahrnout deklinaci (</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="203"/>
        <source>Pres</source>
        <translation>Proti větru</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="204"/>
        <source>Portant</source>
        <translation>Po větru</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="224"/>
        <source>Running</source>
        <translation>Po větru</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="226"/>
        <source>no signal</source>
        <translation>bez signálu</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="228"/>
        <source>Fix quality</source>
        <translation>Kvalita signálu</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="230"/>
        <source>Differential quality</source>
        <translation>Rozdílová kvalita</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="232"/>
        <source>Sensitive quality</source>
        <translation>Kvalita citlivosti</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="234"/>
        <source>no position</source>
        <translation>není pozice</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="236"/>
        <source>2D position</source>
        <translation>2D pozice</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="238"/>
        <source>3D position</source>
        <translation>3D pozice</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="243"/>
        <source>Ideal accuracy</source>
        <translation>Ideální přesnost</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="245"/>
        <source>Excellent accuracy</source>
        <translation>Vynikající přesnost</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="247"/>
        <source>Good accuracy</source>
        <translation>Dobrá přesnost</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="249"/>
        <source>Moderate accuracy</source>
        <translation>Průměrná přesnost</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="251"/>
        <source>Not so good accuracy</source>
        <translation>Nic moc přesnost</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="253"/>
        <source>Very bad accuracy</source>
        <translation>Velice špatná přesnost</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="314"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <source>Bientot des infos ici</source>
        <translation type="obsolete">Soon more info from here</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="337"/>
        <source>Start GPS</source>
        <translation>Start GPS</translation>
    </message>
    <message>
        <location filename="../src/BoardReal.cpp" line="346"/>
        <source>Stop GPS</source>
        <translation>Stop GPS</translation>
    </message>
    <message>
        <source>Cacher le compas</source>
        <translation type="obsolete">Hide compass</translation>
    </message>
    <message>
        <source>Afficher le compas</source>
        <translation type="obsolete">Show compass</translation>
    </message>
</context>
<context>
    <name>boardReal_ui</name>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="26"/>
        <source>Board</source>
        <translation>Deska</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="46"/>
        <source>00a00&apos;00&apos;&apos; W</source>
        <translation>00a00&apos;00&apos;&apos; W</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="63"/>
        <source>000a00&apos;00&apos;&apos; W</source>
        <translation>000a00&apos;00&apos;&apos; W</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="109"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="118"/>
        <source>Deplacer le bateau</source>
        <translation>Přesunout loď</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="133"/>
        <source>Vitesse et Cap</source>
        <translation>Rychlost a kurs</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="189"/>
        <source>Declinaison</source>
        <translation>Deklinace</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="201"/>
        <source>Inclure la declinaison</source>
        <translation>Zahrnout deklinaci</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="217"/>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="253"/>
        <source>Ortho</source>
        <translation>Ortho</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="283"/>
        <location filename="../src/Ui/BoardReal.ui" line="378"/>
        <location filename="../src/Ui/BoardReal.ui" line="429"/>
        <source>999.99</source>
        <translation>999.99</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="313"/>
        <location filename="../src/Ui/BoardReal.ui" line="357"/>
        <source>a</source>
        <translation>za</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="333"/>
        <source>Loxo</source>
        <translation>Loxo</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="405"/>
        <source>DNM</source>
        <translation>DNM</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="453"/>
        <source>nm</source>
        <translation>nm</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="473"/>
        <source>VMG</source>
        <translation>VMG</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="500"/>
        <source>99.99</source>
        <translation>99.99</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="527"/>
        <source>kts</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="545"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="557"/>
        <source>Start GPS</source>
        <translation>Start GPS</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="564"/>
        <source>GPS Status</source>
        <translation>Stavy GPS</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="587"/>
        <source>GPS status loading</source>
        <translation>Nahrávání stavu GPS</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardReal.ui" line="605"/>
        <source>Wind information (from grib)</source>
        <translation>Informace o větru (z gribu)</translation>
    </message>
    <message>
        <source>Stop GPS</source>
        <translation type="obsolete">Stop GPS</translation>
    </message>
    <message>
        <source>GPS status:</source>
        <translation type="obsolete">GPS status:</translation>
    </message>
    <message>
        <source>status</source>
        <translation type="obsolete">status</translation>
    </message>
</context>
<context>
    <name>boardVLM</name>
    <message>
        <location filename="../src/BoardVLM.cpp" line="91"/>
        <location filename="../src/BoardVLM.cpp" line="598"/>
        <source>Prochaine balise (0 WP)</source>
        <translation>Další maják (0 WP)</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="111"/>
        <source>Cacher compas</source>
        <translation>Schovat kompas</translation>
    </message>
    <message>
        <source>Angles au degre</source>
        <translation type="obsolete">Angles at degree</translation>
    </message>
    <message>
        <source>Angles au dixieme</source>
        <translation type="obsolete">Angles at tenth</translation>
    </message>
    <message>
        <source>Angles au centieme</source>
        <translation type="obsolete">Angles at cents</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="246"/>
        <source>Erreur</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <source>Pas de bateau selectionn�</source>
        <translation type="obsolete">No boat selected</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="246"/>
        <source>Pas de bateau selectionne</source>
        <translation>Není vybrána loď</translation>
    </message>
    <message>
        <source>Instruction pour</source>
        <translation type="obsolete">Instruction for</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="435"/>
        <source>Confirmer le changement du WP</source>
        <translation>Potvrď změnu WP</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="435"/>
        <source>WP change</source>
        <translation>Změň WP</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="441"/>
        <source>Confirmer le mode pilotage &apos;Cap&apos;</source>
        <translation>Potvrď mód pilota &apos;Konstantní kurs&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="441"/>
        <source>Mode de pilotage change en &apos;Cap&apos;</source>
        <translation>Mód pilota byl změněn na &apos;Konstantní kurs&apos;</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="680"/>
        <source>Confirmer le mode pilotage &apos;Angle&apos;</source>
        <translation>Potvrď mód pilota &apos;Konstantní úhel&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="680"/>
        <source>Mode de pilotage change en &apos;Angle&apos;</source>
        <translation>Mód pilota změněn na&apos;Konstantní úhel&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="702"/>
        <source>Confirmer le mode &apos;Pilot Ortho&apos;</source>
        <translation>Potvrď &apos;Ortho&apos; mód pilota</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="702"/>
        <source>Mode de pilotage change en &apos;Pilot Ortho&apos;</source>
        <translation>Mód pilota změněn na &apos;Ortho&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="708"/>
        <source>Confirmer le mode &apos;VMG&apos;</source>
        <translation>Potvrď mód pilota &apos;BestVMG&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="708"/>
        <source>Mode de pilotage change en &apos;VMG&apos;</source>
        <translation>Mód pilota změněn na &apos;BestVMG&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="714"/>
        <source>Confirmer le mode &apos;VBVMG&apos;</source>
        <translation>Potvrď mód pilota &apos;VBVMG&apos;</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="714"/>
        <source>Mode de pilotage change en &apos;VBVMG&apos;</source>
        <translation>Mód pilota změněn na &apos;VBVMG&apos;</translation>
    </message>
    <message>
        <source>forcé</source>
        <translation type="obsolete">forced</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="253"/>
        <source>Instruction pour </source>
        <translation>Instrukce pro </translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="600"/>
        <source>Pas de WP actif</source>
        <translation>Žádný aktivní WP</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="623"/>
        <source>WP defini dans VLM (pas de POI correspondant)</source>
        <translation>WP definován ve VLM (žádný korespondující POI v qtVlm)</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="630"/>
        <location filename="../src/BoardVLM.cpp" line="635"/>
        <source>WP defini dans VLM (</source>
        <translatorcomment>WP defined in VLM (</translatorcomment>
        <translation>WP definován ve VLM (</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="630"/>
        <location filename="../src/BoardVLM.cpp" line="635"/>
        <source> dans qtVlm)</source>
        <translation> v qtVlm)</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="636"/>
        <source>Le cap a suivre n&apos;est pas le meme</source>
        <translation>Varování: kurs po WP je jiný</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="643"/>
        <source>WP defini dans VLM mais le mode de navigation n&apos;est pas coherent</source>
        <translation>WP definován ve VLM, ale mód navigace není kompatibilní</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="665"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="726"/>
        <source>forcee</source>
        <translation>vynucený</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="733"/>
        <source>erreur chargement</source>
        <translation>chyba při nahrávání</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="736"/>
        <source>Information sur</source>
        <translation>Informace o</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="737"/>
        <source>Alias:</source>
        <translation>Přezdívka:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="738"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="739"/>
        <source>Pseudo:</source>
        <translation>Přezdívka:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="740"/>
        <source>Nom:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="741"/>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="742"/>
        <source>Course:</source>
        <translation>Závod:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="743"/>
        <source>Score:</source>
        <translation>Skóre:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="744"/>
        <source>Polaire:</source>
        <translation>Polár:</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="820"/>
        <location filename="../src/BoardVLM.cpp" line="835"/>
        <location filename="../src/BoardVLM.cpp" line="853"/>
        <location filename="../src/BoardVLM.cpp" line="874"/>
        <location filename="../src/BoardVLM.cpp" line="889"/>
        <source>GPS synchronisation</source>
        <translation>GPS synchronizace</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="821"/>
        <location filename="../src/BoardVLM.cpp" line="836"/>
        <location filename="../src/BoardVLM.cpp" line="854"/>
        <location filename="../src/BoardVLM.cpp" line="875"/>
        <location filename="../src/BoardVLM.cpp" line="890"/>
        <source>Impossible d&apos;envoyer les donnees sur le port serie</source>
        <translation>Nelze poslat data přes sériový port</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="1059"/>
        <source>Cacher le compas</source>
        <translation>Skrýt kompas</translation>
    </message>
    <message>
        <location filename="../src/BoardVLM.cpp" line="1061"/>
        <source>Afficher le compas</source>
        <translation>Zobrazit kompas</translation>
    </message>
</context>
<context>
    <name>boardVLM_ui</name>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="32"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="85"/>
        <source>Nom bateau</source>
        <translation>Jméno lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="123"/>
        <source>Score</source>
        <translation>Skóre</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="154"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <source>00a00&apos;00&apos;&apos; W</source>
        <translation type="obsolete">00a00&apos;00&apos;&apos; W</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="193"/>
        <location filename="../src/Ui/BoardVLM.ui" line="210"/>
        <source>000a00&apos;00&apos;&apos; W</source>
        <translation>000a00&apos;00&apos;&apos; W</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="230"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="264"/>
        <source>VLM Sync</source>
        <translation>VLM Sync</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="267"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="303"/>
        <source>Pilototo (x/x)</source>
        <translation>Autopilot (x/x)</translation>
    </message>
    <message>
        <source>Vitesse et Cap</source>
        <translation type="obsolete">Speed and Heading</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="364"/>
        <source> Vitesse et Cap</source>
        <translation>Rychlost a kurs</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="542"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1725"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1753"/>
        <source>background-color: rgb(239, 243, 247);</source>
        <translation>barva pozadí: rgb(239, 243, 247);</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="545"/>
        <source>Heading</source>
        <translation>Kurs</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="712"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1926"/>
        <source>&lt;shift&gt;,&lt;control&gt; et &lt;tab&gt; pour changer la precision de l&apos;increment</source>
        <translation>&lt;shift&gt;, &lt;control&gt; a &lt;tab&gt; pro změnu hodnoty jednoho kroku</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="721"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1935"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="798"/>
        <source>Bateau</source>
        <translation>Loď</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="825"/>
        <location filename="../src/Ui/BoardVLM.ui" line="936"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1263"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1578"/>
        <source>99.99</source>
        <translation>99.99</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="850"/>
        <location filename="../src/Ui/BoardVLM.ui" line="871"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1290"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1596"/>
        <source>kts</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="751"/>
        <source>Avg</source>
        <translation>Prům</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="891"/>
        <source>Loch</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="775"/>
        <source>9999.99</source>
        <translation>9999.99</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="322"/>
        <source>Clear</source>
        <translation>Vyčistit</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="915"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1216"/>
        <source>nm</source>
        <translation>nm</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="980"/>
        <source>WP direction</source>
        <translation>Směr WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1016"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1411"/>
        <source>Ortho</source>
        <translation>Ortho</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1046"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1141"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1192"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1656"/>
        <location filename="../src/Ui/BoardVLM.ui" line="2029"/>
        <location filename="../src/Ui/BoardVLM.ui" line="2109"/>
        <source>999.99</source>
        <translation>999.99</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1076"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1120"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1673"/>
        <location filename="../src/Ui/BoardVLM.ui" line="2046"/>
        <location filename="../src/Ui/BoardVLM.ui" line="2126"/>
        <source>a</source>
        <translation>za</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1096"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1728"/>
        <source>Angle</source>
        <translation>Úhel</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1168"/>
        <source>DNM</source>
        <translation>DNM</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1236"/>
        <location filename="../src/Ui/BoardVLM.ui" line="1380"/>
        <source>VMG</source>
        <translation>VMG</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1337"/>
        <source>PushButton</source>
        <translation>Tlačítko</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1442"/>
        <source>VBVMG</source>
        <translation>VBVMG</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1531"/>
        <source>Vent</source>
        <translation>Vítr</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1702"/>
        <source>background-color: rgb(170, 170, 255);</source>
        <translation>barva pozadí: rgb(170, 170, 255);</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1756"/>
        <source>+/-</source>
        <translation>+/-</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1560"/>
        <source>Speed</source>
        <translation>Rychlost</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1641"/>
        <source>Dir</source>
        <translation>Směr</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="1989"/>
        <source>Meilleur VMG vent</source>
        <translation>Nejlepší VMG vítr</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="2014"/>
        <source>Pres</source>
        <translation>Proti větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/BoardVLM.ui" line="2091"/>
        <source>Portant</source>
        <translation>Po větru</translation>
    </message>
</context>
<context>
    <name>boat</name>
    <message>
        <location filename="../src/boat.cpp" line="169"/>
        <source>Center on boat</source>
        <translation>Centrovat na loď</translation>
    </message>
    <message>
        <location filename="../src/boat.cpp" line="179"/>
        <location filename="../src/boat.cpp" line="919"/>
        <location filename="../src/boat.cpp" line="930"/>
        <source>Tirer un cap</source>
        <translation>Zakreslit kurs</translation>
    </message>
    <message>
        <location filename="../src/boat.cpp" line="184"/>
        <source>Tracer une estime TWA</source>
        <translation>Zakreslit předpoklad gribu</translation>
    </message>
    <message>
        <location filename="../src/boat.cpp" line="190"/>
        <source>Activate barrier sets</source>
        <translation>Aktivovat sady bariér</translation>
    </message>
    <message>
        <location filename="../src/boat.cpp" line="924"/>
        <source>Arret du cap</source>
        <translation>Zastavit kurs</translation>
    </message>
    <message>
        <location filename="../src/boat.cpp" line="936"/>
        <source>Cacher estime</source>
        <translation>Skrýt odhad</translation>
    </message>
    <message>
        <location filename="../src/boat.cpp" line="938"/>
        <source>Afficher estime</source>
        <translation>Zobrazit odhad</translation>
    </message>
</context>
<context>
    <name>boatAccount_dialog</name>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="17"/>
        <source>Boat account</source>
        <translation>Lodní účet</translation>
    </message>
    <message>
        <source>Polaire</source>
        <translation type="obsolete">Polár</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="363"/>
        <source>Forcer la polaire</source>
        <translation>Vynutit polár</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="331"/>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="345"/>
        <source>Alias</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>Activer l&apos;alias</source>
        <translation type="obsolete">Aktivovat přezdívku</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="315"/>
        <source>Actif</source>
        <translation>Aktivní</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="322"/>
        <source>Bloquer modif</source>
        <translation>Zamknout nastavení</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="45"/>
        <source>Bateaux du compte</source>
        <translation>Lodě účtu</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="111"/>
        <source>Boat sitting</source>
        <translation>Opečovávání lodí</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="215"/>
        <source>Pseudo</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="222"/>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="236"/>
        <source>TextLabel</source>
        <translation>Textlabel</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="229"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="394"/>
        <source>Utiliser un skin specifique</source>
        <translation>Použít určitý styl</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="424"/>
        <source>Parcourir</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <location filename="../src/Ui/boatAccount_dialog.ui" line="464"/>
        <source>Note: La mise a jour de la liste des bateaux disponibles se fait dans l&apos;ecran &quot;compte&quot;</source>
        <translation>Poznámka: aktualizace seznamu dostupných lodí by mělo být prováděno v okně &quot;Účet&quot;</translation>
    </message>
</context>
<context>
    <name>boatReal</name>
    <message>
        <location filename="../src/boatReal.cpp" line="349"/>
        <source>Hdg</source>
        <translation>Hdg</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="140"/>
        <source>Definir la position</source>
        <translation>Nastavit pozici</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="349"/>
        <location filename="../src/boatReal.cpp" line="352"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="352"/>
        <source>Angle</source>
        <translation>Úhel</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="355"/>
        <source>Ortho</source>
        <translation>Ortho</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="358"/>
        <source>BVMG</source>
        <translation>BestVMG</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="361"/>
        <source>VBVMG</source>
        <translation>VBVMG</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="365"/>
        <source> (pas de polaire chargee)</source>
        <translation>(není načten polár)</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="366"/>
        <source> (format CSV)</source>
        <translation> (formát CSV)</translation>
    </message>
    <message>
        <location filename="../src/boatReal.cpp" line="367"/>
        <source> (format POL)</source>
        <translation> (formát POL)</translation>
    </message>
    <message>
        <source>(pas de polaire chargee)</source>
        <translation type="obsolete">(no polar loaded)</translation>
    </message>
    <message>
        <source>(pas de polaire charg�e)</source>
        <translation type="obsolete">(no polar loaded)</translation>
    </message>
    <message>
        <source>(format CSV)</source>
        <translation type="obsolete">(CSV format)</translation>
    </message>
    <message>
        <source>(format POL)</source>
        <translation type="obsolete">(POL format)</translation>
    </message>
</context>
<context>
    <name>boatVLM</name>
    <message>
        <location filename="../src/boatVLM.cpp" line="658"/>
        <source>Arrivee&lt;br&gt;</source>
        <translation>Cíl&lt;br&gt;</translation>
    </message>
    <message>
        <source>Une seule bouee a laisser au</source>
        <translation type="obsolete">Single buoy, leave to</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="650"/>
        <source>Passer au moins une fois au Sud de cette ligne&lt;br&gt;</source>
        <translation>Pluj alespoň jednou jižně od této linie</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="651"/>
        <location filename="../src/boatVLM.cpp" line="654"/>
        <source>Vous n&apos;etes pas oblige de couper la ligne</source>
        <translation>Nemusíš překročit linii</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="653"/>
        <source>Passer au moins une fois au Nord de cette ligne&lt;br&gt;</source>
        <translation>Pluj alespoň jednou severně od této linie&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="663"/>
        <location filename="../src/boatVLM.cpp" line="665"/>
        <source>Une seule bouee a laisser au </source>
        <translation>Jedna bóje, nech na </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="668"/>
        <source>Passage a deux bouees</source>
        <translation>Brána se dvěma bójemi</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="670"/>
        <source>&lt;br&gt;A passer dans le sens des aiguilles d&apos;une montre</source>
        <translation>&lt;br&gt;Objet po směru hodinových ručiček</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="672"/>
        <source>&lt;br&gt;A passer dans le sens contraire des aiguilles d&apos;une montre</source>
        <translation>&lt;br&gt;Objet proti směru hodinových ručiček</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="674"/>
        <source>&lt;br&gt;Sens du passage libre</source>
        <translation>&lt;br&gt;Směr průjezdu libovolný</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="676"/>
        <source>&lt;br&gt;Il est interdit de couper cette ligne plusieurs fois</source>
        <translation>&lt;br&gt;Zakázáno projet více než jednou</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="678"/>
        <source>&lt;br&gt;Il est autorise de couper cette ligne plusieurs fois</source>
        <translation>&lt;br&gt;Počet průjezdů volný</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="721"/>
        <source> est introuvable sur le site VLM</source>
        <translation> nebyl nalezen na webu VLM</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="886"/>
        <source>Hdg</source>
        <translation>Hdg</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="886"/>
        <location filename="../src/boatVLM.cpp" line="889"/>
        <location filename="../src/boatVLM.cpp" line="917"/>
        <location filename="../src/boatVLM.cpp" line="937"/>
        <location filename="../src/boatVLM.cpp" line="952"/>
        <location filename="../src/boatVLM.cpp" line="953"/>
        <location filename="../src/boatVLM.cpp" line="989"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="902"/>
        <source> (pas de polaire chargee)</source>
        <translation>(není načten polár)</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="903"/>
        <source> (format CSV)</source>
        <translation> (formát CSV)</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="904"/>
        <source> (format POL)</source>
        <translation> (formát POL)</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="908"/>
        <source>Bateau echoue, pour encore </source>
        <translation>Loď na útesech, pro </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="908"/>
        <source>secondes</source>
        <translation>sekund</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="916"/>
        <source>Donnees GRIB a la derniere vac:</source>
        <translation>Grib data v předchozí mezeře:</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="929"/>
        <location filename="../src/boatVLM.cpp" line="949"/>
        <source>BS polaire: </source>
        <translation>BS polár:</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="936"/>
        <source>Donnees GRIB a la prochaine vac:</source>
        <translation>Grib data v příští mezeře:</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="989"/>
        <source>Prochaine porte: </source>
        <translation>Příští brána: </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="992"/>
        <source>kts</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="929"/>
        <location filename="../src/boatVLM.cpp" line="949"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="141"/>
        <source>Pilotage au cap ?</source>
        <translation>Řídit podle kurzu?</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="141"/>
        <source>Mode de pilotage change en &apos;Cap&apos;</source>
        <translation>Pilot mode changed to &apos;Constant heading&apos;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="150"/>
        <source>Pilotage a angle constant par rapport au vent ?</source>
        <translation>Řídit podle konstantního úhlu k větru?</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="150"/>
        <source>Mode de pilotage change en &apos;Angle&apos;</source>
        <translation>Pilot mode changed to &apos;Constant angle&apos;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="159"/>
        <source>Pilotage orthodromique ?</source>
        <translation>Řídit po orthodromě?</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="159"/>
        <source>Mode de pilotage change en &apos;Pilot Ortho&apos;</source>
        <translation>Pilot mode changed to &apos;Ortho&apos;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="167"/>
        <source>Pilotage en VMG ?</source>
        <translation>Řídit podle VMG?</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="167"/>
        <source>Mode de pilotage change en &apos;VMG&apos;</source>
        <translation>Pilot mode changed to &apos;VMG&apos;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="175"/>
        <source>Pilotage en VBVMG ?</source>
        <translation>Řídit podle VBVMG?</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="175"/>
        <source>Mode de pilotage change en &apos;VBVMG&apos;</source>
        <translation>Mód řízení změněn na &apos;VBVMG&apos;</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="184"/>
        <source>Confirmer le changement du WP</source>
        <translation>Potvrď změnu WP</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="184"/>
        <source>WP change</source>
        <translation>Změň WP</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="205"/>
        <source>Instruction pour </source>
        <translation>Instrukce pro </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="952"/>
        <source>BVMG vent (pres): </source>
        <translation>BVMG vítr (proti větru): </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="953"/>
        <source>BVMG vent (portant): </source>
        <translation>BVMG vítr (po větru): </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="956"/>
        <source>Tendance a l&apos;estime: </source>
        <translation>Předpokládaná tendence: </translation>
    </message>
    <message>
        <source>Tendance: </source>
        <translation type="obsolete">Tendency:</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="964"/>
        <source>stable.</source>
        <translation>stabilní.</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="966"/>
        <source>force stable</source>
        <translation>stabilní síla</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="968"/>
        <source>mollissant</source>
        <translation>slábnutí</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="970"/>
        <source>forcissant</source>
        <translation>zesilování</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="974"/>
        <source>, direction stable</source>
        <translation>, stejný směr</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="978"/>
        <source> en refusant</source>
        <translation>, stáčení proti</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="980"/>
        <source> en adonnant</source>
        <translation> , stáčení k zádi</translation>
    </message>
    <message>
        <source>(pas de polaire chargee)</source>
        <translation type="obsolete">(no polar loaded)</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="889"/>
        <source>Angle</source>
        <translation>Úhel</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="892"/>
        <source>Ortho</source>
        <translation>Ortho</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="895"/>
        <source>BVMG</source>
        <translation>BestVMG</translation>
    </message>
    <message>
        <location filename="../src/boatVLM.cpp" line="898"/>
        <source>VBVMG</source>
        <translation>VBVMG</translation>
    </message>
    <message>
        <source>(pas de polaire charg�e)</source>
        <translation type="obsolete">(no polar loaded)</translation>
    </message>
    <message>
        <source>(format CSV)</source>
        <translation type="obsolete">(CSV format)</translation>
    </message>
    <message>
        <source>(format POL)</source>
        <translation type="obsolete">(POL format)</translation>
    </message>
</context>
<context>
    <name>dialogFaxMeteo</name>
    <message>
        <source>Fichiers Fax (*.png *.jpg *.tiff *.bmp)</source>
        <translation type="obsolete">Fax files (*.png *.jpg *.tiff *.bmp)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogFaxMeteo.cpp" line="143"/>
        <source>Fichiers Fax (*.png *.jpg *.tiff *.gif *.bmp)</source>
        <translation>Fax soubory (*.png *.jpg *.tiff *.gif *.bmp)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogFaxMeteo.cpp" line="144"/>
        <source>;;Autres fichiers (*)</source>
        <translation>;;Jiné soubory (*)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogFaxMeteo.cpp" line="154"/>
        <source>Choisir un fichier FAX METEO</source>
        <translation>Vyber meteo fax soubor</translation>
    </message>
</context>
<context>
    <name>dialogFaxMeteo_ui</name>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="14"/>
        <source>Dialog</source>
        <translation>Meteo Fax</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="43"/>
        <source>Position du coin gauche</source>
        <translation>Pozice levého rohu</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="122"/>
        <source>Nord</source>
        <translation>Sever</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="127"/>
        <source>Sud</source>
        <translation>Jih</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="55"/>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="138"/>
        <source> °</source>
        <translation> °</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="74"/>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="157"/>
        <source> &apos;</source>
        <translation> &apos;</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="101"/>
        <source>Est</source>
        <translation>Východ</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="106"/>
        <source>Ouest</source>
        <translation>Západ</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="114"/>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="192"/>
        <source>Latitude</source>
        <translation>Zem. šířka</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="93"/>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="185"/>
        <source>Longitude</source>
        <translation>Zem. délka</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="24"/>
        <source>Nom du fichier Fax</source>
        <translation>Jméno souboru s faxem</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="33"/>
        <source>Parcourir</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="179"/>
        <source>Etendue</source>
        <translation>Rozsah</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="202"/>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="221"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="240"/>
        <source>Transparence</source>
        <translation>Průhlednost</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="306"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="313"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="320"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="327"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="343"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Vous pouvez aussi utiliser la souris Pour bouger l&apos;image (touche SHIFT enfoncee), pour la retailler (touche CTRL enfoncee), ou pour regler la transparence (touche ALT enfoncee)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Můžeš také použít myš pro posun (s klávesou SHIFT), pro změnu velikosti (klávesa CTRL), nebo k vyladění průhlednosti (klávesa ALT)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="269"/>
        <source>Configuration number</source>
        <translation>Číslo konfigurace</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="275"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="285"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="292"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogFaxMeteo.ui" line="299"/>
        <source>4</source>
        <translation>4</translation>
    </message>
</context>
<context>
    <name>dialogLoadImg</name>
    <message>
        <location filename="../src/Dialogs/dialogLoadImg.cpp" line="50"/>
        <source>Fichier Kap invalide</source>
        <translation>Nesprávný soubor KAP</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogLoadImg.cpp" line="72"/>
        <source>Fichiers kap (*.kap *.KAP)</source>
        <translation>Kap soubory (*.kap *.KAP)</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/dialogLoadImg.cpp" line="82"/>
        <source>Choisir un fichier kap</source>
        <translation>Vyber KAP soubor</translation>
    </message>
</context>
<context>
    <name>dialogLoadImg_ui</name>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="14"/>
        <source>Ouvrir un fichier KAP</source>
        <translation>Otevři KAP soubor</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="24"/>
        <source>Nom du fichier Kap</source>
        <translation>Jméno KAP souboru</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="46"/>
        <source>Parcourir</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="69"/>
        <location filename="../src/Ui/dialogLoadImg.ui" line="131"/>
        <source>Opacite</source>
        <translation>Průhlednost</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="82"/>
        <source>Grib</source>
        <translation>Grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="88"/>
        <source>Ne pas colorer le grib</source>
        <translation>Neobarvovat grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialogLoadImg.ui" line="95"/>
        <source>Afficher au dessus du KAP</source>
        <translation>Zobrazit přes KAP</translation>
    </message>
</context>
<context>
    <name>dialog_gribDate_ui</name>
    <message>
        <location filename="../src/Ui/dialog_gribDate.ui" line="20"/>
        <source>Grib dates</source>
        <translation>Grib data</translation>
    </message>
    <message>
        <location filename="../src/Ui/dialog_gribDate.ui" line="29"/>
        <source>M/d/yyyy hh:mm:ss</source>
        <translation>MM/dd/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <source>Valider</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
</context>
<context>
    <name>faxMeteo</name>
    <message>
        <location filename="../src/faxMeteo.cpp" line="248"/>
        <source>Trop de zoom pour le faxMeteo</source>
        <translation>Moc přiblíženo pro meteo fax</translation>
    </message>
</context>
<context>
    <name>finePosit_ui</name>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Etendue de la recherche (longitude)</source>
        <translation type="obsolete">Search range (longitude)</translation>
    </message>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="25"/>
        <source>Ecart initial (longitude)</source>
        <translation>Počáteční mezera (zem. délka)</translation>
    </message>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="77"/>
        <source>Precision de la recherche</source>
        <translation>Přesnost vyhledávání</translation>
    </message>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="108"/>
        <source>Afficher les routes calculees</source>
        <translation>Zobraz vypočtené trasy</translation>
    </message>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="128"/>
        <source>Ecart automatique</source>
        <translation>Automatická mezera</translation>
    </message>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="51"/>
        <source>Ecart initial (latitude)</source>
        <translation>Počáteční mezera (zem. šířka)</translation>
    </message>
    <message>
        <location filename="../src/Ui/finePosit.ui" line="118"/>
        <source>Conserver une copie du POI apres le calcul</source>
        <translation>Zachovat kopii WP po výpočtu</translation>
    </message>
    <message>
        <source>Précision de la recherche</source>
        <translation type="obsolete">Search precision</translation>
    </message>
    <message>
        <source>Afficher les routes calculées</source>
        <translation type="obsolete">Show calculated routes</translation>
    </message>
    <message>
        <source>Etendue de la recherche (latitude)</source>
        <translation type="obsolete">Search range (latitude)</translation>
    </message>
</context>
<context>
    <name>gribValidation</name>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="14"/>
        <source>Validation de l&apos;interpolation</source>
        <translation>Ověřit interpolaci</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <source>Le type est egalement utilisé pour l&apos;affichage du grib</source>
        <translation type="obsolete">Type is also used for grib display</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="58"/>
        <source>Le type est egalement utilise pour l&apos;affichage du grib</source>
        <translation>Typ je také použit pro zobrazení gribu</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="67"/>
        <source>Prametres du calcul</source>
        <translation>Nastavení výpočtu</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="76"/>
        <source>Latitude</source>
        <translation>Zem. šířka</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="86"/>
        <source>Longitude</source>
        <translation>Zem. délka</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="96"/>
        <source>Tstamp</source>
        <translation>Časová značka</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="106"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="118"/>
        <source>Resultat du calcul</source>
        <translation>Výsledek výpočtu</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="124"/>
        <source>Vitesse</source>
        <translation>Rychlost</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="145"/>
        <source>Angle</source>
        <translation>Úhel</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="157"/>
        <source>now</source>
        <translation>teď</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="164"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="25"/>
        <source>Type d&apos;interpolation</source>
        <translation>Druh interpolace</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="33"/>
        <source>TWSA</source>
        <translation>TWSA</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="38"/>
        <source>Selective TWSA</source>
        <translation>Selektivní TWSA</translation>
    </message>
    <message>
        <location filename="../src/Ui/gribValidation.ui" line="43"/>
        <source>Hybrid</source>
        <translation>Hybrid</translation>
    </message>
</context>
<context>
    <name>inetConn_progressDialog_ui</name>
    <message>
        <source>Download progess</source>
        <translation type="obsolete">Download progess</translation>
    </message>
    <message>
        <location filename="../src/Ui/inetConn_progessDialog.ui" line="20"/>
        <source>Download progress</source>
        <translation>Průběh stahování</translation>
    </message>
    <message>
        <location filename="../src/Ui/inetConn_progessDialog.ui" line="32"/>
        <location filename="../src/Ui/inetConn_progessDialog.ui" line="49"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>instruction_ui</name>
    <message>
        <location filename="../src/Ui/instructions.ui" line="29"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="35"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="54"/>
        <source>Colle l&apos;instruction depuis le press-papier</source>
        <translation>Vložit instrukce ze schránky</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="70"/>
        <source>Synchronise l&apos;heure</source>
        <translation>Synchronizovat čas</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="89"/>
        <source>Valide l&apos;instruction</source>
        <translation>Potvrdit instrukce</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="108"/>
        <source>Annule les changements</source>
        <translation>Zrušit změny</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="118"/>
        <source>d/M/yyyy HH:mm:ss</source>
        <translation>d/M/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <source>d/M/yyyy hh:mm:ss</source>
        <translation type="obsolete">d/m/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="147"/>
        <source>Copier l&apos;instruction</source>
        <translation>Kopírovat instrukci</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="163"/>
        <location filename="../src/Ui/instructions.ui" line="166"/>
        <source>Edition complete</source>
        <translation>Upravit vše</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="179"/>
        <source>Selection d&apos;un WP</source>
        <translation>Výběr WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="182"/>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/instructions.ui" line="195"/>
        <source>Supprime l&apos;instruction</source>
        <translation>Smazat instrukci</translation>
    </message>
</context>
<context>
    <name>mapCompass</name>
    <message>
        <source>Vent:</source>
        <translation type="obsolete">Wind:</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="396"/>
        <source>nds</source>
        <translation>kts</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="396"/>
        <location filename="../src/mapcompass.cpp" line="416"/>
        <location filename="../src/mapcompass.cpp" line="421"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <source>WP:</source>
        <translation type="obsolete">WP:</translation>
    </message>
    <message>
        <source>WP%vent:</source>
        <translation type="obsolete">WP%wind:</translation>
    </message>
    <message>
        <source>nm</source>
        <translation type="obsolete">nm</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="396"/>
        <source>Vent: </source>
        <translation>Vítr:</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="416"/>
        <location filename="../src/mapcompass.cpp" line="426"/>
        <source>WP: </source>
        <translation>WP:</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="421"/>
        <source>WP%vent: </source>
        <translation>WP%vítr:</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="426"/>
        <source> nm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="683"/>
        <source>Centimetres</source>
        <translation>Centimetry</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="685"/>
        <source>Metres</source>
        <translation>Metry</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="694"/>
        <location filename="../src/mapcompass.cpp" line="699"/>
        <source>Collision avec les terres detectee</source>
        <translation>Kolize s pevninou detekována</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="763"/>
        <source>jours</source>
        <translation>dny</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="763"/>
        <source>heures</source>
        <translation>hodiny</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="764"/>
        <source>minutes</source>
        <translation>minuty</translation>
    </message>
    <message>
        <location filename="../src/mapcompass.cpp" line="765"/>
        <source>secondes</source>
        <translation>sekundy</translation>
    </message>
</context>
<context>
    <name>myCentralWidget</name>
    <message>
        <source>Téléchargement d&apos;un fichier GRIB</source>
        <translation type="obsolete">Download a GRIB file</translation>
    </message>
    <message>
        <source>Vous devez sélectionner une zone de la carte.</source>
        <translation type="obsolete">You must select a zone on the map.</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1390"/>
        <source>Demande d&apos;un fichier GRIB a sailsDoc</source>
        <translation>Vyžádat soubor GRIB u SailDocs</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1341"/>
        <location filename="../src/mycentralwidget.cpp" line="1391"/>
        <source>Vous devez selectionner une zone de la carte.</source>
        <translation>Musíš vybrat na mapě zónu.</translation>
    </message>
    <message>
        <source>Chargement des cartes VLM</source>
        <translation type="obsolete">Nahrávání map VLM</translation>
    </message>
    <message>
        <source>Vous n&apos;avez pas la bonne version des cartes VLM</source>
        <translation type="obsolete">Nemáš správnou verzi map VLM</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1340"/>
        <source>Telechargement d&apos;un fichier GRIB</source>
        <translation>Stáhnout GRIB soubor</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1364"/>
        <source>dd/MM/yyyy hh:mm</source>
        <translation>dd/MM/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1405"/>
        <location filename="../src/mycentralwidget.cpp" line="1422"/>
        <source>oui</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1409"/>
        <location filename="../src/mycentralwidget.cpp" line="1435"/>
        <source>non</source>
        <translation>ne</translation>
    </message>
    <message>
        <source>oui (calculé par la formule de Magnus-Tetens)</source>
        <translation type="obsolete">yes (using the Magnus-Tetens formula)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1447"/>
        <location filename="../src/mycentralwidget.cpp" line="1507"/>
        <source>Informations sur le fichier GRIB</source>
        <translation>Informace o GRIB souboru</translation>
    </message>
    <message>
        <source>Aucun fichir GRIB n&apos;est chargé.</source>
        <translation type="obsolete">No GRIB file loaded.</translation>
    </message>
    <message>
        <source>Fichier : %1</source>
        <translation type="obsolete">File : %1</translation>
    </message>
    <message>
        <source>Taille : %1 octets</source>
        <translation type="obsolete">Size : %1 bytes</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="802"/>
        <location filename="../src/mycentralwidget.cpp" line="811"/>
        <location filename="../src/mycentralwidget.cpp" line="835"/>
        <location filename="../src/mycentralwidget.cpp" line="838"/>
        <location filename="../src/mycentralwidget.cpp" line="845"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>%1 enregistrements,</source>
        <translation type="obsolete">%1 registration,</translation>
    </message>
    <message>
        <source>%1 dates :</source>
        <translation type="obsolete">%1 dates :</translation>
    </message>
    <message>
        <source>du %1</source>
        <translation type="obsolete">from %1</translation>
    </message>
    <message>
        <source>au %1</source>
        <translation type="obsolete">to %1</translation>
    </message>
    <message>
        <source>Données disponibles :</source>
        <translation type="obsolete">Available data :</translation>
    </message>
    <message>
        <source>Température : %1</source>
        <translation type="obsolete">Temperature : %1</translation>
    </message>
    <message>
        <source>Pression : %1</source>
        <translation type="obsolete">Pressure : %1</translation>
    </message>
    <message>
        <source>Vent  : %1</source>
        <translation type="obsolete">Wind  : %1</translation>
    </message>
    <message>
        <source>Cumul de précipitations : %1</source>
        <translation type="obsolete">Cumul of precipitation : %1</translation>
    </message>
    <message>
        <source>Nébulosité : %1</source>
        <translation type="obsolete">Cloud cover : %1</translation>
    </message>
    <message>
        <source>Humidité relative : %1</source>
        <translation type="obsolete">Relative humidity : %1</translation>
    </message>
    <message>
        <source>Isotherme 0 C : %1</source>
        <translation type="obsolete">Isotherm 0 C : %1</translation>
    </message>
    <message>
        <source>Point de rosée : %1</source>
        <translation type="obsolete">Dewpoint : %1</translation>
    </message>
    <message>
        <source>Température (min) : %1</source>
        <translation type="obsolete">Temperature (min) : %1</translation>
    </message>
    <message>
        <source>Température (max) : %1</source>
        <translation type="obsolete">Temperature (max) : %1</translation>
    </message>
    <message>
        <source>Température (pot) : %1</source>
        <translation type="obsolete">Temperature (pot) : %1</translation>
    </message>
    <message>
        <source>Neige (risque) : %1</source>
        <translation type="obsolete">Snow (risk) : %1</translation>
    </message>
    <message>
        <source>Neige (épaisseur) : %1</source>
        <translation type="obsolete">Snow (depth) : %1</translation>
    </message>
    <message>
        <source>Humidité spécifique :</source>
        <translation type="obsolete">Specific humidity :</translation>
    </message>
    <message>
        <source>- 200: %1</source>
        <translation type="obsolete">- 200: %1</translation>
    </message>
    <message>
        <source>- 300: %1</source>
        <translation type="obsolete">- 300: %1</translation>
    </message>
    <message>
        <source>- 500: %1</source>
        <translation type="obsolete">- 500: %1</translation>
    </message>
    <message>
        <source>- 700: %1</source>
        <translation type="obsolete">- 700: %1</translation>
    </message>
    <message>
        <source>- 850: %1</source>
        <translation type="obsolete">- 850: %1</translation>
    </message>
    <message>
        <source>Grille : %1 points (%2x%3)</source>
        <translation type="obsolete">Grid : %1 points (%2x%3)</translation>
    </message>
    <message>
        <source>Etendue :</source>
        <translation type="obsolete">Extension :</translation>
    </message>
    <message>
        <source>%1  -&gt;  %2</source>
        <translation type="obsolete">%1  -&gt;  %2</translation>
    </message>
    <message>
        <source>Date de référence : %1</source>
        <translation type="obsolete">Reference date : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1414"/>
        <source>oui (calcule par la formule de Magnus-Tetens)</source>
        <translation>ano (použít vzorec Magnus-Tetens)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1448"/>
        <source>Aucun fichir GRIB n&apos;est charge.</source>
        <translation>Není nahrán GRIB soubor.</translation>
    </message>
    <message>
        <source>Donnees disponibles :</source>
        <translation type="obsolete">Available data :</translation>
    </message>
    <message>
        <source>Temperature : %1</source>
        <translation type="obsolete">Temperature : %1</translation>
    </message>
    <message>
        <source>Nebulosite : %1</source>
        <translation type="obsolete">Cloud cover : %1</translation>
    </message>
    <message>
        <source>Humidite relative : %1</source>
        <translation type="obsolete">Relative humidity : %1</translation>
    </message>
    <message>
        <source>Isotherme 0degC : %1</source>
        <translation type="obsolete">Isotherm 0 C : %1</translation>
    </message>
    <message>
        <source>Point de rosee : %1</source>
        <translation type="obsolete">Dewpoint : %1</translation>
    </message>
    <message>
        <source>Temperature (min) : %1</source>
        <translation type="obsolete">Temperature (min) : %1</translation>
    </message>
    <message>
        <source>Temperature (max) : %1</source>
        <translation type="obsolete">Temperature (max) : %1</translation>
    </message>
    <message>
        <source>Temperature (pot) : %1</source>
        <translation type="obsolete">Temperature (pot) : %1</translation>
    </message>
    <message>
        <source>Neige (epaisseur) : %1</source>
        <translation type="obsolete">Snow (depth) : %1</translation>
    </message>
    <message>
        <source>Humidite specifique :</source>
        <translation type="obsolete">Specific humidity :</translation>
    </message>
    <message>
        <source>Date de reference : %1</source>
        <translation type="obsolete">Reference date : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1520"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1649"/>
        <source>Suppression de toutes les marques</source>
        <translation>Smazání všech značek</translation>
    </message>
    <message>
        <source>La destruction d&apos;une marque est definitive.  Etes-vous sur ?</source>
        <translation type="obsolete">Deleting a mark is final.  Are you sure?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1451"/>
        <source>Fichier : %1
</source>
        <translation>Soubor : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1452"/>
        <source>Taille : %1 octets
</source>
        <translation>Velikost : %1 bytů</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1453"/>
        <location filename="../src/mycentralwidget.cpp" line="1464"/>
        <location filename="../src/mycentralwidget.cpp" line="1491"/>
        <location filename="../src/mycentralwidget.cpp" line="1495"/>
        <location filename="../src/mycentralwidget.cpp" line="1502"/>
        <source>
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1455"/>
        <source>%1 enregistrements, </source>
        <translation>%1 registrace,</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1456"/>
        <source>%1 dates :
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1460"/>
        <source>    du %1
</source>
        <translation>   od %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1461"/>
        <source>    au %1
</source>
        <translation>   do %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1465"/>
        <source>Donnees disponibles :
</source>
        <translation>Dostipná data :
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1466"/>
        <source>    Temperature : %1
</source>
        <translation>    Teplota : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1467"/>
        <source>    Pression : %1
</source>
        <translation>    Tlak : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1468"/>
        <source>    Vent  : %1
</source>
        <translation>    Vítr  : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1469"/>
        <source>    Courant  : %1
</source>
        <translation>    Proud  : %</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1470"/>
        <source>    Cumul de précipitations : %1
</source>
        <translation>   Úhrn srážek : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1471"/>
        <source>    Nebulosite : %1
</source>
        <translation>    Oblačnost : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1472"/>
        <source>    Humidite relative : %1
</source>
        <translation>    Relativní vlhkost : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1473"/>
        <source>    Isotherme 0degC : %1
</source>
        <translation>    Isotherma 0°C : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1474"/>
        <source>    Point de rosee : %1
</source>
        <translation>    Rosný bod : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1475"/>
        <source>    Temperature (min) : %1
</source>
        <translation>    Teplota (min) : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1476"/>
        <source>    Temperature (max) : %1
</source>
        <translation>    Teplota (max) : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1477"/>
        <source>    Temperature (pot) : %1
</source>
        <translation>    Teplota (pot) : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1478"/>
        <source>    Neige (risque) : %1
</source>
        <translation>    Sníh (riziko) : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1479"/>
        <source>    Neige (epaisseur) : %1
</source>
        <translation>    Sníh (pokrývka) : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1480"/>
        <source>    Humidite specifique :
</source>
        <translation>    Specifická vlhkost :
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1481"/>
        <source>        - 200: %1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1482"/>
        <source>        - 300: %1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1483"/>
        <source>        - 500: %1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1484"/>
        <source>        - 700: %1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1485"/>
        <source>        - 850: %1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1492"/>
        <source>Grille : %1 points (%2x%3)
</source>
        <translation>Grid : %1 body (%2x%3)
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1494"/>
        <source>Resolution : %1x%2
</source>
        <translation>Rozlišení : %1x%2</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1496"/>
        <source>Etendue :
</source>
        <translation>Rozsah :
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1500"/>
        <source>%1  -&gt;  %2
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1503"/>
        <source>Date de reference : %1
</source>
        <translation>Referenční datum : %1
</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1650"/>
        <source>La destruction d&apos;une marque est definitive.

Etes-vous sur ?</source>
        <translation>Smazání značky nelze vrátit.

Jsi si jist?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1848"/>
        <source>Creating a new barrier</source>
        <translation>Vytvořit novou bariéru</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1849"/>
        <source>Point must be inside map
Please select another point or press esc to exit barrier creation mode</source>
        <translation>Bod musí být na mapě
Vyber jiný bod nebo stiskni Esc pro opuštění módu vytváření bariér</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2009"/>
        <source>Photo Ecran</source>
        <translation>Snímek obrazovky</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2129"/>
        <location filename="../src/mycentralwidget.cpp" line="2132"/>
        <source>Ouvrir un fichier Route</source>
        <translation>Otevřít soubor trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2201"/>
        <location filename="../src/mycentralwidget.cpp" line="2313"/>
        <location filename="../src/mycentralwidget.cpp" line="2513"/>
        <source>Nom de la route a importer</source>
        <translation>Jméno trasy pro import</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2202"/>
        <location filename="../src/mycentralwidget.cpp" line="2313"/>
        <location filename="../src/mycentralwidget.cpp" line="2513"/>
        <source>Nom de la route</source>
        <translation>Jméno trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2212"/>
        <location filename="../src/mycentralwidget.cpp" line="2323"/>
        <location filename="../src/mycentralwidget.cpp" line="2522"/>
        <location filename="../src/mycentralwidget.cpp" line="3526"/>
        <source>Ce nom est deja utilise ou invalide</source>
        <translation>Toto jméno už je použito nebo je neplatné</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2218"/>
        <location filename="../src/mycentralwidget.cpp" line="2642"/>
        <location filename="../src/mycentralwidget.cpp" line="3039"/>
        <location filename="../src/mycentralwidget.cpp" line="3532"/>
        <source>Import de routes</source>
        <translation>Import tras</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2219"/>
        <location filename="../src/mycentralwidget.cpp" line="3533"/>
        <source>Import en cours, veuillez patienter...</source>
        <translation>Probíhá import, prosím počkej...</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2611"/>
        <source>Exporter une Route</source>
        <translation>Export trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2631"/>
        <source>Exporter seulement les POIs ou egalement tous les details?</source>
        <translation>Exportovat jen POIs nebo i detaily?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2632"/>
        <source>Exporter une route</source>
        <translation>Export trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2634"/>
        <source>POIs</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2635"/>
        <source>Details</source>
        <translation>Detaily</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2643"/>
        <location filename="../src/mycentralwidget.cpp" line="3040"/>
        <source>Export en cours, veuillez patienter...</source>
        <translation>Probíhá export, prosím počkej...</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2856"/>
        <source>Le nom de la route est invalide</source>
        <translation>Jméno trasy je neplatné</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2866"/>
        <source>Importer la route : %1</source>
        <translation>Import trasy: %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="2867"/>
        <source>Cette route existe deja.

Voulez-vous la remplacer?</source>
        <translation>Tato trasa už existuje.

Chceš ji nahradit?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3137"/>
        <location filename="../src/mycentralwidget.cpp" line="3155"/>
        <location filename="../src/mycentralwidget.cpp" line="3179"/>
        <location filename="../src/mycentralwidget.cpp" line="3186"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3622"/>
        <source>Routage%d</source>
        <translation>Routing%d</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3724"/>
        <location filename="../src/mycentralwidget.cpp" line="3831"/>
        <source>aucun</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3726"/>
        <source>Optimisation en cours</source>
        <translation>Optimizace probíhá</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3727"/>
        <source>Veuillez patienter...</source>
        <translation>Prosím čekej...</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3816"/>
        <source>Optimisation terminee</source>
        <translation>Optimizace dokončena</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3820"/>
        <source>Dernier POI avant optimisation: %1 (ETA: %2)</source>
        <translation>Poslední POI před optimalizací: %1 (ETA: %2)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3827"/>
        <location filename="../src/mycentralwidget.cpp" line="3830"/>
        <source>Dernier POI apres optimisation: %1 (ETA: %2)</source>
        <translation>Poslední POI po optimalizaci: %1 (ETA: %2)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3866"/>
        <source> (maximum)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3866"/>
        <source> (minimum)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3867"/>
        <source>Simplification en cours</source>
        <translation>Probíhá zjednodušení</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3870"/>
        <source>Abandonner</source>
        <translation>Zastavit</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3872"/>
        <location filename="../src/mycentralwidget.cpp" line="3920"/>
        <location filename="../src/mycentralwidget.cpp" line="3960"/>
        <location filename="../src/mycentralwidget.cpp" line="4011"/>
        <source>Phase </source>
        <translation>Fáze </translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3714"/>
        <source>Optimiser</source>
        <translation>Optimalizovat</translation>
    </message>
    <message>
        <source>Ouverture des cartes</source>
        <translation type="obsolete">Otevření map</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="1431"/>
        <source>oui (GRIB Courants)</source>
        <translation>ano (GRIB proudů)</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3803"/>
        <source> minutes perdues</source>
        <translation> minut ztraceno</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3805"/>
        <source> minutes gagnees</source>
        <translation> minut ušetřeno</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3813"/>
        <source>ETA avant optimisation: </source>
        <translation>ETA před optimalizací: </translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3814"/>
        <source>ETA apres optimisation: </source>
        <translation>ETA po optimalizaci: </translation>
    </message>
    <message>
        <source>Phase 1...</source>
        <translation type="obsolete">Phase 1...</translation>
    </message>
    <message>
        <source>Phase 2...</source>
        <translation type="obsolete">Phase 2...</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3696"/>
        <source> minutes perdues, </source>
        <translation> minut ztraceno, </translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3699"/>
        <location filename="../src/mycentralwidget.cpp" line="3806"/>
        <source> POIs supprimes sur </source>
        <translation> POI vypuštěno z </translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3698"/>
        <source> minutes gagnees(!), </source>
        <translation> minut ušetřeno(!), </translation>
    </message>
    <message>
        <source>Choisir un repertoire</source>
        <translation type="obsolete">Vybrat rozsah</translation>
    </message>
    <message>
        <source>Telechargement</source>
        <translation type="obsolete">Stáhnout</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="621"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Les cartes sont absentes
Que voulez vous faire?</source>
        <translation type="obsolete">Chybí mapy
Co chceš dělat?</translation>
    </message>
    <message>
        <source>Vous n&apos;avez pas la bonne version des cartes
Que voulez vous faire?</source>
        <translation type="obsolete">Máš špatnou verzi map.
Co chceš dělat?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="649"/>
        <source>Select maps folder</source>
        <translation>Vyber složku map</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3708"/>
        <source>ETA avant simplification: </source>
        <translation>ETA před zjednodušením: </translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3709"/>
        <source>ETA apres simplification: </source>
        <translation>ETA po zjednodušení: </translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3653"/>
        <source>Envoyer la route au pilototo</source>
        <translation>Pošli trasu do autopilota</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="618"/>
        <source>Maps loading</source>
        <translation>Mapy se nahrávaji</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="619"/>
        <source>Select existing maps folder</source>
        <translation>Vyber existující složku map</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="620"/>
        <source>Downloading</source>
        <translation>Stahování</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="626"/>
        <source>Maps are missing
What do you want to do?</source>
        <translation>Mapy chybí
Co chceš dělat?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="631"/>
        <source>An old version of maps has been detected
What do you want to do?</source>
        <translation>Byla detekována stará verze map.
Co chceš dělat?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="675"/>
        <source>Download and decompress maps</source>
        <translation>Stáhnout a rozbalit mapy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="681"/>
        <source>Finishing map init</source>
        <translation>Ukončit inicializaci map</translation>
    </message>
    <message>
        <source>    Cumul de prÃ©cipitations : %1
</source>
        <translation type="obsolete">    Úhrn srážek : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="3653"/>
        <source>Pour pouvoir envoyer la route au pilototo if faut que:&lt;br&gt;-La route demarre du bateau et de la prochaine vac&lt;br&gt;et que le mode VbVmg-Vlm soit active</source>
        <translation>Abys mohl poslat trasu do autopilota, je třeba:&lt;br&gt;-Trasa musí začínat od lodi a příští mezery&lt;br&gt;-Musí být aktivován mód VbVmg ve Vlm</translation>
    </message>
    <message>
        <source>Suppression d&apos;une route</source>
        <translation type="obsolete">Delete a route</translation>
    </message>
    <message>
        <source>Vous ne pouvez pas supprimer une route figee</source>
        <translation type="obsolete">You cannot delete a frozen route</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4135"/>
        <source>Detruire la route : %1</source>
        <translation>Smazat trasu : %1</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4136"/>
        <source>La destruction d&apos;une route est definitive.

Voulez-vous egalement supprimer tous les POIs lui appartenant?</source>
        <translation>Smazání trasy nelze zvrátit. Chceš také smazat POI této trasy?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4165"/>
        <source>Detruire le routage : %1?</source>
        <translation>Smazat routing: %1?</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4166"/>
        <source>La destruction d&apos;un routage est definitive.</source>
        <translation>Smazání routingu nelze zvrátit.</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4636"/>
        <source>Sauvegarde des POIs et des routes</source>
        <translation>Uložit POI a trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4636"/>
        <source>Sauvegarde reussie</source>
        <translation>Uložení proběhlo úspěšně</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4662"/>
        <source>Chargement des POIs et des routes</source>
        <translation>Nahrát POI a trasy</translation>
    </message>
    <message>
        <location filename="../src/mycentralwidget.cpp" line="4662"/>
        <source>Chargement reussi</source>
        <translation>Znovunačtení úspěšné</translation>
    </message>
    <message>
        <source>ETA avant simplification:</source>
        <translation type="obsolete">ETA before simplification:</translation>
    </message>
    <message>
        <source>ETA apres simplification:</source>
        <translation type="obsolete">ETA after simplification:</translation>
    </message>
</context>
<context>
    <name>myScene</name>
    <message>
        <source>Gesture</source>
        <translation type="obsolete">Gesto</translation>
    </message>
    <message>
        <source>Pan gesture detected</source>
        <translation type="obsolete">Gesto tažení detekováno</translation>
    </message>
    <message>
        <source>Other gesture detected!!</source>
        <translation type="obsolete">Jiné gesto detekováno!!</translation>
    </message>
</context>
<context>
    <name>opponent</name>
    <message>
        <source>Classement:</source>
        <translation type="obsolete">Ranking:</translation>
    </message>
    <message>
        <source>Loch 1h:</source>
        <translation type="obsolete">Loch 1h:</translation>
    </message>
    <message>
        <source>Loch 3h:</source>
        <translation type="obsolete">Loch 3h:</translation>
    </message>
    <message>
        <source>Loch 24h:</source>
        <translation type="obsolete">Loch 24h:</translation>
    </message>
    <message>
        <source>Status VLM:</source>
        <translation type="obsolete">VLM status:</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="433"/>
        <source>Classement: </source>
        <translation>Pořadí: </translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="434"/>
        <source>Loch 1h: </source>
        <translation>Log 1h:</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="435"/>
        <source>Loch 3h: </source>
        <translation>Log 3h:</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="436"/>
        <source>Loch 24h: </source>
        <translation>Log 24h:</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="437"/>
        <source>Status VLM: </source>
        <translation>VLM status: </translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="446"/>
        <location filename="../src/opponentBoat.cpp" line="480"/>
        <source>Vitesse estimee: </source>
        <translation>Odhad rychlosti: </translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="449"/>
        <location filename="../src/opponentBoat.cpp" line="483"/>
        <source>Cap estime: </source>
        <translation>Odhad kursu: </translation>
    </message>
    <message>
        <source>Vitesse estimee </source>
        <translation type="obsolete">Estimated speed </translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="448"/>
        <location filename="../src/opponentBoat.cpp" line="482"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <source>Cap estime</source>
        <translation type="obsolete">Estimated heading</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="451"/>
        <location filename="../src/opponentBoat.cpp" line="485"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="455"/>
        <source>Trop loin pour estimer le cap et la vitesse</source>
        <translation>Příliš daleko pro odhad rychlosti a kursu</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="457"/>
        <source>Distance: </source>
        <translation>Vzdálenost:</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="459"/>
        <source> NM</source>
        <translation> NM</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="473"/>
        <source>Id: </source>
        <translation>Id:</translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="474"/>
        <source>Date de la position: </source>
        <translation>Datum pozice: </translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="476"/>
        <source>Latitude:  </source>
        <translation>Zem. šířka: </translation>
    </message>
    <message>
        <location filename="../src/opponentBoat.cpp" line="477"/>
        <source>Longitude: </source>
        <translation>Zem. délka: </translation>
    </message>
    <message>
        <source>nds</source>
        <translation type="obsolete">kts</translation>
    </message>
</context>
<context>
    <name>paramAccount</name>
    <message>
        <location filename="../src/Ui/paramAccount.ui" line="14"/>
        <source>Paramétrage Compte</source>
        <translation>Nastavení účtu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramAccount.ui" line="23"/>
        <source>Identifiant</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramAccount.ui" line="33"/>
        <source>Mot de passe</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramAccount.ui" line="57"/>
        <source>Type of boat</source>
        <translation>Typ lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramAccount.ui" line="63"/>
        <source>Real boat</source>
        <translation>Skutečná loď</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramAccount.ui" line="70"/>
        <source>VLM boat</source>
        <translation>Loď VLM</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <source>Vrai bateau</source>
        <translation type="obsolete">Real boat</translation>
    </message>
    <message>
        <source>Bateau VLM</source>
        <translation type="obsolete">VLM boat</translation>
    </message>
</context>
<context>
    <name>paramProxy_ui</name>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="14"/>
        <source>Proxy parameter</source>
        <translation>Parametr proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="20"/>
        <source>Direct connection to internet</source>
        <translation>Přímé připojení k internetu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="30"/>
        <source>Connection using a proxy</source>
        <translation>Připojení pomocí proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="40"/>
        <source>Proxy config</source>
        <translation>Konfigurace proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="46"/>
        <source>Proxy type</source>
        <translation>Typ proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="60"/>
        <source>System default proxy</source>
        <translation>Výchozí systémová proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="65"/>
        <source>HTTP proxy</source>
        <translation>HTTP proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="70"/>
        <source>SOCKS5 proxy</source>
        <translation>SOCKS5 proxy</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="78"/>
        <source>Proxy server</source>
        <translation>Proxy server</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="88"/>
        <source>Port number</source>
        <translation>Číslo portu</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="115"/>
        <source>User *</source>
        <translation>Uživatel *</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="125"/>
        <source>Password *</source>
        <translation>Heslo *</translation>
    </message>
    <message>
        <location filename="../src/Ui/paramProxy.ui" line="139"/>
        <source>(* if needed)</source>
        <translation>(*pokud je třeba)</translation>
    </message>
</context>
<context>
    <name>pilototo_param_ui</name>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="14"/>
        <source>Instruction du pilototo</source>
        <translation>Instrukce autopilota</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="54"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="122"/>
        <source>Longitude</source>
        <translation>Zem. délka</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="130"/>
        <source>Est</source>
        <translation>Východ</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="135"/>
        <source>Ouest</source>
        <translation>Západ</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <source>&apos;</source>
        <translation type="obsolete">&apos;</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="83"/>
        <location filename="../src/Ui/Pilototo_param.ui" line="143"/>
        <source> °</source>
        <translation> °</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="102"/>
        <location filename="../src/Ui/Pilototo_param.ui" line="162"/>
        <source> &apos;</source>
        <translation> &apos;</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="182"/>
        <source>Prochain cap</source>
        <translation>Příští kurs</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="62"/>
        <source>Latitude</source>
        <translation>Zem. šířka</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="70"/>
        <source>Nord</source>
        <translation>Sever</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="75"/>
        <source>Sud</source>
        <translation>Jih</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="222"/>
        <source>Selectionner une marque</source>
        <translation>Vybrat značku</translation>
    </message>
    <message>
        <source>Valider</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="25"/>
        <source>Mode de pilotage</source>
        <translation>Mód pilota</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo_param.ui" line="35"/>
        <source>Cap / Angle</source>
        <translation>Kurs / Úhel</translation>
    </message>
</context>
<context>
    <name>pilototo_ui</name>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="23"/>
        <source>Pilototo</source>
        <translation>Autopilot</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="34"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="46"/>
        <source>Horadate actuelle</source>
        <translation>Aktuální časová značka</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="60"/>
        <source>Rafraichir</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="112"/>
        <source>N / 5 instructions</source>
        <translation>N / 5 instrukcí</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="132"/>
        <source>Valider</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Ui/Pilototo.ui" line="145"/>
        <source>Annuler</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>playerAccount</name>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="14"/>
        <source>Gestion des comptes</source>
        <translation>Správa účtů</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="69"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="103"/>
        <source>Nouveau</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="113"/>
        <source>Modifier</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="139"/>
        <source>Mise a jour</source>
        <translation>Aktualizovat</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="162"/>
        <source>Supprimer</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="186"/>
        <source>Language/Langage/Idioma</source>
        <translation></translation>
    </message>
    <message>
        <source>Language/Langage</source>
        <translation type="obsolete">Language/Langage/Jazyk</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="192"/>
        <source>Francais</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="202"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="209"/>
        <source>Spanish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="216"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="79"/>
        <source>nb boat</source>
        <translation>č. lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/playerAccount.ui" line="62"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
</context>
<context>
    <name>race_dialog_ui</name>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="14"/>
        <source>Paramétre des courses</source>
        <translation>Nastavení závodů</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="39"/>
        <source> courses</source>
        <translation> závody</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="222"/>
        <source> deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="302"/>
        <source>Aucun</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="323"/>
        <source>Cette information peut ne pas etre disponible</source>
        <translation>Tato informace není vždy k dispozici</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="326"/>
        <source>Afficher les reels</source>
        <translation>Ukaž skutečné lodě</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="333"/>
        <source>Filtrer les reels</source>
        <translation>Filtrovat skutečné</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="336"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="32"/>
        <source>XXX</source>
        <translation>XXX</translation>
    </message>
    <message>
        <source>courses</source>
        <translation type="obsolete">races</translation>
    </message>
    <message>
        <source>Appliquer</source>
        <translation type="obsolete">Apply</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="184"/>
        <source>Ligne de No Sail Zone</source>
        <translation>Hranice No Sail zóny</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="158"/>
        <source>Arrives</source>
        <translation>Dojezd</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="192"/>
        <source>Afficher</source>
        <translation>Ukázat</translation>
    </message>
    <message>
        <source>deg</source>
        <translation type="obsolete">deg</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="212"/>
        <source>Latitude</source>
        <translation>Zem. šířka</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="248"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="253"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="279"/>
        <source>Options</source>
        <translation>Možnosti</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="285"/>
        <source>Afficher ma selection</source>
        <translation>Zobrazit můj výběr</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="309"/>
        <source>Afficher les 10 premiers</source>
        <translation>Zobrazit top 10</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="295"/>
        <source>Afficher les 10 plus proches en distance</source>
        <translation>Zobrazit 10 sousedních (vzdálenost)</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="316"/>
        <source>Afficher les 10 plus proches au classement</source>
        <translation>Zobrazit 10 sousedních (pořadí)</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="138"/>
        <source>En course</source>
        <translation>Závodí</translation>
    </message>
    <message>
        <source>Arrivés</source>
        <translation type="obsolete">Finished</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="76"/>
        <source>Bateaux:</source>
        <translation>Lodě:</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="83"/>
        <location filename="../src/Ui/race_dialog.ui" line="97"/>
        <location filename="../src/Ui/race_dialog.ui" line="111"/>
        <source>xxxx</source>
        <translation>xxxx</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="90"/>
        <source>En course:</source>
        <translation>Závodí:</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="104"/>
        <source>Arrivés:</source>
        <translation>Dojelo:</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="118"/>
        <source>Sélectionnés:</source>
        <translation>Vybráno:</translation>
    </message>
    <message>
        <location filename="../src/Ui/race_dialog.ui" line="125"/>
        <source>xx</source>
        <translation>xx</translation>
    </message>
</context>
<context>
    <name>realBoatConfig</name>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="14"/>
        <source>Paramétrage du bateau</source>
        <translation>Nastavení lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="20"/>
        <source>Port serie</source>
        <translation>Sériový port</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="29"/>
        <source>Nom</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="39"/>
        <source>Vitesse</source>
        <translation>Rychlost</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="47"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="52"/>
        <source>75</source>
        <translation>75</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="57"/>
        <source>110</source>
        <translation>110</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="62"/>
        <source>134</source>
        <translation>134</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="67"/>
        <source>150</source>
        <translation>150</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="72"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="77"/>
        <source>300</source>
        <translation>300</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="82"/>
        <source>600</source>
        <translation>600</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="87"/>
        <source>1200</source>
        <translation>1200</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="92"/>
        <source>1800</source>
        <translation>1800</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="97"/>
        <source>2400</source>
        <translation>2400</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="102"/>
        <source>4800</source>
        <translation>4800</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="107"/>
        <source>9600</source>
        <translation>9600</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="112"/>
        <source>14400</source>
        <translation>14400</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="117"/>
        <source>19200</source>
        <translation>19200</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="122"/>
        <source>38400</source>
        <translation>38400</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="127"/>
        <source>56000</source>
        <translation>56000</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="132"/>
        <source>57600</source>
        <translation>57600</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="137"/>
        <source>76800</source>
        <translation>76800</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="142"/>
        <source>115200</source>
        <translation>115200</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="147"/>
        <source>128000</source>
        <translation>128000</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="152"/>
        <source>256000</source>
        <translation>256000</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="160"/>
        <source>Afficher les donnees NMEA brutes</source>
        <translation>Zobrazit syrová NMEA data</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="170"/>
        <source>Declinaison</source>
        <translation>Deklinace</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="178"/>
        <source>Valeur par defaut</source>
        <translation>Výchozí hodnota</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="235"/>
        <source>Efficacite</source>
        <translation>Účinnost</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="258"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="274"/>
        <source>Moteur si vitesse inferieure a</source>
        <translation>Použij motor, pokud je rychlost lodi menší než</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="317"/>
        <location filename="../src/Ui/realBoatConfig.ui" line="330"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="294"/>
        <source>Vitesse au moteur</source>
        <translation>Rychlost na motor</translation>
    </message>
    <message>
        <source>Declinaison par defaut</source>
        <translation type="obsolete">Default declinaison</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="198"/>
        <source>Positif pour Est, negatif pour Ouest</source>
        <translation>Kladný pro východ, negativní pro západ</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="204"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <source>Afficher les donnees brutes</source>
        <translation type="obsolete">Display raw NMEA data</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatConfig.ui" line="222"/>
        <source>Polaire</source>
        <translation>Polár</translation>
    </message>
</context>
<context>
    <name>realBoatPos</name>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="14"/>
        <source>Position du bateau</source>
        <translation>Pozice lodi</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="20"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="29"/>
        <location filename="../src/Ui/realBoatPosition.ui" line="54"/>
        <location filename="../src/Ui/realBoatPosition.ui" line="93"/>
        <location filename="../src/Ui/realBoatPosition.ui" line="197"/>
        <source> °</source>
        <translation> °</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="76"/>
        <source>Latitude (dd,dd)</source>
        <translation>Zem. šířka (dd,dd)</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="83"/>
        <source>Longitude (dd,dd)</source>
        <translation>Zem. délka (dd,dd)</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="112"/>
        <location filename="../src/Ui/realBoatPosition.ui" line="155"/>
        <source> &apos;</source>
        <translation> &apos;</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="132"/>
        <source>Est</source>
        <translation>Východ</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="137"/>
        <source>Ouest</source>
        <translation>Západ</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="145"/>
        <source>Latitude</source>
        <translation>Zem. šířka</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="175"/>
        <source>Nord</source>
        <translation>Sever</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="180"/>
        <source>Sud</source>
        <translation>Jih</translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="216"/>
        <location filename="../src/Ui/realBoatPosition.ui" line="238"/>
        <source>&apos;&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/realBoatPosition.ui" line="257"/>
        <source>Longitude</source>
        <translation>Zem. délka</translation>
    </message>
    <message>
        <source>Lat</source>
        <translation type="obsolete">Lat</translation>
    </message>
    <message>
        <source>Lon</source>
        <translation type="obsolete">Lon</translation>
    </message>
    <message>
        <source>000a00a00W</source>
        <translation type="obsolete">000a00a00W</translation>
    </message>
    <message>
        <source>000a00a00N</source>
        <translation type="obsolete">00a00a00N</translation>
    </message>
    <message>
        <source>Deplacer</source>
        <translation type="obsolete">Posunout</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Zrušit</translation>
    </message>
</context>
<context>
    <name>routeInfo</name>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Informations Vent</source>
        <translation type="obsolete">Data větru</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="88"/>
        <source>TWD</source>
        <translation>TWD</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="55"/>
        <source>TWS</source>
        <translation>TWS</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="81"/>
        <source>TWA</source>
        <translation>TWA</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="155"/>
        <source>AWS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="48"/>
        <source>AWA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="254"/>
        <source>Amure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="35"/>
        <location filename="../src/Ui/routeInfo.ui" line="71"/>
        <location filename="../src/Ui/routeInfo.ui" line="142"/>
        <location filename="../src/Ui/routeInfo.ui" line="188"/>
        <location filename="../src/Ui/routeInfo.ui" line="279"/>
        <location filename="../src/Ui/routeInfo.ui" line="301"/>
        <location filename="../src/Ui/routeInfo.ui" line="392"/>
        <source>deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="20"/>
        <source>Informations Vent/Courant</source>
        <translation>Informace o větru/proudu</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="104"/>
        <location filename="../src/Ui/routeInfo.ui" line="123"/>
        <location filename="../src/Ui/routeInfo.ui" line="216"/>
        <location filename="../src/Ui/routeInfo.ui" line="352"/>
        <location filename="../src/Ui/routeInfo.ui" line="428"/>
        <source> nds</source>
        <translation> kts</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="162"/>
        <source>CS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="169"/>
        <source>CD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="264"/>
        <source>Informations Bateau et WPs</source>
        <translation>Data lodi a WP</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="336"/>
        <source>BS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="369"/>
        <source>HDG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="376"/>
        <source>CNM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="405"/>
        <source>COG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="412"/>
        <source>SOG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="362"/>
        <source>DNM</source>
        <translation>DNM</translation>
    </message>
    <message>
        <location filename="../src/Ui/routeInfo.ui" line="323"/>
        <source> NM</source>
        <translation> NM</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/routeInfo.cpp" line="55"/>
        <source>Au moteur</source>
        <translation>Na motor</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/routeInfo.cpp" line="60"/>
        <source>Babord amure</source>
        <translation>Vítr zleva</translation>
    </message>
    <message>
        <location filename="../src/Dialogs/routeInfo.cpp" line="62"/>
        <source>Tribord amure</source>
        <translation>Vítr zprava</translation>
    </message>
</context>
<context>
    <name>selectionWidget</name>
    <message>
        <location filename="../src/selectionWidget.cpp" line="47"/>
        <source>Effacer toutes les marques</source>
        <translation>Smazat všechny značky</translation>
    </message>
    <message>
        <location filename="../src/selectionWidget.cpp" line="48"/>
        <source>Effacer les marques...</source>
        <translation>Smazat značky...</translation>
    </message>
    <message>
        <location filename="../src/selectionWidget.cpp" line="49"/>
        <source>Rendre toutes les marques non-simplifiables</source>
        <translation>Udělat všechny značky nezjednodušitelné</translation>
    </message>
    <message>
        <location filename="../src/selectionWidget.cpp" line="50"/>
        <source>Rendre toutes les marques simplifiables</source>
        <translation>Udělat všechny značky zjednodušitelné</translation>
    </message>
    <message>
        <location filename="../src/selectionWidget.cpp" line="51"/>
        <source>Download with ZyGrib</source>
        <translation>Stáhnout ZyGribem</translation>
    </message>
    <message>
        <location filename="../src/selectionWidget.cpp" line="52"/>
        <source>Mail SailsDoc</source>
        <translation>Mail SailDocs</translation>
    </message>
    <message>
        <location filename="../src/selectionWidget.cpp" line="53"/>
        <source>Zoom on selection</source>
        <translation>Zaměřit na výběr</translation>
    </message>
</context>
<context>
    <name>twaLine</name>
    <message>
        <location filename="../src/Ui/twaline.ui" line="14"/>
        <source>Outil twa</source>
        <translation>Twa nástroj</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="24"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="71"/>
        <location filename="../src/Ui/twaline.ui" line="192"/>
        <location filename="../src/Ui/twaline.ui" line="250"/>
        <location filename="../src/Ui/twaline.ui" line="352"/>
        <location filename="../src/Ui/twaline.ui" line="454"/>
        <source>Nb Vacs</source>
        <translation>počet ohybů</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="39"/>
        <location filename="../src/Ui/twaline.ui" line="166"/>
        <location filename="../src/Ui/twaline.ui" line="243"/>
        <location filename="../src/Ui/twaline.ui" line="345"/>
        <location filename="../src/Ui/twaline.ui" line="447"/>
        <source>Twa</source>
        <translation>Twa</translation>
    </message>
    <message>
        <source>vacs</source>
        <translation type="obsolete">cranks</translation>
    </message>
    <message>
        <source>deg</source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="81"/>
        <location filename="../src/Ui/twaline.ui" line="176"/>
        <location filename="../src/Ui/twaline.ui" line="285"/>
        <location filename="../src/Ui/twaline.ui" line="387"/>
        <location filename="../src/Ui/twaline.ui" line="489"/>
        <source> vacs</source>
        <translation> ohyby</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="49"/>
        <location filename="../src/Ui/twaline.ui" line="144"/>
        <location filename="../src/Ui/twaline.ui" line="260"/>
        <location filename="../src/Ui/twaline.ui" line="362"/>
        <location filename="../src/Ui/twaline.ui" line="464"/>
        <source> deg</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="30"/>
        <source>Partie 1</source>
        <translation>Část 1</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="126"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="132"/>
        <source>Partie 2</source>
        <translation>Část 2</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="228"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="234"/>
        <source>Partie 3</source>
        <translation>Část 3</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="330"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="336"/>
        <source>Partie 4</source>
        <translation>Část 4</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="432"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="438"/>
        <source>Partie 5</source>
        <translation>Část 5</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="537"/>
        <source>Conserver</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="543"/>
        <source>POIs</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="550"/>
        <source>Estime</source>
        <translation>Odhad</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="560"/>
        <source>Date depart</source>
        <translation>Datum startu</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="566"/>
        <source>A la date et heure du Grib</source>
        <translation>Datum a čas gribu</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="569"/>
        <source>Grib</source>
        <translation>Grib</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="579"/>
        <source>A la date et heure de la prochaine vacation</source>
        <translation>Datum a čas další mezery</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="582"/>
        <source>Vac</source>
        <translation>Mez</translation>
    </message>
    <message>
        <source>Pas de sync Vlm depuis plus d&apos;une vac</source>
        <translation type="obsolete">No sync with VLM for more than one vac</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="99"/>
        <location filename="../src/Ui/twaline.ui" line="201"/>
        <location filename="../src/Ui/twaline.ui" line="303"/>
        <location filename="../src/Ui/twaline.ui" line="405"/>
        <location filename="../src/Ui/twaline.ui" line="507"/>
        <source>Mode</source>
        <translation>Mód</translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="105"/>
        <location filename="../src/Ui/twaline.ui" line="207"/>
        <location filename="../src/Ui/twaline.ui" line="309"/>
        <location filename="../src/Ui/twaline.ui" line="411"/>
        <location filename="../src/Ui/twaline.ui" line="513"/>
        <source>TWA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Ui/twaline.ui" line="115"/>
        <location filename="../src/Ui/twaline.ui" line="217"/>
        <location filename="../src/Ui/twaline.ui" line="319"/>
        <location filename="../src/Ui/twaline.ui" line="421"/>
        <location filename="../src/Ui/twaline.ui" line="523"/>
        <source>Cap</source>
        <translation>Kurs</translation>
    </message>
</context>
<context>
    <name>xml_POIData</name>
    <message>
        <source>Fichiers ini (*.ini)</source>
        <translation type="obsolete">ini soubory (*.ini)</translation>
    </message>
    <message>
        <source>;;Autres fichiers (*)</source>
        <translation type="obsolete">;;Jiné soubory (*)</translation>
    </message>
    <message>
        <source>Choisir un fichier ini</source>
        <translation type="obsolete">Vyber ini soubor</translation>
    </message>
    <message>
        <source>Zygrib POI import</source>
        <translation type="obsolete">Import Zygrib POI</translation>
    </message>
    <message>
        <source>POI imported from zyGrib</source>
        <translation type="obsolete">POI importován ze zyGrib</translation>
    </message>
    <message>
        <source>%1 POI de zyGrib trouve</source>
        <translation type="obsolete">%1 POI ze zyGrib nalezeno</translation>
    </message>
    <message>
        <source>Supprimer les POI de zyGrib apres importation?</source>
        <translation type="obsolete">Smazat POI ze zyGRib po dokončení importu?</translation>
    </message>
    <message>
        <source>POI de zyGrib</source>
        <translation type="obsolete">zyGrib POI</translation>
    </message>
    <message>
        <source>POI importes, pensez a sauvegarder les POI</source>
        <translation type="obsolete">POI importovány, nezapomeň uložit POI</translation>
    </message>
    <message>
        <source>Pas de POI de zyGrib trouves</source>
        <translation type="obsolete">Nenalezeny žádné zyGrib POI</translation>
    </message>
    <message>
        <source>Fichiers textes (*.txt)</source>
        <translation type="obsolete">Textové soubory</translation>
    </message>
    <message>
        <source>Choisir un fichier GeoData</source>
        <translation type="obsolete">Vyber GeoData soubor</translation>
    </message>
    <message>
        <source>Classement </source>
        <translation type="obsolete">Pořadí</translation>
    </message>
    <message>
        <source>GeoData POI import</source>
        <translatorcomment>GeoData POI Import</translatorcomment>
        <translation type="obsolete">Import POI GeoData</translation>
    </message>
    <message>
        <source>POI imported from GeoData</source>
        <translation type="obsolete">POI importován z GeoData</translation>
    </message>
    <message>
        <source>Erreur ligne %1, colonne %2:
%3</source>
        <translation type="obsolete">Chyba na řádce %1, sloupec %2: %3</translation>
    </message>
    <message>
        <source>POI importer, pensez a sauvegarder les POI</source>
        <translation type="obsolete">POIs imported, don&apos;t forget to save POIs</translation>
    </message>
    <message>
        <source>Pas de POI de zyGrib trouv�e</source>
        <translation type="obsolete">No zyGrib POIs found</translation>
    </message>
    <message>
        <source>Erreur ligne %1, colonne %2: %3</source>
        <translation type="obsolete">Error line %1, column %2: %3</translation>
    </message>
</context>
<context>
    <name>xml_boatData</name>
    <message>
        <location filename="../src/xmlBoatData.cpp" line="476"/>
        <source>Chargement des comptes/bateaux</source>
        <translation>Načítám účty/lodě</translation>
    </message>
    <message>
        <location filename="../src/xmlBoatData.cpp" line="477"/>
        <source>Ancienne version de fichier, demarrage avec une configuration vide</source>
        <translation>Soubor ze staré verze, začni s novým konfiguračním souborem</translation>
    </message>
    <message>
        <location filename="../src/xmlBoatData.cpp" line="893"/>
        <source>Parametrage des courses</source>
        <translation>Nastavení závodů</translation>
    </message>
    <message>
        <location filename="../src/xmlBoatData.cpp" line="894"/>
        <source>Nombre maximum de concurrent depasse</source>
        <translation>Maximální počet závodníků překročen</translation>
    </message>
    <message>
        <source>Paramétrage des courses</source>
        <translation type="obsolete">Races settings</translation>
    </message>
    <message>
        <source>Nombre maximum de concurrent dépassé</source>
        <translation type="obsolete">Max number of competitors exceeded</translation>
    </message>
</context>
</TS>
